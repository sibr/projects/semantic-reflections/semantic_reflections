/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SemanticUtils.hpp"

#include "core/system/Utils.hpp"
#include "core/imgproc/PoissonReconstruction.hpp"
#include "core/system/String.hpp"


namespace sibr {

	namespace semantic {

		
		void transformMesh(const sibr::Mesh::Ptr& mesh, int i) {
			sibr::Matrix3f converter;
			converter << 1, 0, 0, 0, -1, 0, 0, 0, -1;

			sibr::Mesh::Vertices tverts(mesh->vertices().size());
			for (size_t vid = 0; vid < mesh->vertices().size(); ++vid) {
				tverts[vid] = converter * mesh->vertices()[vid];
			}
			mesh->vertices(tverts);
			if (i > 0) {
				mesh->generateSmoothNormals(i);
			}

		}


		std::vector<InputCamera::Ptr> loadColmapFlipped(const std::string & colmapSparsePath, const float zNear, const float zFar)
		{
			const std::string camerasListing = colmapSparsePath + "/cameras.txt";
			const std::string imagesListing = colmapSparsePath + "/images.txt";


			std::ifstream camerasFile(camerasListing);
			std::ifstream imagesFile(imagesListing);
			if (!camerasFile.is_open()) {
				SIBR_ERR << "Unable to load camera colmap file" << std::endl;
			}
			if (!imagesFile.is_open()) {
				SIBR_WRG << "Unable to load images colmap file" << std::endl;
			}

			std::vector<sibr::InputCamera::Ptr> cameras;

			std::string line;

			struct CameraParametersColmap {
				size_t id;
				size_t width;
				size_t height;
				float  fx;
				float  fy;
				float  dx;
				float  dy;
			};

			std::map<size_t, CameraParametersColmap> cameraParameters;

			while (std::getline(camerasFile, line)) {
				if (line.empty() || line[0] == '#') {
					continue;
				}

				std::vector<std::string> tokens = sibr::split(line, ' ');
				if (tokens.size() < 8) {
					SIBR_WRG << "Unknown line." << std::endl;
					continue;
				}
				if (tokens[1] != "PINHOLE" && tokens[1] != "OPENCV") {
					SIBR_WRG << "Unknown camera type." << std::endl;
					continue;
				}

				CameraParametersColmap params;
				params.id = std::stol(tokens[0]);
				params.width = std::stol(tokens[2]);
				params.height = std::stol(tokens[3]);
				params.fx = std::stof(tokens[4]);
				params.fy = std::stof(tokens[5]);
				params.dx = std::stof(tokens[6]);
				params.dy = std::stof(tokens[7]);

				cameraParameters[params.id] = params;

			}

			// Now load the individual images and their extrinsic parameters
			sibr::Matrix3f converter;
			converter << 1, 0, 0,
				0, -1, 0,
				0, 0, -1;

			int camid = 0;
			while (std::getline(imagesFile, line)) {
				if (line.empty() || line[0] == '#') {
					continue;
				}
				std::vector<std::string> tokens = sibr::split(line, ' ');
				if (tokens.size() < 10) {
					SIBR_WRG << "Unknown line." << std::endl;
					continue;
				}
				size_t      id = std::stol(tokens[8]);
				float       qw = std::stof(tokens[1]);
				float       qx = std::stof(tokens[2]);
				float       qy = std::stof(tokens[3]);
				float       qz = std::stof(tokens[4]);
				float       tx = std::stof(tokens[5]);
				float       ty = std::stof(tokens[6]);
				float       tz = std::stof(tokens[7]);

				std::string imageName = tokens[9];

				if (cameraParameters.find(id) == cameraParameters.end())
				{
					SIBR_ERR << "Could not find intrinsics for image: "
						<< tokens[9] << std::endl;
				}
				const CameraParametersColmap & camParams = cameraParameters[id];

				const sibr::Quaternionf quat(qw, qx, qy, qz);
				const sibr::Matrix3f orientation = converter.transpose() * quat.toRotationMatrix().transpose() * converter;
				sibr::Vector3f position(tx, ty, tz);
				position = -(orientation * converter.transpose() * position);

				sibr::InputCamera::Ptr camera = std::make_shared<sibr::InputCamera>(camParams.fy, 0.0f, 0.0f, int(camParams.width), int(camParams.height), camid);
				camera->name(imageName);
				camera->position(position);
				camera->rotation(sibr::Quaternionf(orientation));
				camera->znear(zNear);
				camera->zfar(zFar);

				cameras.push_back(camera);

				++camid;
				// Skip the observations.
				std::getline(imagesFile, line);
			}


			return cameras;
		}

	}
}
