/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_SEMANTIC_REFLECTION_SCENE_HPP__
#define __SIBR_SEMANTIC_REFLECTION_SCENE_HPP__

#include "Config.hpp"

#include <core/assets/InputCamera.hpp>
#include <core/graphics/Mesh.hpp>
#include <core/graphics/Texture.hpp>
#include <core/system/Utils.hpp>

#include <vector>
#include <string>
#include "core/scene/BasicIBRScene.hpp"

/**
 * The semantic reflection scene will lazily compute missing data when loading.
 * Flags allow to control which data should be computed when creating the scene.
 * */
class SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT SemanticReflectionsScene {

	SIBR_CLASS_PTR(SemanticReflectionsScene)
	SIBR_DISALLOW_COPY(SemanticReflectionsScene)

public:

	/** Determines which data will be computed if missing. Successive flags imply the previous.*/
	enum LoadMode : uint {
		GEOMETRY_PREPROCESS = 1, ///< Only precompute meshes (windows and hull+bg) and the corresponding mask images.
		BG_PREPROCESS = 2, ///< Also compute the min interior layers using a ULR like approach.
		FLOWS_PREPROCESS = 3, ///< Also compute extra window geometry info.
		RENDERING = 4 ///< Full loading.
	};

	/** Constructor. Depending on the mode specified, will initialize missing data.
	 *\param basePath the root path of the scene
	 *\param maxWidth the maximum input image horizontal size
	 *\param mode which data should be loaded/generated depending if we are laoding the scene for preprocess or rendering
	 *\param halfResDepth for scenes with many input views, depth maps can be generated at half resolution to free up space
	 */
	SemanticReflectionsScene(const std::string & basePath, int maxWidth, LoadMode mode, bool halfResDepth);

	/** Load window mesh and preprocess it. */
	void loadWindows(const std::string& finalWindowMeshPathOBJ);

	/** update cameras clipping planes based on scene mesh. */
	void updateClippingPlanes();

	/** Should depth maps be generated using the refined mesh or the input scene. */
	enum DepthMapsSource {
		ULR, FINAL
	};

	/** Update the internal input views depth maps.
	 * \param source the mesh to use for generation
	 */
	void updateDepthmaps(const DepthMapsSource source);
	
	/// Helper.
	size_t idForCameraName(const std::string & name);

	/// Select closest cameras.
	std::vector<uint> closestCameras(const sibr::InputCamera& cam, int idToExclude = -1) const;

	/// Getters

	const std::vector<sibr::InputCamera::Ptr> & cameras();
	const std::vector<sibr::InputCamera::Ptr> & fullSizeCameras();
	sibr::Mesh::Ptr  inputMesh() const;
	sibr::Texture2DRGB::Ptr inputMeshTexture() const;
	sibr::Texture2DRGB::Ptr baseMeshTexture() const;
	sibr::Mesh::Ptr windows() const;
	sibr::Mesh::Ptr baseUlr() const;
	const std::vector<sibr::ImageRGB> & inputImages() const;
	const std::vector<sibr::ImageL8> & partsImages() const;
	const sibr::Texture2DArrayRGB::Ptr inputTextures() const;
	const sibr::Texture2DArrayLum::Ptr partsTextures() const;
	const sibr::Texture2DArrayLum32F::Ptr depthTextures() const;
	const sibr::Texture2DArrayLum::Ptr idTextures() const;
	const sibr::Texture2DArrayRGB::Ptr minBGTextures() const;
	const sibr::Texture2DArrayRGB::Ptr minFGTextures() const;
	const std::string & basePath() const;
	const sibr::Vector2f & resolution() const;
	int numWindows() const;

	/** Create a basic iBR version of the scene. */
	sibr::BasicIBRScene::Ptr getIBRScene();

	/** Returns the up vector of the input mesh *transformed* into the cameras frame. */
	sibr::Vector3f upVector();

	/** Window ellipsoid parameters. */
	struct WindowParameters {
		int meshId;
		sibr::Vector2f radii;
	};

	const std::vector<WindowParameters> & parameters() const;


private:

	/** Setup input views depth maps, using a custom mesh if needed. */
	void initDepthTextureArrays(const unsigned int w, const unsigned int h, const sibr::Mesh::Ptr mesh);

	/** Generate the window mask maps with window ID. */
	void initWindowIDTextureArrays(const unsigned int w, const unsigned int h);

	/** Generate the min composite interior maps. */
	void initMinBackgroundImages(const int w, const int h);
	
	/** Load foreground images from disk. */
	void loadForegroundMinImages(const std::string& outputDir);

	/** Generate per window mask, containing window ID (scaled), at full resolution */
	void generatePerWindowMasks(const std::vector<sibr::InputCamera::Ptr>& fullSizeCameras);

	/** Generate global window masks at full resolution. */
	void generateMasks(const std::vector<sibr::InputCamera::Ptr> & fullSizeCameras);

	/** Generate the final geometry: cut out windows and hull+background. */
	void generateFinalMeshes();

	const std::string _basePath;

	std::vector<sibr::InputCamera::Ptr> _cameras;
	std::vector<sibr::InputCamera::Ptr> _camerasFullSize;

	std::vector<sibr::ImageRGB> _inputImages;
	std::vector<sibr::ImageL8> _partsImages;

	sibr::Texture2DArrayRGB::Ptr _inputTextures;
	sibr::Texture2DArrayLum::Ptr _partsTextures;
	sibr::Texture2DArrayLum32F::Ptr _depthTextures;
	sibr::Texture2DArrayLum::Ptr _idTextures;
	sibr::Texture2DArrayRGB::Ptr _minFGTextures;
	sibr::Texture2DArrayRGB::Ptr _minBGTextures;

	sibr::Mesh::Ptr _inputMesh;
	sibr::Mesh::Ptr _windows;
	sibr::Mesh::Ptr _baseUlrMesh;

	sibr::Texture2DRGB::Ptr _inputMeshTexture;
	sibr::Texture2DRGB::Ptr _baseMeshTexture;

	sibr::Vector2f _resourcesResolution;

	int _numWindows = -1;

	std::vector<WindowParameters> _windowsParameters;
	sibr::Vector3f _upVector;
	bool _halfResDepth = false;
};


#endif // __SIBR_SEMANTIC_REFLECTION_SCENE_HPP__
