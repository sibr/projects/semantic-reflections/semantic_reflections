/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

in vec2 screenUV;
in float windowParams;

layout(location = 0) out vec4 out_color;

// 2D proxy texture.
layout(binding=0) uniform sampler2D background;
layout(binding=1) uniform sampler2D reflections;
layout(binding=2) uniform sampler2D minBackground;

uniform float alphaOut = 1.0;

void main(void){
	
	
	vec3 bgColor = texture(background, screenUV).rgb;
	vec3 reflectionColor = texture(reflections, screenUV).rgb;
	vec3 minBGColor = texture(minBackground, screenUV).rgb;
	// if the background is close enough to the min, use it.
	float lumBG = dot(bgColor, vec3(1.0/3.0));
	float lumMin = dot(minBGColor, vec3(1.0/3.0));
	float lumRef = dot(reflectionColor, vec3(1.0/3.0));
	if(abs(lumBG - lumMin) < 0.5){
		//minBGColor = bgColor;
	}

	
	// Compute transmission+reflection combined color.
	//float blendFactor = lumRef/lumMin;
	vec3 mergedColor = mix(minBGColor, reflectionColor, alphaOut);
	// Apply fading on the edges.
	out_color.rgb = mix(bgColor, mergedColor, pow(windowParams, 1.0/3.0));
	out_color.a = 1.0;
	

}
