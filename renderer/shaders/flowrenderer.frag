/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420


in vec3 worldPos;
out vec3 outColor;

uniform mat4 referenceMVP;

uniform vec3 referenceDir;
uniform vec3 referencePos;
uniform float epsilonOcclusion = 1e-2;

layout(binding=0) uniform sampler2D inputDepths;

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc) {
  vec3 d1 = referenceDir;
  vec3 d2 = p - referencePos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

void main(void) {
	outColor.rgb = vec3(0.0);

	vec3 uvd = project(worldPos, referenceMVP);
	vec2 ndc = abs(2.0*uvd.xy-1.0);
	if (frustumTest(worldPos, ndc)){

		vec2 xy_camid = vec2(uvd.xy);
		float refDepth = texture(inputDepths, xy_camid).r;

		if(abs(uvd.z-refDepth) >= epsilonOcclusion) {	  
			return;
		}

		outColor.rg = uvd.xy;
		outColor.b = 1.0;
	}
	

}
