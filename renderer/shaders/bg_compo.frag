/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

in vec2 vertex_coord;

layout(location = 0) out vec4 out_color;

// 2D proxy texture.
layout(binding=0) uniform sampler2D worldSpaceLocations;
layout(binding=1) uniform sampler2D deepBlendingTexture;

uniform vec3  gridMin;
uniform vec3  gridMax;
uniform float compositingMargin;

void main(void) {
	vec3 worldSpaceLocation = texture(worldSpaceLocations, vertex_coord).rgb;
	vec3 dbColor            = texture(deepBlendingTexture, vertex_coord).rgb;

	// TODO: CROSS FADE FOR NICER TRANSITIONS? BLEH.
	vec3 borderSize = (gridMax - gridMin) * compositingMargin;
	if (worldSpaceLocation.x + borderSize.x < gridMin.x || worldSpaceLocation.x - borderSize.x > gridMax.x ||
		worldSpaceLocation.y + borderSize.y < gridMin.y || worldSpaceLocation.y - borderSize.y > gridMax.y ||
		worldSpaceLocation.z + borderSize.z < gridMin.z || worldSpaceLocation.z - borderSize.z > gridMax.z)
		discard;

	out_color.rgb = dbColor;
	out_color.a = 1.0;
}
