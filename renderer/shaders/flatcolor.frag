/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

in float windowParams;
uniform int color;

out vec3 outColor;

uniform int windowId = -1;

void main(void) {
	// Are we in the window we want to display?
	if(windowId >= 0.0 && abs(windowParams - windowId) > 0.3){
		discard;
	}
	outColor = vec3(float(color)/255.0);
}
