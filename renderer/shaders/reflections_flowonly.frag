/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

#define NUM_CAMS (12)

in vec3 vertex_coord;
in vec3 vertex_normal;
in vec3 vertex_center;
in vec2 screenUV;
in vec2 windowParams;

layout(location = 0) out vec4 out_color;

// 2D proxy texture.
layout(binding=0) uniform sampler2DArray parts;
layout(binding=1) uniform sampler2DArray windowIds;
// Input cameras.
struct CameraInfos
{
  mat4 vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=0) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

// Uniforms.
uniform int camsCount;
uniform vec3 currentPos;
uniform vec3 sphereCenter;
uniform float sphereRadius;
uniform vec2 flowIntensity;
uniform vec3 sceneUpVector;
uniform int windowId = -1;

#define INFTY_W 100000.0
#define BETA 	1e-1  	/* Relative importance of resolution penalty */
// Helpers.

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int i) {
  vec3 d1 = cameras[i].dir;
  vec3 d2 = p - cameras[i].pos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

mat3 jacobianNorm(vec3 v){
	float vlength = length(v);
	return mat3(1.0) / vlength - outerProduct(v,v) / (vlength*vlength*vlength);
}

float intersectSphere(vec3 sc, float sr, vec3 p, vec3 d){
	// intersects with sphere.
	vec3 OJ = p - sc;
	float dotDJ = dot(d, OJ);
	float delta2 = dotDJ*dotDJ - (dot(OJ,OJ) - sr*sr);
	if(delta2 < 0.0){
		return -1.0;
	}
	float delta = sqrt(delta2);
	
	// Return the smallest positive root.
	float l1 = -dotDJ - delta;
	float l2 = -dotDJ + delta;
	if(l1 < 0.0 && l2 < 0.0){
		return -1.0;
	}

	float lambda = l1;
	if(l1 < 0.0){
		lambda = l2;
	}
	
	return lambda;
}

// Derived by IQ: see https://www.shadertoy.com/view/MlsSzn
float intersectAAEllipsoid(vec3 ec, vec3 er, vec3 p, vec3 d)
{
    vec3 oc = p - ec;
    vec3 ocn = oc / er;
    vec3 rdn = d / er;
    float a = dot( rdn, rdn );
	float b = dot( ocn, rdn );
	float c = dot( ocn, ocn );
	float h = b*b - a*(c-1.0);
	if( h<0.0 ) return -1.0;
	return (-b - sqrt( h ))/a;
}


vec3 findReflectionPositionEllipsoid(vec3 localRadii, vec3 upVector, vec3 vertexCenter, vec3 planeNormal, vec3 rayDir, vec3 refCamPos, out float loss){
	// We want to align planeNormal to (1,0,0), upVector to (0,1,0)
   	vec3 a = normalize(planeNormal);
   	vec3 b = normalize(upVector);
   	vec3 c = normalize(cross(a, b));
   	mat3 localToWorld = mat3(a,b,c);
  	mat3 worldToLocal = transpose(localToWorld);

  	// Rotate all points.
  	vec3 vertexCenterLocal = worldToLocal * vertexCenter;
  	vec3 refCamPosLocal = worldToLocal * refCamPos;
  	vec3 rayDirLocal = normalize(worldToLocal * rayDir);
  	vec3 currentPosLocal = worldToLocal * currentPos;
	vec3 sphereCenterLocal = worldToLocal * sphereCenter;

	// Assume vertex_center is at the surface of the ellipsoid, along the planeNormal direction, ie (1,0,0).
	vec3 ellipsoidCenter = vertexCenterLocal - localRadii.x * 0.95 * vec3(1.0,0.0,0.0);
	float lambdaE = intersectAAEllipsoid(ellipsoidCenter, localRadii, currentPosLocal, rayDirLocal);
	vec3 intersectionLocal = currentPosLocal + lambdaE * rayDirLocal;
	vec3 windowNormalLocal = normalize((intersectionLocal - ellipsoidCenter) / localRadii);
	//vec3 posFromwindowNormalLocal = ellipsoidCenter + localRadii * windowNormalLocal;

	vec3 refRayDir = normalize(rayDirLocal - 2.0 * (dot(rayDirLocal, windowNormalLocal) * windowNormalLocal));
	float lambda = intersectSphere(sphereCenterLocal, sphereRadius, intersectionLocal, refRayDir);
	vec3 bgIntersection = intersectionLocal + lambda * refRayDir;
	
	// Move to frame centered on ellipsoid center.
	vec3 ellipsoidNormal = windowNormalLocal;
	vec3 refCamSpherePos = refCamPosLocal - ellipsoidCenter;
	vec3 pointSpherePos = bgIntersection - ellipsoidCenter;
	vec3 ellipsoidPos = intersectionLocal - ellipsoidCenter;
	loss = lambdaE;
	
	for(int gid = 0; gid < 50; ++gid){
		vec3 prn = pointSpherePos - ellipsoidPos;
		vec3 irn = refCamSpherePos - ellipsoidPos;
		vec3 v   = normalize(prn) + normalize(irn);
		//float vlength = length(v);
		 
		//vec3 g = normalize(v) - localRadius * normalWindowSphere * jacobianNorm(v) * (jacobianNorm(prn) + jacobianNorm(irn));
		vec3 g = normalize(v) - ellipsoidNormal;
		ellipsoidNormal += 0.2f * (g);
		ellipsoidNormal = normalize(ellipsoidNormal);
		ellipsoidPos = localRadii * ellipsoidNormal;
	}

	// Final loss.
	vec3 prn = pointSpherePos - ellipsoidPos;
	vec3 irn = refCamSpherePos - ellipsoidPos;
	vec3 v   = normalize(normalize(prn) + normalize(irn));
	loss = dot(v, ellipsoidNormal);

	return localToWorld*(ellipsoidCenter + ellipsoidPos);
	
}


vec3 projectIfVisible(vec3 posWindowSphere, int i){
	vec3 refuvd = project(posWindowSphere, cameras[i].vp);
	// Check frustum
	if (!frustumTest(posWindowSphere, abs(2.0*refuvd.xy-1.0), i)){
		return vec3(0.0);
	}

	vec2 refxy_flip = refuvd.xy;
	refxy_flip.y = 1.0 - refxy_flip.y;
	return vec3(refxy_flip, 1.0);
}


void main(void){
	// Are we in the window we want to display?
	out_color = vec4(0.0,0.0,0.0,1.0);

	if(windowId >= 0.0 && abs(windowParams.x - windowId) > 0.3){
		discard;
	}		
	// Plane parameters.
	vec3 planeNormal = normalize(vertex_normal);
	vec3 rayDir = normalize(vertex_coord - currentPos);
	vec4 accumColorFull = vec4(0.0);
	vec4 accumColorDiff = vec4(0.0);
	vec3 softWeight = vec3(0.0);
	

	vec2 windowSphereRadii = flowIntensity;
	vec3 ellipsoidRadii = windowSphereRadii.xxy;

	

  	for(int i = 0; i < NUM_CAMS; i++){
		if(i>=camsCount){
			continue;
		}
		if(cameras[i].selected == 0){
			continue;
		}
		// Reproject current point in reference view.
		// todo check if using intersection instead would help
		vec3 uvd = project(vertex_coord, cameras[i].vp);
		vec2 ndc = abs(2.0*uvd.xy-1.0);
		// Check frustum
		if (!frustumTest(vertex_coord, ndc , i)){
			continue;
		}
		// Occlusions are included in the input masks as they have been regenerated.
		vec3 xy_camid = vec3(uvd.xy, i);
		// Final UVs.
		vec3 xy_flip = xy_camid;
		xy_flip.y = 1.0 - xy_flip.y;
		// Check mask.
		float inputMask = texture(parts, xy_flip).r;
		if(inputMask < 0.5){
			continue;
		}

		// Ellipsoid.
		float loss;
		
		// Will break for scene horizontal planes.
		vec3 localUp = sceneUpVector - dot(sceneUpVector, planeNormal) * planeNormal;
		vec3 positionEllipsoid = findReflectionPositionEllipsoid(ellipsoidRadii, localUp, vertex_center, planeNormal, rayDir, cameras[i].pos, loss);
		
	

		vec3 pixels = projectIfVisible(positionEllipsoid, i);
		if(pixels.z < 0.5){
			continue;
		}
		vec2 flow = pixels.xy - xy_flip.xy;

		
		
		vec3 finalFetch = vec3(xy_flip.xy + flow, i);

		// Check mask.
		float refMask = texture(parts, finalFetch).r;
		

		if(refMask < 0.5){
			continue;
		}
		
		
		vec3 flipFinalFetch = finalFetch;
		flipFinalFetch.y = 1.0 - flipFinalFetch.y;
		float localId = texture(windowIds, flipFinalFetch).r*255.0;
		if(abs(windowParams.x - localId) > 0.3){
			continue;
		}

		// If we are here, we are in both masks and we have the final texture UVS, we can use the flow.
	
		// In that case we are writing to a RGB_32F RT, output the final fetch UVs and the mask status.
		out_color.rg = finalFetch.xy;
		out_color.b = 1.0;
		out_color.a = 1.0;
		return;
		
		
	}
}
