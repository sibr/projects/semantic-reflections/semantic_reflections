/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

#define NUM_CAMS (12)

in vec3 vertex_coord;
in vec3 vertex_normal;
in vec3 vertex_center;
in vec2 screenUV;
in vec2 windowParams;

layout(location = 0) out vec4 out_color;

// 2D proxy texture.
layout(binding=0) uniform sampler2DArray colors;
layout(binding=1) uniform sampler2DArray depths;
layout(binding=2) uniform sampler2DArray parts;
layout(binding=3) uniform sampler2DArray mins;
layout(binding=4) uniform sampler2D minNovelView;
layout(binding=5) uniform sampler2D depthULR;
layout(binding=6) uniform sampler2DArray windowIds;
// Input cameras.
struct CameraInfos
{
  mat4 vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=0) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

// Uniforms.
uniform int camsCount;
uniform vec3 currentPos;
uniform vec3 sphereCenter;
uniform float sphereRadius;

uniform vec2 flowIntensity;

uniform bool useMinCompo;
uniform float mixFullAndDiff = 1.0;
uniform float panComparison = 1.0;

uniform bool applyBackgroundMin;

uniform vec3 sceneUpVector;
uniform int windowId = -1;
uniform bool outputFirstValidFlow = false;
uniform bool stricterMasks = false;

#define INFTY_W 100000.0
#define BETA 	1e-1  	/* Relative importance of resolution penalty */
// Helpers.

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int i) {
  vec3 d1 = cameras[i].dir;
  vec3 d2 = p - cameras[i].pos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

mat3 jacobianNorm(vec3 v){
	float vlength = length(v);
	return mat3(1.0) / vlength - outerProduct(v,v) / (vlength*vlength*vlength);
}

float intersectSphere(vec3 sc, float sr, vec3 p, vec3 d){
	// intersects with sphere.
	vec3 OJ = p - sc;
	float dotDJ = dot(d, OJ);
	float delta2 = dotDJ*dotDJ - (dot(OJ,OJ) - sr*sr);
	if(delta2 < 0.0){
		return -1.0;
	}
	float delta = sqrt(delta2);
	
	// Return the smallest positive root.
	float l1 = -dotDJ - delta;
	float l2 = -dotDJ + delta;
	if(l1 < 0.0 && l2 < 0.0){
		return -1.0;
	}

	float lambda = l1;
	if(l1 < 0.0){
		lambda = l2;
	}
	
	return lambda;
}

// Derived by IQ: see https://www.shadertoy.com/view/MlsSzn
float intersectAAEllipsoid(vec3 ec, vec3 er, vec3 p, vec3 d)
{
    vec3 oc = p - ec;
    vec3 ocn = oc / er;
    vec3 rdn = d / er;
    float a = dot( rdn, rdn );
	float b = dot( ocn, rdn );
	float c = dot( ocn, ocn );
	float h = b*b - a*(c-1.0);
	if( h<0.0 ) return -1.0;
	return (-b - sqrt( h ))/a;
}


vec3 findReflectionPositionEllipsoid(vec3 localRadii, vec3 upVector, vec3 vertexCenter, vec3 planeNormal, vec3 rayDir, vec3 refCamPos, out float loss){
	// We want to align planeNormal to (1,0,0), upVector to (0,1,0)
   	vec3 a = normalize(planeNormal);
   	vec3 b = normalize(upVector);
   	vec3 c = normalize(cross(a, b));
   	mat3 localToWorld = mat3(a,b,c);
  	mat3 worldToLocal = transpose(localToWorld);

  	// Rotate all points.
  	vec3 vertexCenterLocal = worldToLocal * vertexCenter;
  	vec3 refCamPosLocal = worldToLocal * refCamPos;
  	vec3 rayDirLocal = normalize(worldToLocal * rayDir);
  	vec3 currentPosLocal = worldToLocal * currentPos;
	vec3 sphereCenterLocal = worldToLocal * sphereCenter;

	// Assume vertex_center is at the surface of the ellipsoid, along the planeNormal direction, ie (1,0,0).
	vec3 ellipsoidCenter = vertexCenterLocal - localRadii.x * 0.95 * vec3(1.0,0.0,0.0);
	float lambdaE = intersectAAEllipsoid(ellipsoidCenter, localRadii, currentPosLocal, rayDirLocal);
	vec3 intersectionLocal = currentPosLocal + lambdaE * rayDirLocal;
	vec3 windowNormalLocal = normalize((intersectionLocal - ellipsoidCenter) / localRadii);
	//vec3 posFromwindowNormalLocal = ellipsoidCenter + localRadii * windowNormalLocal;

	vec3 refRayDir = normalize(rayDirLocal - 2.0 * (dot(rayDirLocal, windowNormalLocal) * windowNormalLocal));
	float lambda = intersectSphere(sphereCenterLocal, sphereRadius, intersectionLocal, refRayDir);
	vec3 bgIntersection = intersectionLocal + lambda * refRayDir;
	
	// Move to frame centered on ellipsoid center.
	vec3 ellipsoidNormal = windowNormalLocal;
	vec3 refCamSpherePos = refCamPosLocal - ellipsoidCenter;
	vec3 pointSpherePos = bgIntersection - ellipsoidCenter;
	vec3 ellipsoidPos = intersectionLocal - ellipsoidCenter;
	loss = lambdaE;
	
	for(int gid = 0; gid < 50; ++gid){
		vec3 prn = pointSpherePos - ellipsoidPos;
		vec3 irn = refCamSpherePos - ellipsoidPos;
		vec3 v   = normalize(prn) + normalize(irn);
		//float vlength = length(v);
		 
		//vec3 g = normalize(v) - localRadius * normalWindowSphere * jacobianNorm(v) * (jacobianNorm(prn) + jacobianNorm(irn));
		vec3 g = normalize(v) - ellipsoidNormal;
		ellipsoidNormal += 0.2f * (g);
		ellipsoidNormal = normalize(ellipsoidNormal);
		ellipsoidPos = localRadii * ellipsoidNormal;
	}

	// Final loss for visualization only.
	vec3 prn = pointSpherePos - ellipsoidPos;
	vec3 irn = refCamSpherePos - ellipsoidPos;
	vec3 v   = normalize(normalize(prn) + normalize(irn));
	loss = dot(v, ellipsoidNormal);

	return localToWorld*(ellipsoidCenter + ellipsoidPos);
	
}

vec3 findReflectionPosition(float localRadius, vec3 vertexCenter, vec3 planeNormal, vec3 rayDir, vec3 refCamPos, out float loss){
	vec3 windowSphereCenter = (vertexCenter - localRadius * planeNormal);
	vec3 intersection = currentPos + intersectSphere(windowSphereCenter, localRadius, currentPos, rayDir) * rayDir;
	vec3 windowNormal = normalize(intersection - windowSphereCenter);


	vec3 refRayDir = normalize(rayDir - 2.0 * (dot(rayDir, windowNormal) * windowNormal));
	float lambda = intersectSphere(sphereCenter, sphereRadius, intersection, refRayDir);
	vec3 bgIntersection = intersection + lambda * refRayDir;
	
	vec3 normalWindowSphere = windowNormal;
	vec3 refCamSpherePos = refCamPos - windowSphereCenter;
	vec3 pointSpherePos = bgIntersection - windowSphereCenter;

	for(int gid = 0; gid < 20; ++gid){
		vec3 prn = pointSpherePos -  localRadius*normalWindowSphere;
		vec3 irn = refCamSpherePos - localRadius*normalWindowSphere;
		vec3 v   = normalize(prn) + normalize(irn);
		float vlength = length(v);
		 
		vec3 g = normalize(v) - localRadius * normalWindowSphere * jacobianNorm(v) * (jacobianNorm(prn) + jacobianNorm(irn));
		//vec3 g = normalize(v) - normalWindowSphere;
		normalWindowSphere += 0.01f * (g);
		normalWindowSphere = normalize(normalWindowSphere);
	}
	// Final loss.
	vec3 prn = pointSpherePos -  localRadius*normalWindowSphere;
	vec3 irn = refCamSpherePos - localRadius*normalWindowSphere;
	vec3 v   = normalize(normalize(prn) + normalize(irn));
	loss = dot(v, normalWindowSphere);

	return windowSphereCenter + localRadius * normalWindowSphere;
}

vec3 projectIfVisible(vec3 posWindowSphere, int i){
	vec3 refuvd = project(posWindowSphere, cameras[i].vp);
	// Check frustum
	if (!frustumTest(posWindowSphere, abs(2.0*refuvd.xy-1.0), i)){
		return vec3(0.0);
	}

	vec2 refxy_flip = refuvd.xy;
	refxy_flip.y = 1.0 - refxy_flip.y;
	return vec3(refxy_flip, 1.0);
}


void main(void){
	// Are we in the window we want to display?
	if(windowId >= 0.0 && abs(windowParams.x - windowId) > 0.3){
		discard;
	}
	
	// Plane parameters.
	vec3 planeNormal = normalize(vertex_normal);
	vec3 rayDir = normalize(vertex_coord - currentPos);
	vec4 accumColorFull = vec4(0.0);
	vec4 accumColorDiff = vec4(0.0);
	vec3 softWeight = vec3(0.0);
	
	vec4 color0 = vec4(0.0,0.0,0.0,INFTY_W);
	vec4 color1 = vec4(0.0,0.0,0.0,INFTY_W);
	vec4 color2 = vec4(0.0,0.0,0.0,INFTY_W);
	vec4 color3 = vec4(0.0,0.0,0.0,INFTY_W);

	vec4 color4 = vec4(0.0,0.0,0.0,INFTY_W);
	vec4 color5 = vec4(0.0,0.0,0.0,INFTY_W);
	vec4 color6 = vec4(0.0,0.0,0.0,INFTY_W);
	vec4 color7 = vec4(0.0,0.0,0.0,INFTY_W);

	vec2 windowSphereRadii = flowIntensity;
	vec3 ellipsoidRadii = windowSphereRadii.xxy;

	vec3 minForeground = vec3(0.0);
	vec3 minForegroundW = vec3(0.0);

  	for(int i = 0; i < NUM_CAMS; i++){
		if(i>=camsCount){
			continue;
		}
		if(cameras[i].selected == 0){
			continue;
		}
		// Reproject current point in reference view.
		// todo check if using intersection instead would help
		vec3 uvd = project(vertex_coord, cameras[i].vp);
		vec2 ndc = abs(2.0*uvd.xy-1.0);
		// Check frustum
		if (!frustumTest(vertex_coord, ndc , i)){
			continue;
		}
		// Occlusions are already tested in the regenerated masks.
		vec3 xy_camid = vec3(uvd.xy, i);
		/*float inputDepth = texture(depths, xy_camid).r;
		if(uvd.z >= inputDepth) {	  
			continue;
		}*/
		// Final UVs.
		vec3 xy_flip = xy_camid;
		xy_flip.y = 1.0 - xy_flip.y;
		// Check mask.
		float inputMask = texture(parts, xy_flip).r;
		if(inputMask < 0.5){
			continue;
		}
		
//#define PLANAR_APPROX 1

#if BISPHERE_APPROX
		float lossX, lossY;
		vec3 posWindowSphereX = findReflectionPosition(windowSphereRadii.x, vertex_center, planeNormal, rayDir, cameras[i].pos, lossX);
		vec3 posWindowSphereY = findReflectionPosition(windowSphereRadii.y, vertex_center, planeNormal, rayDir, cameras[i].pos, lossY);
		
		#ifdef SHOW_LOSS
		accumColor.rgb = vec3((1.0 - max(lossY, 0.0))*1000.0);
		accumColor.a = 1.0;
		continue;
		#endif

		// Is it visible in the reflection in the other camera.
		vec3 pixelsX = projectIfVisible(posWindowSphereX, i);
		vec3 pixelsY = projectIfVisible(posWindowSphereY, i);
		if(pixelsX.z + pixelsY.z < 2.0){
			continue;
		}
		vec2 flow = vec2(pixelsX.x, pixelsY.y) - xy_flip.xy;
#elif PLANAR_APPROX
		vec3 intersection = vertex_coord;
		vec3 refRayDir = normalize(rayDir - 2.0 * (dot(rayDir, planeNormal) * planeNormal));
	
	
		float lambda = intersectSphere(sphereCenter, sphereRadius, intersection, refRayDir);
		vec3 posBGFlip = intersection + lambda * rayDir;

		// Is it visible in the reflection in the other camera.
		vec3 refuvd = project(posBGFlip, cameras[i].vp);
		vec2 refndc = abs(2.0*refuvd.xy-1.0);
		// Check frustum
		if (!frustumTest(posBGFlip, refndc, i)){
			continue;
		}

		vec2 refxy_flip = refuvd.xy;
		refxy_flip.y = 1.0 - refxy_flip.y;

		vec2 flow = refxy_flip.xy - xy_flip.xy;
		//flow *= flowIntensity;
#else
		// Ellipsoid.
		float loss;
		
		// Will break for scene horizontal planes.
		vec3 localUp = sceneUpVector - dot(sceneUpVector, planeNormal) * planeNormal;
		vec3 positionEllipsoid = findReflectionPositionEllipsoid(ellipsoidRadii, localUp, vertex_center, planeNormal, rayDir, cameras[i].pos, loss);
		
		#ifdef SHOW_LOSS
		loss = 0.5 * loss + 0.5;
		out_color.rgb = vec3((1.0 - loss)*100.0);
		out_color.a = 1.0;
		return;
		#endif

		vec3 pixels = projectIfVisible(positionEllipsoid, i);
		if(pixels.z < 0.5){
			continue;
		}
		vec2 flow = pixels.xy - xy_flip.xy;
#endif
		
		
		vec3 finalFetch = vec3(xy_flip.xy + flow, i);
		
		// Check mask.
		float refMask = texture(parts, finalFetch).r;
		

		if(refMask < 0.5){
			continue;
		}
		if(stricterMasks){
			const float shiftFetch = 15.0;
			float refMaskX = texture(parts, finalFetch + vec3(shiftFetch/textureSize(parts, 0).x,0.0,0.0)).r;
			float refMaskY = texture(parts, finalFetch + vec3(0.0, shiftFetch/textureSize(parts, 0).y,0.0)).r;
			
			float refMaskX1 = texture(parts, finalFetch - vec3(shiftFetch/textureSize(parts, 0).x,0.0,0.0)).r;
			float refMaskY1 = texture(parts, finalFetch - vec3(0.0, shiftFetch/textureSize(parts, 0).y,0.0)).r;

			if( refMaskX < 0.5 || refMaskY < 0.5 || refMaskX1 < 0.5 || refMaskY1 < 0.5){
				continue;
			}
		}
		
		vec3 flipFinalFetch = finalFetch;
		flipFinalFetch.y = 1.0 - flipFinalFetch.y;
		float localId = texture(windowIds, flipFinalFetch).r*255.0;
		if(abs(windowParams.x - localId) > 0.3){
			continue;
		}

		// If we are here, we are in both masks and we have the final texture UVS, we can use the flow.
		if(outputFirstValidFlow){
			// In that case we are writing to a RGB_32F RT, output the final fetch UVs and the mask status.
			out_color.rg = finalFetch.xy;
			out_color.b = 1.0;
			out_color.a = 1.0;
			return;
		}
		// Fetch directly the color there.
		

		vec3 v1 = (vertex_coord - cameras[i].pos);
		vec3 v2 = (vertex_coord - currentPos);
		float dist_i2p 	= length(v1);
		float dist_n2p 	= length(v2);
		float penalty_ang = max(0.0001, acos(dot(v1,v2)/(dist_i2p*dist_n2p)));
		float penalty_res = max(0.0001, (dist_i2p - dist_n2p)/dist_i2p );
		float weight = penalty_ang + BETA*penalty_res;

		vec4 currentCol = vec4(finalFetch, weight);
		if (currentCol.w<color3.w) {    // better than fourth best candidate
			if (currentCol.w<color2.w) {    // better than third best candidate
				color3 = color2;
				if (currentCol.w<color1.w) {    // better than second best candidate
					color2 = color1;
					if (currentCol.w<color0.w) {    // better than best candidate
						color1 = color0;
						color0 = currentCol;
					} else {
						color1 = currentCol;
					}
				} else {
					color2 = currentCol;
				}
			} else {
				color3 = currentCol;
			}
		}
		accumColorFull.a = 1.0;
		accumColorDiff.a = 1.0;
		
		
	}


	if(accumColorDiff.a == 0.0){
		discard;
	}
	
	if (!useMinCompo){
		float thresh = 1.000001 * color3.w;
	    color0.w = max(0.0, 1.0 - color0.w/thresh);
	    color1.w = max(0.0, 1.0 - color1.w/thresh);
	    color2.w = max(0.0, 1.0 - color2.w/thresh);
	    color3.w = max(0.0, 1.0 - color3.w/thresh);
	    // fetching fullcolor
	    vec3 refColor0 = texture(colors, color0.rgb).rgb;
		vec3 refColor1 = texture(colors, color1.rgb).rgb;
		vec3 refColor2 = texture(colors, color2.rgb).rgb;
		vec3 refColor3 = texture(colors, color3.rgb).rgb;
		vec3 finalReflip0 = color0.rgb; finalReflip0.y = 1.0 - finalReflip0.y;
		vec3 finalReflip1 = color1.rgb; finalReflip1.y = 1.0 - finalReflip1.y;
		vec3 finalReflip2 = color2.rgb; finalReflip2.y = 1.0 - finalReflip2.y;
		vec3 finalReflip3 = color3.rgb; finalReflip3.y = 1.0 - finalReflip3.y;
		// fetching min image
		vec3 minColor0 = texture(mins, finalReflip0).rgb;
		vec3 minColor1 = texture(mins, finalReflip1).rgb;
		vec3 minColor2 = texture(mins, finalReflip2).rgb;
		vec3 minColor3 = texture(mins, finalReflip3).rgb;

		vec3 diffColor0 = max(refColor0 - minColor0, vec3(0.0));
		vec3 diffColor1 = max(refColor1 - minColor1, vec3(0.0));
		vec3 diffColor2 = max(refColor2 - minColor2, vec3(0.0));
		vec3 diffColor3 = max(refColor3 - minColor3, vec3(0.0));

		// Full color.
		vec3 fullOutColor = color0.w*refColor0.rgb + color1.w * refColor1.rgb + color2.w * refColor2.rgb + color3.w * refColor3.rgb;
		float denomFull = color0.w + color1.w + color2.w + color3.w;
		fullOutColor /= denomFull;
		// Diff soft max.
		vec3 bgColor = texture(minNovelView, screenUV).rgb;
		const float alpha = 16.0;
		vec3 w0 = color0.w*exp(alpha*color0.w*diffColor0.rgb);
		vec3 w1 = color1.w*exp(alpha*color1.w*diffColor1.rgb);
		vec3 w2 = color2.w*exp(alpha*color2.w*diffColor2.rgb);
		vec3 w3 = color3.w*exp(alpha*color3.w*diffColor3.rgb);
		
		vec3 softCol = diffColor0.rgb*w0 + diffColor1.rgb * w1 + diffColor2.rgb * w2 + diffColor3.rgb*w3;
		vec3 softColWeigh = w0 + w1 + w2 + w3;

		vec3 diffOutColor = float(applyBackgroundMin) * bgColor + (applyBackgroundMin ? 1.0 : 5.0) * softCol / softColWeigh;
		out_color.rgb = mix(fullOutColor, diffOutColor, mixFullAndDiff);
		out_color.a = 1.0;
	} else {
		float thresh = 1.000001 * color3.w;
	    color0.w = max(0.0, 1.0 - color0.w/thresh);
	    color1.w = max(0.0, 1.0 - color1.w/thresh);
	    color2.w = max(0.0, 1.0 - color2.w/thresh);
	    color3.w = max(0.0, 1.0 - color3.w/thresh);
	    // fetching fullcolor
		vec3 finalReflip0 = color0.rgb; //finalReflip0.y = 1.0 - finalReflip0.y;
		vec3 finalReflip1 = color1.rgb; //finalReflip1.y = 1.0 - finalReflip1.y;
		vec3 finalReflip2 = color2.rgb; //finalReflip2.y = 1.0 - finalReflip2.y;
		vec3 finalReflip3 = color3.rgb; //finalReflip3.y = 1.0 - finalReflip3.y;
		// fetching min image
		vec3 minColor0 = texture(colors, finalReflip0).rgb;
		vec3 minColor1 = texture(colors, finalReflip1).rgb;
		vec3 minColor2 = texture(colors, finalReflip2).rgb;
		vec3 minColor3 = texture(colors, finalReflip3).rgb;

		
		// Full color.
		vec3 fullOutColor = color0.w*minColor0.rgb + color1.w * minColor1.rgb + color2.w * minColor2.rgb + color3.w * minColor3.rgb;
		float denomFull = color0.w + color1.w + color2.w + color3.w;
		fullOutColor /= denomFull;

		// Diff soft max.
		vec3 bgColor = texture(minNovelView, screenUV).rgb;
		
		out_color.rgb = fullOutColor;//mix(bgColor, fullOutColor, 1.0);//dot(fullOutColor, vec3(1.0))/dot(bgColor, vec3(1.0)));
		out_color.a = 1.0;
	}
	//out_color.rgb = mix(texture(minNovelView, screenUV).rgb, out_color.rgb, pow(windowParams.y, 1.0));
	//out_color.a = 1.0;		
	//return;
	if(screenUV.x > panComparison){
		if(screenUV.x < panComparison + 1e-3){
			out_color.a = 1.0;
			out_color.rgb = vec3(1.0, 0.0, 0.0);
		} else {
			out_color.a = 0.0;
		}
		
	}

}
