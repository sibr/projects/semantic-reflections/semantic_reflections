/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

#define NUM_CAMS (12)
#define ULR_STREAMING (0)

in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;

// 2D proxy texture.
layout(binding=0) uniform sampler2D proxy;

// Input cameras.
struct CameraInfos
{
  mat4 vp;
  vec3 pos;
  int selected;
  vec3 dir;
};
// They are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
layout(std140, binding=4) uniform InputCameras
{
  CameraInfos cameras[NUM_CAMS];
};

// Uniforms.
uniform int camsCount;
uniform vec3 ncam_pos;
uniform bool occ_test = true;
uniform bool invert_mask = false;
uniform bool is_binary_mask = true;
uniform bool discard_black_pixels = true;
uniform bool doMasking = true;
uniform bool flipRGBs = false;
uniform bool showWeights = false;
uniform bool gammaCorrection = false;
uniform float epsilonOcclusion = 1e-2;


#define INFTY_W 100000.0
#define BETA    1e-1    /* Relative importance of resolution penalty */

// Textures.
// To support both the regular version (using texture arrays) and the streaming version (using 2D RTs),
// we wrap the texture accesses in two helpers that hide the difference.


layout(binding=1) uniform sampler2DArray input_rgbs;
layout(binding=2) uniform sampler2DArray input_depths;
layout(binding=3) uniform sampler2DArray input_masks;



// Helpers.

vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int i) {
  vec3 d1 = cameras[i].dir;
  vec3 d2 = p - cameras[i].pos;
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}

void main(void){
		
	vec4 point = texture(proxy, vertex_coord);
 	// discard if there was no intersection with the proxy
	if ( point.w >= 1.0) {
		discard;
	}

	vec4 colorMin = vec4(0.0);
  	vec3 smoothWeight = vec3(0.0);
  	for(int i = 0; i < NUM_CAMS; i++){
		if(i>=camsCount){
			continue;
		}
		if(cameras[i].selected == 0){
			continue;
		}

		vec3 uvd = project(point.xyz, cameras[i].vp);
		vec2 ndc = abs(2.0*uvd.xy-1.0);

	
		if (frustumTest(point.xyz, ndc, i)){
			vec3 xy_camid = vec3(uvd.xy,i);
			
			float depth = texture(input_depths, xy_camid).r;
			if(abs(uvd.z-depth) >= 1e-2) {    
				continue;
			}
			
			
			
			if(flipRGBs){
				xy_camid.y = 1.0 - xy_camid.y;
			}
			vec4 color = texture(input_rgbs, xy_camid);

			float alpha = 1.0;
			colorMin.rgb += color.rgb * exp(-alpha*color.rgb);
			smoothWeight += exp(-alpha*color.rgb);
			colorMin.a = 1.0;
			
		}
	}
   
	
	// blending
	out_color.w = 1.0;
	if(colorMin.a == 0.0 || all(equal(colorMin.rgb, vec3(0.0)))){
		discard;
		
	}
	out_color.xyz = colorMin.rgb / smoothWeight;
	
}



