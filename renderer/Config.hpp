/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_SEMANTIC_REFLECTIONS_CONFIG_HPP__
#define __SIBR_SEMANTIC_REFLECTIONS_CONFIG_HPP__

# include "core/system/Config.hpp"

/// Disable warning message for unsafe 'fopen' function
#pragma warning(disable:4996)

//// Export Macro (used for creating DLLs) ////
# if defined(_WIN32)
#  ifdef SIBR_STATIC_SEMANTIC_REFLECTIONS_LIB_DEFINE
#    define SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT
#    define SIBR_NO_SEMANTIC_REFLECTIONS_LIB_EXPORT
#  else
#    ifndef SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT
#      ifdef SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORTS
          /* We are building this library */
#        define SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT __declspec(dllexport)
#      else
          /* We are using this library */
#        define SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT __declspec(dllimport)
#      endif
#    endif
#    ifndef SIBR_NO_EXPORT
#      define SIBR_NO_EXPORT 
#    endif
#  endif
# else
#  define SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT
# endif

#endif // __SIBR_SEMANTIC_REFLECTIONS_CONFIG_HPP__
