/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SemanticReflectionsScene.hpp"
#include "core/graphics/Texture.hpp"
#include "projects/semantic_reflections/renderer/SemanticUtils.hpp"
#include "core/raycaster/Intersector2D.h"
#include "core/assets/Resources.hpp"
#include "core/assets/InputCamera.hpp"
#include "core/raycaster/CameraRaycaster.hpp"
#include "projects/ulr/renderer/ULRV3Renderer.hpp"
#include <opencv2/ximgproc/edge_filter.hpp>
#include "core/system/String.hpp"


#define MIN_COMPONENT_SIZE (300)
// Minimum bbox side size under which the blob in the mask is ignored, in fraction of the total mask size.
#define MIN_PIX_FRAC (32)

SemanticReflectionsScene::SemanticReflectionsScene(const std::string & basePath, int maxWidth, LoadMode mode, bool halfResDepth) : 
	_basePath(basePath), _halfResDepth(halfResDepth), _upVector(0.0f, 0.0f, 0.0f)
{
	
	/// Phase 1: compute the final mesh, compute the masks.

	// Cameras
	// Load the cameras, resize them if needed.
	_cameras = sibr::InputCamera::loadColmap(basePath + "/colmap/stereo/sparse/", 0.01f, 1000.0f);

	for(sibr::InputCamera::Ptr & camera : _cameras) {
		const std::string::size_type pos = camera->name().find_last_of('.');
		camera->name(camera->name().substr(0, pos));
	}
	_camerasFullSize = std::vector<sibr::InputCamera::Ptr>(_cameras);
	// Resize cameras.
	if (maxWidth > 0) {
		for (auto & cam : _cameras) {
			const float scale = std::min(1.0f, static_cast<float>(maxWidth) / cam->w());
			sibr::Vector2i size(cam->w(), cam->h());
			size = (size.cast<float>() * scale).cast<int>();
			cam->size(size[0], size[1]);
		}
	}
	_resourcesResolution = sibr::Vector2f(_cameras[0]->w(), _cameras[0]->h());

	// Meshes
	const std::string finalMeshPath = basePath + "/capreal/mesh_final.obj";
	const std::string finalWindowMeshPathOBJ = basePath + "/capreal/mesh_windows.obj";
	const std::string finalFinalMeshPath = basePath + "/capreal/mesh_final_retextured_cleaned.obj";
	
	if (!sibr::fileExists(finalMeshPath) || !sibr::fileExists(finalWindowMeshPathOBJ)) {
		generateFinalMeshes();
	}

	_inputMesh = sibr::Mesh::Ptr(new sibr::Mesh(true));
	// If the final cleaned up retextured version of the mesh exist, use it.
	if (sibr::fileExists(finalFinalMeshPath)) {
		_inputMesh->load(finalFinalMeshPath);
	} else {
		_inputMesh->load(finalMeshPath);
	}
	loadWindows(finalWindowMeshPathOBJ);
	upVector();
	
	// Clipping planes.
	// Make sure the cameras have the best possible clipping planes.
	updateClippingPlanes();

	// Window masks generation.
	const std::string windowMaskDirs = basePath + "/semantic_reflections/masks_per_window/";
	if (!sibr::directoryExists(windowMaskDirs)) {
		generatePerWindowMasks(_camerasFullSize);
	}
	// Regenerate masks if needed.
	const std::string partsMasksDirectory = basePath + "/semantic_reflections/masks_refined_parts/";
	if (!sibr::directoryExists(partsMasksDirectory)) {
		generateMasks(_camerasFullSize);
	}

	if(mode <= GEOMETRY_PREPROCESS) {
		return;
	}
	
	/// Phase 2: flow dumps, we need real camera parameters and images.

	// Load windows parameters sets.
	// For each ID from the window mesh above, we have a set of associated parameters.
	const std::string paramsFilePath = _basePath + "/semantic_reflections/reflections/parameters.txt";
	if (sibr::fileExists(paramsFilePath)) {
		std::ifstream paramsFile(paramsFilePath);
		if (paramsFile.is_open()) {
			_windowsParameters.clear();
			std::string line;
			while (std::getline(paramsFile, line)) {
				if (line.empty() || line[0] == '#') {
					continue;
				}
				const auto tokens = sibr::split(line, ' ');
				const int winId = std::stoi(tokens[0]);
				const float paramX = std::stof(tokens[1]);
				const float paramY = std::stof(tokens[2]);
				_windowsParameters.emplace_back();
				_windowsParameters.back().meshId = winId;
				_windowsParameters.back().radii = sibr::Vector2f(paramX, paramY);
			}
			paramsFile.close();
			std::cout << "[Window parameters] Loaded " << _windowsParameters.size() << " parameters sets." << std::endl;
		}
	} 

	// Load the images from disk.
	_inputImages.resize(_cameras.size());
	_partsImages.resize(_cameras.size());
		
	std::cout << "[SemanticScene] Loading input images and masks..." << std::flush;
	#pragma omp parallel for
	for (int cid = 0; cid < _cameras.size(); ++cid) {
		const auto & camera = *_cameras[cid];
		// Rectified images.
		{
			std::string cameraImageFilename = basePath + "/colmap/stereo/images/" + camera.name() + ".jpg";
			if (!sibr::fileExists(cameraImageFilename)) {
				cameraImageFilename = basePath + "/colmap/stereo/images/" + camera.name() + ".JPG";
			}
			sibr::ImageRGB cameraImage;
			cameraImage.load(cameraImageFilename, false, true);
			_inputImages[cid] = cameraImage.resized(camera.w(), camera.h());
		}

		// Parts masks.
		{
			const std::string partsImageFilename = partsMasksDirectory + camera.name() + "_mask_out.png";
			sibr::ImageL8 partsImage;
			partsImage.load(partsImageFilename, false, true);
			_partsImages[cid] = partsImage.resized(camera.w(), camera.h());
		}
	}
	std::cout << std::endl;

	std::cout << "[SemanticScene] " << "Loading color data." << std::endl;
	// Create textures and send them to the GPU.
	const int w = int(_resourcesResolution[0]);
	const int h = int(_resourcesResolution[1]);
	_partsTextures.reset(new sibr::Texture2DArrayLum(_partsImages, w, h));
	initWindowIDTextureArrays(w, h);

	const int wd = _halfResDepth ? w / 2 : w;
	const int hd = _halfResDepth ? h / 2 : h;
	initDepthTextureArrays(wd, hd, _inputMesh);

	_inputTextures.reset(new sibr::Texture2DArrayRGB(_inputImages, w, h, SIBR_GPU_LINEAR_SAMPLING));

	initMinBackgroundImages(w, h);

	if (mode <= FLOWS_PREPROCESS) {
		return;
	}

	/// Pase 3: rendering and comparisons.
	 
	// Load the reflection layers from disk.
	loadForegroundMinImages(_basePath + "/semantic_reflections/min_foregrounds/");

	// Load the texture.
	{
		sibr::ImageRGB inputTextureImg;
		if (sibr::fileExists(basePath + "/capreal/mesh_u1_v1.png")) {
			inputTextureImg.load(basePath + "/capreal/mesh_u1_v1.png");
		}
		else {
			inputTextureImg.load(basePath + "/capreal/mesh_it0_u1_v1.png");
		}
		_baseMeshTexture.reset(new sibr::Texture2DRGB(inputTextureImg, SIBR_GPU_LINEAR_SAMPLING));
	}

	{
		sibr::ImageRGB inputTextureImg;
		inputTextureImg.load(basePath + "/capreal/mesh_final_retextured_u1_v1.png");
		_inputMeshTexture.reset(new sibr::Texture2DRGB(inputTextureImg, SIBR_GPU_LINEAR_SAMPLING));
	}
	
	// Load the default scene for ULR comparison.
	_baseUlrMesh = sibr::Mesh::Ptr(new sibr::Mesh(true));
	_baseUlrMesh->load(basePath + "/capreal/mesh.obj");
}


void SemanticReflectionsScene::loadWindows(const std::string & finalWindowMeshPathOBJ)
{

	// Load the windows.
	_windows = sibr::Mesh::Ptr(new sibr::Mesh(true));
	_windows->load(finalWindowMeshPathOBJ);

	// Count the number of windows.
	const auto & uvs = _windows->texCoords();
	_numWindows = 0;
	for (const sibr::Vector2f & uv : uvs) {
		_numWindows = std::max(_numWindows, int(uv[0]));
	}
	// We found the max ID but we want the count.
	_numWindows += 1;
	std::cout << "[SemanticScene] Found " << _numWindows << " window components." << std::endl;


	// Obj can't store colors but we are using them to precompute each component centroid.	
	const auto & vertices = _windows->vertices();
	const auto & texcoords = _windows->texCoords();
	std::vector<sibr::Vector3f> colors(vertices.size());
	const auto compIndices = _windows->removeDisconnectedComponents();
	std::vector<int> windowIds;
	// Compute each component centroid.
	for (const auto & component : compIndices) {
		// if we don't even have MIN_COMPONENT_SIZE triangles, skip.
		if (component.size() < MIN_COMPONENT_SIZE) {
			continue;
		}
		sibr::Vector3f centerOfMass(0.0f, 0.0f, 0.0f);
		for (const int vid : component) {
			centerOfMass += vertices[vid];
		}
		if (!component.empty()) {
			centerOfMass /= float(component.size());
		}
		windowIds.push_back(int(texcoords[component[0]][0]));
		for (const int vid : component) {
			colors[vid] = centerOfMass;
		}
	}
	_windows->colors(colors);

	// Populate the window parameters with default values.
	for (const int id : windowIds) {
		_windowsParameters.emplace_back();
		_windowsParameters.back().meshId = id;
		_windowsParameters.back().radii = sibr::Vector2f(0.0f, 0.0f);
	}
}

void SemanticReflectionsScene::updateClippingPlanes()
{
	// Taken from old SIBR core.
	{
		const std::string clipping_planes_file_path = _basePath + "/clipping_planes.txt";
		if (!sibr::fileExists(clipping_planes_file_path)) {
			std::vector<sibr::Vector2f> nearsFars;
			sibr::CameraRaycaster::computeClippingPlanes(*_inputMesh, _cameras, nearsFars);

			std::ofstream file(clipping_planes_file_path, std::ios::trunc | std::ios::out);
			if (file) {
				for (const auto & nearFar : nearsFars) {
					file << nearFar[0] << ' ' << nearFar[1] << std::endl;
				}
				file.close();
			}
			else {
				SIBR_WRG << " Could not save file '" << clipping_planes_file_path << "'." << std::endl;

			}
		}
		std::ifstream zFile(clipping_planes_file_path);
		if (zFile.is_open()) {
			int cid = 0;
			float znear, zfar;
			while (zFile >> znear >> zfar) {
				_cameras[cid]->znear(znear);
				_cameras[cid]->zfar(zfar);
				++cid;
			}
			zFile.close();
		}
	}

}


size_t SemanticReflectionsScene::idForCameraName(const std::string & name) {
	for (size_t cid = 0; cid < _cameras.size(); ++cid) {
		if(_cameras[cid]->name() == name) {
			return cid;
		}
	}
	return 0;
}

sibr::BasicIBRScene::Ptr SemanticReflectionsScene::getIBRScene() {
	sibr::BasicIBRScene::Ptr scene(new sibr::BasicIBRScene());
	scene->proxies()->replaceProxyPtr(_inputMesh);
	scene->cameras()->setupCamerasFromExisting(_cameras);
	scene->data()->datasetType(sibr::IParseData::Type::COLMAP);
	return scene;
}

std::vector<uint> SemanticReflectionsScene::closestCameras(const sibr::InputCamera& cam, int idToExclude) const {

	
	const std::vector<sibr::InputCamera::Ptr> & cams = _cameras;

	std::vector<uint> out;

	// sort angle / dist combined
	struct camAng
	{
		camAng() {}
		camAng(float a, float d, int i) : ang(a), dist(d), id(i) {}
		float ang, dist;
		int id;
		static bool compare(const camAng & a, const camAng & b) { return a.dist < b.dist; }// a.ang / a.dist > b.ang / b.dist;}
	
	};

	
	std::vector<camAng> allAng;
	for (int id = 0; id < (int)cams.size(); ++id) {
		const auto & inputCam = *cams[id];
		float angle = sibr::dot(inputCam.dir(), cam.dir());
		// reject back facing 
		if (angle > 0.1f && inputCam.isActive() && id != idToExclude) {
			float dist = (inputCam.position() - cam.position()).norm();
			allAng.push_back(camAng(angle, dist, id));
		}
	}


	std::sort(allAng.begin(), allAng.end(), camAng::compare);

	for (int id = 0; id < (int)allAng.size(); ++id) {
		out.push_back(allAng[id].id);
	}


	return out;
}

sibr::Vector3f SemanticReflectionsScene::upVector()
{
	if(!_upVector.isNull()) {
		return _upVector;
	}
	// Average up vector.
	sibr::Vector3f avgCamUp(0.0f,0.0f,0.0f);
	for(const auto & camera : _cameras) {
		avgCamUp += camera->up();
	}
	avgCamUp /= float(_cameras.size());

	// Look at all the input mesh normals, keep them if they are in a 45� cone around the avg up.
	sibr::Vector3f avgNormal(0.0f, 0.0f, 0.0f);
	
	for (const sibr::Vector3f & n : _inputMesh->normals()) {
		if (n.dot(avgCamUp) > 0.7) {
			avgNormal += n;
		}
	}
	avgNormal.normalize();
	
	_upVector = avgNormal;
	return _upVector;
}

void SemanticReflectionsScene::updateDepthmaps(const DepthMapsSource source)
{
	const int wd = int(_halfResDepth ? _resourcesResolution[0] / 2.0f : _resourcesResolution[0]);
	const int hd = int(_halfResDepth ? _resourcesResolution[1] / 2.0f : _resourcesResolution[1]);
	//initDepthTextureArrays(wd, hd, _inputMesh);
	initDepthTextureArrays(wd, hd, source == ULR ? _baseUlrMesh : _inputMesh);
}

void SemanticReflectionsScene::initDepthTextureArrays(const unsigned int w, const unsigned int h, const sibr::Mesh::Ptr mesh)
{
	std::cout << "[SemanticScene] Updating input views depthmaps..." << std::flush;
	sibr::GLShader depthOnlyShader;
	depthOnlyShader.init("DepthOnly",
		sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("depthonly.vp")),
		sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("depthonly.fp")));


	sibr::RenderTargetLum32F depthRT(w, h, SIBR_GPU_LINEAR_SAMPLING);

	sibr::GLParameter proj;
	proj.init(depthOnlyShader, "proj");

	_depthTextures.reset(new sibr::Texture2DArrayLum32F(w, h, (uint)_inputImages.size(), SIBR_GPU_LINEAR_SAMPLING));

	for (uint i = 0; i < _inputImages.size(); i++) {
		glViewport(0, 0, w, h);

		depthRT.bind();
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		glDepthMask(GL_TRUE);

		depthOnlyShader.begin();
		proj.set(_cameras[i]->viewproj());
		mesh->render(true, false);
		depthOnlyShader.end();

		depthRT.unbind();

		glCopyImageSubData(
			depthRT.handle(), GL_TEXTURE_2D, 0, 0, 0, 0,
			_depthTextures->handle(), GL_TEXTURE_2D_ARRAY, 0, 0, 0, i,
			w, h, 1);
		CHECK_GL_ERROR;
	}
	CHECK_GL_ERROR;

	std::cout << "Done." << std::endl;
}

void SemanticReflectionsScene::initWindowIDTextureArrays(const unsigned int w, const unsigned int h)
{
	std::cout << "[SemanticScene] Updating input views window IDs..." << std::flush;
	sibr::GLShader uvIdShader;
	uvIdShader.init("UV ID Out",
		sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("window_uvoutpass.vert")),
		sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("window_uvoutpass.frag")));


	sibr::RenderTargetLum depthRT(w, h, 0);

	sibr::GLParameter MVP;
	MVP.init(uvIdShader, "MVP");

	_idTextures.reset(new sibr::Texture2DArrayLum(w, h, (uint)_inputImages.size(), SIBR_GPU_LINEAR_SAMPLING));

	for (uint i = 0; i < _inputImages.size(); i++) {
		glViewport(0, 0, w, h);

		depthRT.bind();
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		glDepthMask(GL_TRUE);

		uvIdShader.begin();
		MVP.set(_cameras[i]->viewproj());
		_windows->render(true, true);
		uvIdShader.end();

		depthRT.unbind();

		glCopyImageSubData(
			depthRT.handle(), GL_TEXTURE_2D, 0, 0, 0, 0,
			_idTextures->handle(), GL_TEXTURE_2D_ARRAY, 0, 0, 0, i,
			w, h, 1);
		CHECK_GL_ERROR;
	}
	CHECK_GL_ERROR;
	std::cout << "Done." << std::endl;
}

void SemanticReflectionsScene::initMinBackgroundImages(const int w, const int h)
{
	const std::string minBGDirectory = _basePath + "/semantic_reflections/min_backgrounds/";
	if (!sibr::directoryExists(minBGDirectory)) {
		sibr::makeDirectory(minBGDirectory);
		std::cout << "[SemanticScene] Generating min backgrounds for input views..." << std::flush;

		// We need a min ulr renderer.
		sibr::ULRV3Renderer minRenderer(cameras(), w, h, "ulr_v3_minmax", "ulr_v3", false);
		minRenderer.epsilonOcclusion() = 0.001f;
		minRenderer.flipRGBs() = true;
		minRenderer.clearDst() = true;
		
		sibr::Texture2DArrayRGB::Ptr sourceImgs = _inputTextures;

		// ULR-like min compositing.
		for (const auto & cam : cameras()) {
			// Use all the cameras.
			const auto sortedCameras = closestCameras(*cam);
			minRenderer.updateCameras(sortedCameras);

			sibr::RenderTargetRGB minRT(cam->w(), cam->h());
			minRenderer.process(*_inputMesh, *cam, minRT, sourceImgs, _depthTextures, false);

			sibr::ImageRGB minBgImg;
			minRT.readBack(minBgImg);
			minBgImg.save(minBGDirectory + cam->name() + ".png", false);

		}
		std::cout << "Done." << std::endl;
	}

	// Load the backgrounds from disk.
	std::cout << "[SemanticScene] Loading min backgrounds for input views..." << std::flush;
	std::vector<sibr::ImageRGB> bgMins(_cameras.size());
#pragma omp parallel for
	for (int cid = 0; cid < _cameras.size(); ++cid) {
		const auto & camera = *_cameras[cid];
		const std::string minBGFilename = minBGDirectory + camera.name() + ".png";
		sibr::ImageRGB minbgImg;
		minbgImg.load(minBGFilename, false, true);
		bgMins[cid] = minbgImg.resized(camera.w(), camera.h());
	}
	_minBGTextures.reset(new sibr::Texture2DArrayRGB(bgMins, w, h, SIBR_GPU_LINEAR_SAMPLING));
	std::cout << std::endl;
}

void SemanticReflectionsScene::loadForegroundMinImages(const std::string & outputDir)
{
	_minFGTextures.reset(new sibr::Texture2DArrayRGB(0, 0));
	if(sibr::directoryExists(outputDir)) {
		std::cout << "[SemanticScene] Loading min foregrounds for input views..." << std::flush;
		std::vector<sibr::ImageRGB> fgMins(_cameras.size());
	#pragma omp parallel for
		for (int cid = 0; cid < _cameras.size(); ++cid) {
			const auto & camera = *_cameras[cid];
			const std::string minFGFilename = outputDir + "/" + camera.name() + ".png";
			sibr::ImageRGB minfgImg;
			minfgImg.load(minFGFilename, false, true);
			fgMins[cid] = minfgImg.resized(camera.w(), camera.h());
		}
		_minFGTextures.reset(new sibr::Texture2DArrayRGB(fgMins, uint(_resourcesResolution[0]), uint(_resourcesResolution[1]), SIBR_GPU_LINEAR_SAMPLING));
		std::cout << std::endl;
	} else {
		SIBR_WRG << "[SemanticScene] No min foregrounds found." << std::endl;
		
	}

}


void SemanticReflectionsScene::generateMasks(const std::vector<sibr::InputCamera::Ptr> & fullSizeCameras)
{
	// For each reference view we want to generate the new mask.
	// We first render the background geometry for the depth test, and the window mesh in white.
	const std::string partsMasksDirectory = _basePath + "/semantic_reflections/masks_refined_parts/";
	sibr::makeDirectory(partsMasksDirectory);
	std::cout << "[SemanticScene] Generating parts masks from windows mesh..." << std::flush;

	sibr::GLShader masksShader;
	masksShader.init("Mask shader", sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flatcolor.vert"), sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flatcolor.frag"));
	sibr::GLuniform<int> masksColor;
	sibr::GLuniform<sibr::Matrix4f> masksMVP;
	masksColor.init(masksShader, "color");
	masksMVP.init(masksShader, "MVP");

	for (const auto & fullCam : fullSizeCameras) {
		sibr::RenderTargetLum maskRT(fullCam->w(), fullCam->h());
		maskRT.clear();
		maskRT.bind();
		glViewport(0, 0, maskRT.w(), maskRT.h());
		masksShader.begin();
		masksMVP.set(fullCam->viewproj());
		masksColor.set(255);

		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		_inputMesh->render(true, false);
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		_windows->render(true, true);
		maskRT.unbind();

		sibr::ImageL8 maskImg;
		maskRT.readBack(maskImg);
		maskImg.save(partsMasksDirectory + fullCam->name() + "_mask_out.png", false);

	}
	std::cout << "Done." << std::endl;
}

void SemanticReflectionsScene::generatePerWindowMasks(const std::vector<sibr::InputCamera::Ptr> & fullSizeCameras)
{
	// We first render the background geometry for the depth test, and the window mesh to get its IDs.
	const std::string partsMasksDirectory = _basePath + "/semantic_reflections/masks_per_window/";
	sibr::makeDirectory(partsMasksDirectory);
	std::cout << "[SemanticScene] Generating windows masks from mesh..." << std::flush;

	sibr::GLShader masksShader;
	masksShader.init("Mask shader", sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flatcolor.vert"), sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flatcolor.frag"));
	sibr::GLuniform<int> masksColor;
	sibr::GLuniform<int> windowId;
	sibr::GLuniform<sibr::Matrix4f> masksMVP;
	masksColor.init(masksShader, "color");
	masksMVP.init(masksShader, "MVP");
	windowId.init(masksShader, "windowId");

	for (const auto & fullCam : fullSizeCameras) {
		
		sibr::RenderTargetLum maskRT(fullCam->w(), fullCam->h());
		maskRT.clear();
		maskRT.bind();
		glViewport(0, 0, maskRT.w(), maskRT.h());
		masksShader.begin();
		masksMVP.set(fullCam->viewproj());
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		_inputMesh->render(true, false);

		for (const auto & win : parameters()) {
			masksColor.set(win.meshId + 1);
			windowId.set(win.meshId);
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			_windows->render(true, true);
		}
		maskRT.unbind();

		sibr::ImageL8 maskImg;
		maskRT.readBack(maskImg);
		maskImg.save(partsMasksDirectory + fullCam->name() + "_mask_out.png", false);

	}
	std::cout << "Done." << std::endl;
}

void SemanticReflectionsScene::generateFinalMeshes()
{
	std::cout << "[SemanticScene] Generating the final background and windows meshes..." << std::endl;
	
	// Load the smooth car meshes.
	std::vector<sibr::Mesh> cars;
	std::vector<Eigen::AlignedBox3f> carBboxes;
	std::vector<sibr::Mesh> windows;

	struct CorrespondingWindowPoints {
		sibr::Vector3f direct;
		sibr::Vector3f reflect;
		int id;
	};
	std::vector<CorrespondingWindowPoints> windowsRays;

	std::vector<sibr::Mesh> windowsFullFlipped;
	const auto files = sibr::listFiles(_basePath + "/semantic_reflections/shrinkwrap/", false, false, { "ply" });
	const std::string meshPrefix = "smooth_mesh_";
	const std::string meshSuffix = "_final.ply";

	// For each car.
	for (const auto & file : files) {
		if (file.size() <= meshPrefix.size() || file.substr(0, meshPrefix.size()) != meshPrefix || file.find(meshSuffix) == std::string::npos) {
			continue;
		}
		std::cout << "Processing mesh " << file << "..." << std::endl;
		const std::string carId = file.substr(meshPrefix.size(), 3);

		const std::string fullCarName = meshPrefix + carId + meshSuffix;
		const std::string fullCarFlippedName = meshPrefix + carId + "_final_flip_aligned.ply";
		const std::string fullCarFlippedAutoName = meshPrefix + carId + "_final_flip_aligned_auto.ply"; 
		const std::string hardMaskTextureName = "mrf_window_mask_" + carId + ".png";
		const std::string softMaskTextureName = "mrf_window_mask_filtered_" + carId + ".png";


		sibr::Mesh::Ptr fullCar(new sibr::Mesh(false));
		fullCar->load(_basePath + "/semantic_reflections/shrinkwrap/" + fullCarName);

		if (!fullCar->hasNormals()) {
			fullCar->generateNormals();
		}
		// We prefer to load the Meshlab aligned version, but fallback to our ICP mesh otherwise.
		sibr::Mesh::Ptr fullCarFlipped(new sibr::Mesh(false));
		if (sibr::fileExists(_basePath + "/semantic_reflections/shrinkwrap/" + fullCarFlippedName)) {
			fullCarFlipped->load(_basePath + "/semantic_reflections/shrinkwrap/" + fullCarFlippedName);
		} else {
			fullCarFlipped->load(_basePath + "/semantic_reflections/shrinkwrap/" + fullCarFlippedAutoName);
		}

		if (!fullCarFlipped->hasNormals()) {
			fullCarFlipped->generateNormals();
		}

		sibr::Mesh car;
		{
			std::vector<int> visible(fullCar->vertices().size(), 0);
			// Let's start by removing vertices that are not visible by any camera.
			sibr::Raycaster raycaster;
			raycaster.addMesh(*fullCar);
			const std::vector<sibr::Vector3f> carVerticesInit = fullCar->vertices();
			const size_t verticesCountInit = carVerticesInit.size();

			// Check for occlusions by raycasting from each camera and comparing the distance.

#pragma omp parallel for
			for (int vid = 0; vid < verticesCountInit; ++vid) {
				int visibilityCount = 0;
				for (const auto & cam : _cameras) {
					// Test visibility.
					const sibr::Vector3f rayDir = (carVerticesInit[vid] - cam->position()).normalized();
					const auto hit = raycaster.intersect(sibr::Ray(cam->position(), rayDir));
					if (!hit.hitSomething()) {
						continue;
					}
					const float refDist = (carVerticesInit[vid] - cam->position()).norm();
					if (refDist <= hit.dist() + 0.00001f) {
						// No occlusion, vertex is visible
						++visibilityCount;
					}
				}
				visible[vid] = visibilityCount > 12;// was 12 apart from caustics
			}
		
			// Filter so that one isolated non-visible vertex do not take all the surrounding triangles with it.
			std::vector<int> partiallyVisible(visible);
			for (int tid = 0; tid < fullCar->triangles().size(); ++tid) {
				const sibr::Vector3u & tri = fullCar->triangles()[tid];
				if (visible[tri[0]] || visible[tri[1]] || visible[tri[2]]) {
					partiallyVisible[tri[0]] = partiallyVisible[tri[1]] = partiallyVisible[tri[2]] = 1;
				}
			}

			car = fullCar->generateSubMesh([&partiallyVisible](int vid) { return partiallyVisible[vid]; });
			
		}
		const int verticesMainCar = int(car.vertices().size());
		{
			std::vector<int> visibleFlipped(fullCarFlipped->vertices().size(), 0);
			// Let's start by removing vertices that are not visible by any camera.
			sibr::Raycaster raycaster;
			auto carFlipped2 = fullCarFlipped->clone();
			carFlipped2->merge(*fullCarFlipped->invertedFacesMesh2());
			raycaster.addMesh(*carFlipped2);
			const std::vector<sibr::Vector3f> carVerticesInit = fullCarFlipped->vertices();
			const size_t verticesCountInit = carVerticesInit.size();

			// Check for occlusions by raycasting from each camera and comparing the distance.

#pragma omp parallel for
			for (int vid = 0; vid < verticesCountInit; ++vid) {
				int visibilityCount = 0;
				for (const auto & cam : _cameras) {
					// Test visibility.
					const sibr::Vector3f rayDir = (carVerticesInit[vid] - cam->position()).normalized();
					const auto hit = raycaster.intersect(sibr::Ray(cam->position(), rayDir));
					if (!hit.hitSomething()) {
						continue;
					}
					const float refDist = (carVerticesInit[vid] - cam->position()).norm();
					if (refDist <= hit.dist() + 0.00001f) {
						// No occlusion, vertex is visible
						++visibilityCount;
					}
				}
				visibleFlipped[vid] = visibilityCount < 20;
			}

			// Filter so that one isolated non-visible vertex do not take all the surrounding triangles with it.
			std::vector<int> partiallyVisible(visibleFlipped);
			for (int tid = 0; tid < fullCarFlipped->triangles().size(); ++tid) {
				const sibr::Vector3u & tri = fullCarFlipped->triangles()[tid];
				if (visibleFlipped[tri[0]] || visibleFlipped[tri[1]] || visibleFlipped[tri[2]]) {
					partiallyVisible[tri[0]] = partiallyVisible[tri[1]] = partiallyVisible[tri[2]] = 1;
				}
			}

			auto carFlipped = fullCarFlipped->generateSubMesh([&partiallyVisible](int vid) { return partiallyVisible[vid]; });
			car.merge(carFlipped);
		}

		// Cutting based on window mask.
		sibr::ImageL8 hardWindowMask;
		hardWindowMask.load(_basePath + "/semantic_reflections/shrinkwrap/" + hardMaskTextureName, false, true);
		// Remove small elements.
		{
			cv::Mat1b hardMaskSmall = cv::Mat1b(hardWindowMask.toOpenCV().clone());
			cv::Mat1i labels;
			const int labCount = cv::connectedComponents(hardMaskSmall, labels, 8, CV_32S);
			std::vector<int> counts(labCount, 0);
			std::vector<int> countsVal(labCount, 0);
			for (int ly = 0; ly < labels.rows; ++ly) {
				for (int lx = 0; lx < labels.cols; ++lx) {
					counts[labels(ly, lx)] += 1;
					countsVal[labels(ly, lx)] = hardMaskSmall(ly, lx);
				}
			}
			const int minSize = hardWindowMask.w() / MIN_PIX_FRAC;
			const int minCount = minSize * minSize;
			// Find elements to skip.
			for (int lid = 0; lid < labCount; ++lid) {
				// Ignore if small window element.
				if (counts[lid] < minCount && countsVal[lid] != 0) {
					counts[lid] = 0;
				}
			}
			// Fill them.
			for (int ly = 0; ly < labels.rows; ++ly) {
				for (int lx = 0; lx < labels.cols; ++lx) {
					if (counts[labels(ly, lx)] == 0) {
						hardMaskSmall(ly, lx) = 0;
					}
				}
			}
			hardWindowMask.fromOpenCV(hardMaskSmall);
		}
		
		// Apply guided fitler on the input mask.
		sibr::ImageL8 softWindowMask;
		sibr::ImageRGB refTexture;
		refTexture.load(_basePath + "/semantic_reflections/shrinkwrap/" + "texture_" + carId + ".png", false, true);
		cv::Mat1b softMaskOut;
		cv::Mat1b hardMaskIn = cv::Mat1b(hardWindowMask.toOpenCV().clone());
		cv::erode(hardMaskIn, hardMaskIn, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5,5)), cv::Point(-1, -1), 1);
		
		const cv::Mat3b rgbImgIn = cv::Mat3b(refTexture.toOpenCV());
		cv::ximgproc::guidedFilter(rgbImgIn, hardMaskIn, softMaskOut, 9, 5.0f);
		softWindowMask.fromOpenCV(softMaskOut);
		
		const std::vector<sibr::Vector2f> maskUvs = car.texCoords();
		const size_t verticesCount = maskUvs.size();
		const sibr::Vector2f resolutionMask = hardWindowMask.size().cast<float>();
		cv::Mat1b hardMaskLarge = cv::Mat1b(hardWindowMask.toOpenCV().clone());
		cv::dilate(hardMaskLarge, hardMaskLarge, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)), cv::Point(-1, -1), 1);
		sibr::ImageL8 largeMask;
		largeMask.fromOpenCV(hardMaskLarge);

		std::vector<float> windowMask(verticesCount, 0.0f);
#pragma omp parallel for
		for(int vid = 0; vid < verticesCount; ++vid) {
			windowMask[vid] = float(largeMask.bilinear(maskUvs[vid].cwiseProduct(resolutionMask))[0])/255.0f;
		}

		// Detect vertices that are at the boundary between the windows and the hull.
		std::vector<bool> isWindow(verticesCount, true);
		std::vector<bool> isOnEdge(verticesCount, false);
		for (int tid = 0; tid < car.triangles().size(); ++tid) {
			const sibr::Vector3u & tri = car.triangles()[tid];
			const bool isWindow0 = windowMask[tri[0]] > 0.25f;
			const bool isWindow1 = windowMask[tri[1]] > 0.25f;
			const bool isWindow2 = windowMask[tri[2]] > 0.25f;
			if (!isWindow0 || !isWindow1 || !isWindow2) {
				isWindow[tri[0]] = isWindow[tri[1]] = isWindow[tri[2]] = false;
			}
			const int sum = int(isWindow0) + int(isWindow1) + int(isWindow2);
			if(sum > 0 && sum < 3) {
				// Some are, some aren't.
				// Those that are window are on the edge.
				if(isWindow0) {
					isOnEdge[tri[0]] = true;
				}
				if (isWindow1) {
					isOnEdge[tri[1]] = true;
				}
				if (isWindow2) {
					isOnEdge[tri[2]] = true;
				}
			}
		}
		const auto & tris = car.triangles();
		const int trianglesCount = int(tris.size());
		// Laplacian smoothing on the edge before cutting anything.
		for(int iit = 0; iit < 20; ++iit) {
			std::vector<sibr::Vector3f> newPos(car.vertices());
#pragma omp parallel for
			for (int vid = 0; vid < verticesCount; ++vid) {
				if (isOnEdge[vid]) {
					sibr::Vector3f avgPos(0.0f, 0.0f, 0.0f);
					int countPos = 0;
					for (int tid = 0; tid < trianglesCount; ++tid) {
						const sibr::Vector3u & tri = tris[tid];
						if (tri[0] == vid || tri[1] == vid || tri[2] == vid) {
							for (int ovid = 0; ovid < 3; ++ovid) {
								if (isOnEdge[tri[ovid]]) {
									avgPos += car.vertices()[tri[ovid]];
									++countPos;
								}
							}
						}
					}
					avgPos /= float(countPos);
					const float lambda = 1.0f;
					newPos[vid] = (1.0f - lambda)*car.vertices()[vid] + lambda*avgPos;
				}
			}
			car.vertices(newPos);
		}
		// Split the car into windows and hull.
		auto carNoWindows = car.generateSubMesh([&isWindow](int vid) { return !isWindow[vid]; });
		auto onlyWindow = car.generateSubMesh([&windowMask, verticesMainCar](int vid) { return windowMask[vid]> 0.25f && vid < verticesMainCar; });
		
		std::vector<sibr::Vector2f> newUVs(onlyWindow.vertices().size());
		for(int vid = 0; vid < onlyWindow.vertices().size(); ++vid) {
			newUVs[vid][0] = -1.0f;
			newUVs[vid][1] = float(softWindowMask.bilinear(onlyWindow.texCoords()[vid].cwiseProduct(resolutionMask))[0]) / 255.0f;
		}
		onlyWindow.texCoords(newUVs);

		cars.push_back(carNoWindows);
		windows.push_back(onlyWindow);
		carBboxes.push_back(carNoWindows.getBoundingBox());

	}
	
	// Merge all windows, compute connected components ID and centroids.
	sibr::Mesh::Ptr allWindows(new sibr::Mesh(false));
	for (const auto & window : windows) {
		allWindows->merge(window);
	}


	{

		allWindows->generateNormals();
		const auto & vertices = allWindows->vertices();
		std::vector<sibr::Vector2f> uvs(allWindows->texCoords());
		const auto compIndices = allWindows->removeDisconnectedComponents();
		
		unsigned int compId = 0;
		for (const auto & component : compIndices) {
			// if we don't even have MIN_COMPONENT_SIZE triangles, skip.
			if (component.size() < MIN_COMPONENT_SIZE) {
				continue;
			}
			std::cout << "Component #" << compId << " with " << component.size() << " vertices." << std::endl;

			for (const int vid : component) {
				uvs[vid][0] = float(compId);
			}
			++compId;
		}
		allWindows->texCoords(uvs);

		std::cout << "[SemanticScene] Windows saved." << std::endl;
	}

	// Load the global colmap mesh.
	sibr::Mesh::Ptr sceneMesh(new sibr::Mesh(false));
	sceneMesh->load(_basePath + "/capreal/mesh.obj");
	
	if (!sceneMesh->hasNormals()) {
		sceneMesh->generateNormals();
	}
	// Merge the smooth hollow cars with the background, cleaning up intersection.
	sibr::Mesh finalCars(false);
	for (const auto & car : cars) {
		finalCars.merge(car);
	}
	const Eigen::AlignedBox3f carsBbox = allWindows->getBoundingBox().extend(finalCars.getBoundingBox());
	const Eigen::AlignedBox3f finalCarsBox(carsBbox.center() - 1.1f * 0.5f * carsBbox.sizes(), carsBbox.center() + 1.1f * 0.5f * carsBbox.sizes());


	sibr::Mesh::Ptr backgroundMesh;

	{
		// then remove the car hull from the background.
		const std::vector<sibr::Vector3f> & sceneVertices = sceneMesh->vertices();
		const size_t verticesSceneCount = sceneVertices.size();
		std::vector<int> shouldKeep(verticesSceneCount, 1);

#pragma omp parallel for
		for (int vid = 0; vid < verticesSceneCount; ++vid) {
			const sibr::Vector3f & cv = sceneVertices[vid];
			// Vertex is too far, keep it in all cases.
			if (!finalCarsBox.contains(cv)) {
				continue;
			}
			// Discard a vertex background if it is quite close to a window, or very close to the car hull.
			const int ocount = int(allWindows->vertices().size());
			for (int oid = 0; oid < ocount; ++oid) {
				const sibr::Vector3f & ov = allWindows->vertices()[oid];
				const float norm = (ov - cv).norm();
				if (norm < 0.02f) {
					shouldKeep[vid] = 0;
					break;
				} 
				if (norm < 0.05f && allWindows->normals()[oid].dot(cv - ov)/norm > 0.5f) {
					// if the norm is larger, but we are on the outside (point - surface dot normal > 0).
					shouldKeep[vid] = 0;
					break;
				}
			}
			if(shouldKeep[vid] == 0) {
				continue;
			}
			for (const sibr::Vector3f & ov : finalCars.vertices()) {
				if ((ov - cv).norm() < 0.01f) {
					shouldKeep[vid] = 0;
					break;
				}
			}
			
		}

		// Don't delete vertices of triangles that are on the edge (at least one vertex to keep -> keep the three vertices)
		
		std::vector<int> shouldReallyKeep(shouldKeep);
		const std::vector<sibr::Vector3u> & sceneTris = sceneMesh->triangles();
		for (int tid = 0; tid < sceneTris.size(); ++tid) {
			const sibr::Vector3u & tri = sceneTris[tid];
			if (shouldKeep[tri[0]] || shouldKeep[tri[1]] || shouldKeep[tri[2]]) {
				shouldReallyKeep[tri[0]] = shouldReallyKeep[tri[1]] = shouldReallyKeep[tri[2]] = 1;
			}
		}
		shouldKeep = std::vector<int>(shouldReallyKeep);
		
		backgroundMesh = sceneMesh->generateSubMesh([&shouldKeep](int vid)
		{	// kept if far enough from smooth
			return shouldKeep[vid];
		}).clone();

		finalCars.makeWhole();
		backgroundMesh->makeWhole();
		backgroundMesh->merge(finalCars);

		// Local up vector.
	
	}
	allWindows->saveToObj(_basePath + "/capreal/mesh_windows.obj");
	backgroundMesh->saveToObj(_basePath + "/capreal/mesh_final.obj");

	std::cout << "[SemanticScene] Merging Done." << std::endl;
}


const std::vector<sibr::InputCamera::Ptr> & SemanticReflectionsScene::cameras() { return _cameras; }

const std::vector<sibr::InputCamera::Ptr> & SemanticReflectionsScene::fullSizeCameras() { return _camerasFullSize; }

sibr::Mesh::Ptr  SemanticReflectionsScene::inputMesh() const { return _inputMesh; }

sibr::Texture2DRGB::Ptr SemanticReflectionsScene::inputMeshTexture() const { return _inputMeshTexture; }

sibr::Texture2DRGB::Ptr SemanticReflectionsScene::baseMeshTexture() const { return _baseMeshTexture; }

sibr::Mesh::Ptr SemanticReflectionsScene::windows() const { return _windows; }

sibr::Mesh::Ptr SemanticReflectionsScene::baseUlr() const { return _baseUlrMesh; }

const std::vector<sibr::ImageRGB> & SemanticReflectionsScene::inputImages() const { return _inputImages; }

const std::vector<sibr::ImageL8> & SemanticReflectionsScene::partsImages() const { return _partsImages; }

const sibr::Texture2DArrayRGB::Ptr SemanticReflectionsScene::inputTextures() const { return _inputTextures; }

const sibr::Texture2DArrayLum::Ptr SemanticReflectionsScene::partsTextures() const { return _partsTextures; }

const sibr::Texture2DArrayLum32F::Ptr SemanticReflectionsScene::depthTextures() const { return _depthTextures; }

const sibr::Texture2DArrayLum::Ptr SemanticReflectionsScene::idTextures() const { return _idTextures; }

const sibr::Texture2DArrayRGB::Ptr SemanticReflectionsScene::minBGTextures() const { return _minBGTextures; }

const sibr::Texture2DArrayRGB::Ptr SemanticReflectionsScene::minFGTextures() const { return _minFGTextures; }

const std::string & SemanticReflectionsScene::basePath() const { return _basePath; }

const sibr::Vector2f & SemanticReflectionsScene::resolution() const {
	return _resourcesResolution;
}

int SemanticReflectionsScene::numWindows() const {
	return _numWindows;
}

const std::vector<SemanticReflectionsScene::WindowParameters> & SemanticReflectionsScene::parameters() const {
	return _windowsParameters;
};