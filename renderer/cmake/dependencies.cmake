# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


# libigl
sibr_addlibrary(NAME Libigl
    MSVC11 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/libigl.zip"
    MSVC12 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/libigl.zip"
    MSVC14 "https://repo-sam.inria.fr/fungraph/dependencies/sibr/~0.9/libigl.zip"    # TODO SV: provide a valid version if required
    SET CHECK_CACHED_VAR Libigl_DIR PATH ${Libigl_WIN3RDPARTY_DIR}
)
find_package(Libigl REQUIRED)

include_directories(${Libigl_INCLUDE_DIR})
message("libigl was found in : ${Libigl_INCLUDE_DIR}")