/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SemanticReflectionsView.hpp"
#include "core/system/String.hpp"


SemanticReflectionsView::SemanticReflectionsView(const SemanticReflectionsScene::Ptr scene, const unsigned int w, const unsigned int h) : ViewBase(w, h){
	_scene = scene;
	initSemanticReflectionsView(w, h);
}

SemanticReflectionsView::SemanticReflectionsView(const SemanticReflectionsScene::Ptr scene, const sibr::DeepBlendingScene::Ptr dbScene, const unsigned int w, const unsigned int h) : ViewBase(w, h){
	_dbScene = dbScene;
	_scene   = scene;
	initSemanticReflectionsView(w, h);
}

void SemanticReflectionsView::initShaders(bool reset) {
	// Copy and update all shaders on the fly (for live reloading).
	sibr::copyFile("../../src/projects/semantic_reflections/renderer/shaders/reflections_multi.frag", sibr::getShadersDirectory("semantic_reflections") + "/reflections_multi.frag", true);
	sibr::copyFile("../../src/projects/semantic_reflections/renderer/shaders/reflections_base.vert", sibr::getShadersDirectory("semantic_reflections") + "/reflections_base.vert", true);
	sibr::copyFile("../../src/projects/semantic_reflections/renderer/shaders/ulr_v3_minmax.frag", sibr::getShadersDirectory("semantic_reflections") + "/ulr_v3_minmax.frag", true);
	sibr::copyFile("../../src/projects/semantic_reflections/renderer/shaders/reflections_composite.frag", sibr::getShadersDirectory("semantic_reflections") + "/reflections_composite.frag", true);
	sibr::copyFile("../../src/projects/semantic_reflections/renderer/shaders/reflections_composite.vert", sibr::getShadersDirectory("semantic_reflections") + "/reflections_composite.vert", true);

	const sibr::Vector2f flowIntensityBack = reset ? sibr::Vector2f(1.0f, 1.0f) : _flowIntensity;
	const float alphaOutBack = reset ? 0.75f : _alphaOut;
	const float mixFullAndDiffBack = reset ? 0.0f : _mixFullAndDiff;
	const bool useMinCompoBack = reset ? true : _useMinCompo;
	const bool applyBackgroundMinBack = reset ? true : _applyBackgroundMin;
	const int windowIdBack = reset ? -1 : _windowId;
	const int outputFlowBack = reset ? false : _outputFlow;
	const float panComparisonBack = reset ? 1.0f : _panComparison;
	const bool stricterMasksBack = reset ? false : _stricterMasks;

	sibr::GLShader::Define::List defines;
	defines.emplace_back("NUM_CAMS", _scene->cameras().size());
	_flowShader.init("Flow shader", sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_base.vert"),
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_multi.frag", defines));

	_paramMVP.init(_flowShader, "MVP");

	_sphereRadius.init(_flowShader, "sphereRadius");
	_sphereCenter.init(_flowShader, "sphereCenter");
	_currentPos.init(_flowShader, "currentPos");

	_camsCount.init(_flowShader, "camsCount");
	_flowIntensity.init(_flowShader, "flowIntensity");
	_mixFullAndDiff.init(_flowShader, "mixFullAndDiff");
	_panComparison.init(_flowShader, "panComparison");
	_useMinCompo.init(_flowShader, "useMinCompo");
	_applyBackgroundMin.init(_flowShader, "applyBackgroundMin");
	_windowId.init(_flowShader, "windowId");
	_outputFlow.init(_flowShader, "outputFirstValidFlow");
	_stricterMasks.init(_flowShader, "stricterMasks");

	_flowIntensity = flowIntensityBack;
	
	_mixFullAndDiff = mixFullAndDiffBack;
	_panComparison = panComparisonBack;
	_camsCount = int(_scene->cameras().size());
	_useMinCompo = useMinCompoBack;
	_applyBackgroundMin = applyBackgroundMinBack;
	_windowId = windowIdBack;
	_outputFlow = outputFlowBack;
	_stricterMasks = stricterMasksBack;

	sibr::GLuniform<sibr::Vector3f> sceneUpVector;
	sceneUpVector.init(_flowShader, "sceneUpVector");
	sceneUpVector = _scene->upVector();
	_flowShader.begin();
	sceneUpVector.send();
	_flowShader.end();

	_finalCompo.init("Compo shader", sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_composite.vert"),
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_composite.frag"));
	_alphaOut.init(_finalCompo, "alphaOut");
	_mvpFinal.init(_finalCompo, "MVP");
	_alphaOut = alphaOutBack;

	_backgroundCompo.init("BG compo shader",
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/bg_compo.vert"),
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/bg_compo.frag", defines));

}

void SemanticReflectionsView::onUpdate(sibr::Input& input, const sibr::Viewport& vp) {
	if(input.key().isReleased(sibr::Key::F8)) {
		initShaders(false);
	}
}

void SemanticReflectionsView::updateCameras(const std::vector<uint> & camIds) {
	// Reset all cameras.
	for (auto & caminfos : _cameraInfos) {
		caminfos.selected = 0;
	}
	// Enabled the ones passed as indices.
	for (const auto & camId : camIds) {
		_cameraInfos[camId].selected = 1;
	}

	// Update the content of the UBO.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraUBOInfos)*(_scene->cameras().size()), &_cameraInfos[0]);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}


void SemanticReflectionsView::onGUI()
{
	if(ImGui::Begin("Reflections controls")) {
		ImGui::Checkbox("Show background only (ULR+DB)", &_onlyBackground);
		const ComparisonMode oldMode = _compMode;
		if(ImGui::Combo("Comparison", (int*)&_compMode, "Ours\0ULR\0Textured\0\0")) {
			if(oldMode == OURS && _compMode == ULR) {
				_scene->updateDepthmaps(SemanticReflectionsScene::ULR);
			} else if (oldMode == ULR && _compMode == OURS) {
				_scene->updateDepthmaps(SemanticReflectionsScene::FINAL);
			}
			// Reset export ID.
			_imgId = 0;
		}

		ImGui::Separator();
		if (_dbScene != NULL) {

#if defined(TF_INTEROP)
			ImGui::Checkbox("Use deep blending", &_dbScene->get_tf_blending());
			_dbScene->get_patch_cloud().use_tf_blending(_dbScene->get_tf_blending());
#endif
		}

		std::string selectedFile;
		if (ImGui::Button("Export path")) {
			std::vector<sibr::InputCamera::Ptr> cameras;
			if (sibr::showFilePicker(selectedFile, sibr::FilePickerMode::Default)) {
				if (!selectedFile.empty()) {
					SIBR_LOG << "Loading" << std::endl;
					cameras.clear();
					if (sibr::getExtension(selectedFile) == "out") {
						cameras = sibr::InputCamera::loadBundleFRIBR(selectedFile, 0.01f, 1000.0f, "output_deepblending/");
					}
					else {
						SIBR_ERR << "Path format not supported!! " << std::endl;
					}
				}
			}

			exportPathDBBG(cameras, selectedFile);
		}

		ImGui::Separator();
		
		ImGui::Checkbox("Save replay", &_saveReplay);
		ImGui::Checkbox("Leave one out", &_leaveOneOut); 
		if (_leaveOneOut) {
			ImGui::SameLine();
			if (ImGui::InputInt("Excluded", &_leaveOneOutID, 1, 10)) {
				_leaveOneOutID = sibr::clamp(_leaveOneOutID, 0, int(_scene->cameras().size())-1);
			}
			if (_leaveOneOutID > 0) {
				const std::string & name = _scene->cameras()[_leaveOneOutID]->name();
				ImGui::Text("Camera %d named \"%s\".", _leaveOneOutID, name.c_str());
			}
		}

		if (ImGui::Button("Avg path distance")) {
			float avgDist = 0.0f;
			const int w = _scene->cameras()[0]->w();
			const int h = _scene->cameras()[0]->h();
			const auto & cams = _handler->getCameraRecorder().cams();
			float maxi = 0.0f;
			for (const auto & cam : cams) {
				const sibr::InputCamera cami(cam, w, h);
				const auto & ocam = *_scene->cameras()[_scene->closestCameras(cami)[0]];
				const float dist = (ocam.position() - cam.position()).norm();
				avgDist += dist;
				maxi = std::max(maxi, dist);
			}
			avgDist /= cams.size();
			std::cout << "Avg distance " << avgDist << ", max distance " << maxi << std::endl;
		}
		ImGui::SameLine();
		if (ImGui::Button("Min cam distance")) {
			const auto & ocam = *_scene->cameras()[_scene->closestCameras(_currentCam)[0]];
			const float dist = (ocam.position() - _currentCam.position()).norm();
			
			std::cout << "Closest cam distance " << dist << std::endl;
		}

		if(ImGui::Button("Timings")) {
			const int cfid = (_fid+1)%2;

			std::vector<GLuint64> times(_queries[cfid].size(), 0);
			for(int i = 0; i < times.size(); ++i) {
				glGetQueryObjectui64v(_queries[cfid][i], GL_QUERY_RESULT, &times[i]);
			}

			std::cout << "Min BG: " << times[0] << "ns." << std::endl;
			std::cout << "DB+ULR BG: " << times[1] << "ns." << std::endl;
			std::cout << "Reflections: " << times[2] << "ns." << std::endl;
			std::cout << "Poisson: " << times[3] << "ns." << std::endl;
			std::cout << "Final blit: " << times[4] << "ns." << std::endl;
			
		}
	}
	ImGui::End();

	
}

void SemanticReflectionsView::initSemanticReflectionsView(const unsigned int w, const unsigned int h)
{

	// For each camera we store a matrix, 2 vecs3, 2 floats (including padding).
	const unsigned int bytesPerCamera = 4 * (16 + 2 * 3 + 2);

	const int camsCountMax = int(_scene->cameras().size());

	// Populate the cameraInfos array (will be uploaded to the GPU).
	_cameraInfos.clear();
	_cameraInfos.resize(camsCountMax);
	for (size_t i = 0; i < camsCountMax; ++i) {
		const auto & cam = *_scene->cameras()[i];
		_cameraInfos[i].vp = cam.viewproj();
		_cameraInfos[i].pos = cam.position();
		_cameraInfos[i].dir = cam.dir();
		_cameraInfos[i].selected = cam.isActive();
	}
	// Create UBO.
	_uboIndex = 0;
	glGenBuffers(1, &_uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraUBOInfos)*camsCountMax, &_cameraInfos[0], GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// Scene bounding sphere.
	const auto & bbox = _scene->inputMesh()->getBoundingBox();
	_sphereRadius = bbox.diagonal().norm()*0.5f;
	_sphereCenter = _scene->windows()->getBoundingBox().center();

	// Renderers.
	_bgRenderer.reset(new sibr::ULRV3Renderer(_scene->cameras(), w, h, "ulr/ulr_v3_fast", "ulr/ulr_v3", false));
	_bgRenderer->flipRGBs() = true;
	_poissonRenderer.reset(new sibr::PoissonRenderer(w, h));
	_poissonRenderer->enableFix() = true;

	// Framebuffers.
	_minBGTexture.reset(new sibr::RenderTargetRGB(w, h, SIBR_GPU_LINEAR_SAMPLING));
	_reflectionRT.reset(new sibr::RenderTargetRGB(w, h, SIBR_GPU_LINEAR_SAMPLING));
	_finalRT.reset(new sibr::RenderTargetRGBA(w, h, SIBR_GPU_LINEAR_SAMPLING));
	_ulrTexture.reset(new sibr::RenderTargetRGB(w, h, SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS));
	_deepBlendingTexture.reset(new sibr::RenderTargetRGB(w, h, SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS));
	
	initShaders(true);

	if (!_scene->parameters().empty()) {
		_windowId = _scene->parameters()[0].meshId;
	}

	_reflectionsViewCount = camsCountMax / 2;
	_backgroundViewCount = camsCountMax / 2;

	if (!_scene->minFGTextures() || _scene->minFGTextures()->depth() == 0) {
		_useMinCompo = false;
	}
	_queries.resize(2);
	_queries[0] = std::vector<GLuint>(5);
	_queries[1] = std::vector<GLuint>(5);
	glGenQueries(GLsizei(_queries[0].size()), &_queries[0][0]);
	glGenQueries(GLsizei(_queries[1].size()), &_queries[1][0]);
}

void SemanticReflectionsView::renderWarpedReflection(const unsigned int cid, const std::vector<unsigned int> & camerasEnabled, const int windowId, const float paramX, const float paramY, sibr::IRenderTarget & dst) {

	_flowIntensity.get()[0] = paramX;
	_flowIntensity.get()[1] = paramY;
	_mixFullAndDiff = 0.0f; 
	_panComparison = 1.0;
	_useMinCompo = false;
	_applyBackgroundMin = false;
	_windowId = windowId;
	_outputFlow = false;
	_stricterMasks = true;
	updateCameras(camerasEnabled);

	// Prepare cameras.
	const sibr::InputCamera & eye = *_scene->cameras()[cid];

	dst.clear();
	dst.bind();
	glViewport(0, 0, dst.w(), dst.h());


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation(GL_FUNC_ADD);
	
	_flowShader.begin();
	_paramMVP.set(eye.viewproj());
	_sphereCenter.send();
	_sphereRadius.send();
	_currentPos.set(eye.position());
	_flowIntensity.send();
	_panComparison.send();
	_mixFullAndDiff.send();
	_camsCount.send();
	_useMinCompo.send();
	_applyBackgroundMin.send();
	_windowId.send();
	_outputFlow.send();
	_stricterMasks.send();

	// Bind UBO to shader.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, _uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	glActiveTexture(GL_TEXTURE0);
	const GLuint srcHandle =_scene->inputTextures()->handle();
	glBindTexture(GL_TEXTURE_2D_ARRAY, srcHandle);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->depthTextures()->handle());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->partsTextures()->handle());
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->minBGTextures()->handle());
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, _minBGTexture->handle());
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, _bgRenderer->depthHandle());
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->idTextures()->handle());

	_scene->windows()->render(true, true);
	
	_flowShader.end();
	glDisable(GL_BLEND);
	dst.unbind();
	CHECK_GL_ERROR;
}

void SemanticReflectionsView::renderAllReflections(const sibr::InputCamera & eye, const bool outputFlow, const bool useMinForeground) {
	// Setup everything before the loop to minize state changes.
	
	_flowShader.begin();
	_paramMVP.set(eye.viewproj());
	_sphereCenter.send();
	_sphereRadius.send();
	_currentPos.set(eye.position());

	_mixFullAndDiff.send();
	_camsCount.send();
	_useMinCompo.send();
	_applyBackgroundMin.send();
	_outputFlow = outputFlow;
	_outputFlow.send();
	_panComparison.send();
	_stricterMasks.send();

	// Bind UBO to shader.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, _uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, (useMinForeground ? _scene->minFGTextures() : _scene->inputTextures())->handle());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->depthTextures()->handle());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->partsTextures()->handle());
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->minFGTextures()->handle());
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, _minBGTexture->handle());
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, _bgRenderer->depthHandle());
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->idTextures()->handle());


	if (_scene->parameters().empty() || _windowId == -1 || _customRadii) {
		_flowIntensity.send();
		_windowId.send();
		_scene->windows()->render(true, true);
	} else {
		// Iterate and render each window separately.
		for (const auto & win : _scene->parameters()) {
			_flowIntensity = win.radii;
			_windowId = win.meshId;
			_flowIntensity.send();
			_windowId.send();
			_scene->windows()->render(true, true);
		}
	}

	_flowShader.end();
}

void SemanticReflectionsView::exportPathDBBG(const std::vector<sibr::InputCamera::Ptr> cams, std::string path)
{
	for (int i = 2; i < cams.size(); i++) {
		sibr::InputCamera cam = *cams[i];
		sibr::ImageRGBA currDBImg, blendImg;
		std::string inpImgPath = sibr::parentDirectory(path) + "/output_deepblending/" + cam.name();
		if (sibr::fileExists(inpImgPath)) {
			currDBImg.load(inpImgPath);
			sibr::Texture2DRGBA::Ptr inpTexture;
			inpTexture.reset(new sibr::Texture2DRGBA(currDBImg, SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS | SIBR_FLIP_TEXTURE));
			sibr::RenderTargetRGBA dest(currDBImg.w(), currDBImg.h());
			onRenderIBRDB(dest, cam, inpTexture);
			dest.readBack(blendImg);
			if (!_onlyBackground) {
				blendImg.save(sibr::parentDirectory(path) + "/output_semantic/" + cam.name());
			}
			else {
				blendImg.save(sibr::parentDirectory(path) + "/output_deepblending_ref/" + cam.name());
			}
		}
		else {
			SIBR_LOG << inpImgPath << " does not exist!" << std::endl;
		}
	}
}

void SemanticReflectionsView::onRenderIBRDB(sibr::IRenderTarget & dst, const sibr::Camera & eye, const sibr::Texture2DRGBA::Ptr inpTexture)
{
	_currentCam = sibr::InputCamera(eye, dst.w(), dst.h());
	
	const auto sortedCameras = _scene->closestCameras(_currentCam, _leaveOneOut ? _leaveOneOutID : -1);

	if (_backgroundViewCount > 0) {
		const int neighCount = std::min(_backgroundViewCount, int(sortedCameras.size()));
		std::vector<uint> pickedCams(neighCount);
		for (int i = 0; i < neighCount; ++i) {
			pickedCams[i] = sortedCameras[i];
		}
		_bgRenderer->updateCameras(pickedCams);
	}


	std::vector<uint> pickedCams;
	if (_reflectionsViewCount > 0) {
		const int neighCount = std::min(_reflectionsViewCount, int(sortedCameras.size()));
		for (int i = 0; i < neighCount; ++i) {
			pickedCams.push_back(sortedCameras[i]);
		}
	}
	else {
		pickedCams = sortedCameras;
	}
	updateCameras(pickedCams);

	_debugView->updateActiveCams(pickedCams);
	std::vector<bool> flags(_scene->getIBRScene()->cameras()->inputCameras().size(), false);
	for(const auto id : pickedCams) {
		flags[id] = true;
	}
	_scene->getIBRScene()->cameras()->usedCameraForRendering(flags);


	if (dst.w() != _minBGTexture->w() || dst.h() != _minBGTexture->h()) {
		_minBGTexture.reset(new sibr::RenderTargetRGB(dst.w(), dst.h(), SIBR_GPU_LINEAR_SAMPLING));
		_ulrTexture.reset(new sibr::RenderTargetRGB(dst.w(), dst.h(), SIBR_GPU_LINEAR_SAMPLING));
		_bgRenderer->resize(dst.w(), dst.h());
	}

	// Shortpath for baseline comparison.
	if (_compMode == ULR) {
		dst.clear();
		glViewport(0, 0, dst.w(), dst.h());
		_bgRenderer->backfaceCull() = true;
		_bgRenderer->process(*_scene->baseUlr(), eye, dst, _scene->inputTextures(), _scene->depthTextures(), true);
		_bgRenderer->backfaceCull() = false;
		_texturedRenderer.process(*_scene->baseUlr(), eye, _scene->baseMeshTexture()->handle(), dst, false);
		handleSaving(dst);
		return;
	}
	if (_compMode == TEXTURED) {
		dst.clear();
		glViewport(0, 0, dst.w(), dst.h());
		_texturedRenderer.process(*_scene->baseUlr(), eye, _scene->baseMeshTexture()->handle(), dst, false);
		handleSaving(dst);
		return;
	}


	// Start by rendering the min BG interior using ULR.
	glViewport(0, 0, _minBGTexture->w(), _minBGTexture->h());
	_minBGTexture->clear();
	_bgRenderer->process(*_scene->inputMesh(), eye, *_minBGTexture, _scene->minBGTextures(), _scene->depthTextures());
	// Debug show.
	if (_showMinRender) {
		sibr::blit(*_minBGTexture, dst);
		return;
	}

	// Then, render regular background ULR and textured mesh.
	//_bgTexture->clear();
	glViewport(0, 0, _ulrTexture->w(), _ulrTexture->h());
	_ulrTexture->clear();

	if (_showBackgroundMesh) {
		_bgRenderer->process(*_scene->inputMesh(), eye, *_ulrTexture, _scene->inputTextures(), _scene->depthTextures(), true);
		_texturedRenderer.process(*_scene->inputMesh(), eye, _scene->inputMeshTexture()->handle(), *_ulrTexture, true);

		if (_dbScene != NULL) {
			
			Eigen::Affine3f viewMat;
			viewMat.matrix() = eye.view();
			
			sibr::GLuniform<sibr::Vector3f> deepBlendingGridMin = _dbScene->get_patch_cloud().grid_min();
			sibr::GLuniform<sibr::Vector3f> deepBlendingGridMax = _dbScene->get_patch_cloud().grid_max();
			sibr::GLuniform<float>          compositingMargin = 0.03f; // Expressed as a percentage of the AABB size for the deep blending scene.

			deepBlendingGridMin.init(_backgroundCompo, "gridMin");
			deepBlendingGridMax.init(_backgroundCompo, "gridMax");
			compositingMargin.init(_backgroundCompo, "compositingMargin");

			glViewport(0, 0, _ulrTexture->w(), _ulrTexture->h());
			_ulrTexture->bind();
			_backgroundCompo.begin();

			deepBlendingGridMin.send();
			deepBlendingGridMax.send();
			compositingMargin.send();

			// Textures.
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, _bgRenderer->depthHandle());

			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, inpTexture->handle());

			bool shouldRestoreDepth = glIsEnabled(GL_DEPTH_TEST);
			glDisable(GL_DEPTH_TEST);

			sibr::RenderUtility::renderScreenQuad();

			if (shouldRestoreDepth) {
				glEnable(GL_DEPTH_TEST);
			}

			_backgroundCompo.end();
			_ulrTexture->unbind();
		}
	}
	if (_onlyBackground) {
		sibr::blit(*_ulrTexture, dst);
		return;
	}

	_reflectionRT->clear();
	_reflectionRT->bind();
	glViewport(0, 0, _reflectionRT->w(), _reflectionRT->h());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation(GL_FUNC_ADD);
	// Render the reflections.
	renderAllReflections(_currentCam, false, _useMinCompo);
	glDisable(GL_BLEND);
	_reflectionRT->unbind();

	// Fill in with poisson.
	if (_usePoisson) {
		_poissonRenderer->process(uint(_reflectionRT->handle()), _finalRT);
	}
	else {
		sibr::blit(*_reflectionRT, *_finalRT);
	}

	// Final composite:
	// ULR background + mask * mix(min, reflections).
	sibr::blit(*_ulrTexture, dst, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
	dst.bind();
	glViewport(0, 0, dst.w(), dst.h());
	_finalCompo.begin();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _ulrTexture->handle());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _finalRT->handle());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _minBGTexture->handle());
	_alphaOut.send();
	_mvpFinal.set(eye.viewproj());
	_scene->windows()->render(true, true);
	_finalCompo.end();
	dst.unbind();
	
	handleSaving(dst);
	CHECK_GL_ERROR;
}

void SemanticReflectionsView::onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) 
{
	_currentCam = sibr::InputCamera(eye, dst.w(), dst.h());
	//_debugView->setUserCamera(_currentCam);

	const auto sortedCameras = _scene->closestCameras(_currentCam, _leaveOneOut ? _leaveOneOutID : -1);

	
	if (_backgroundViewCount > 0) {
		const int neighCount = std::min(_backgroundViewCount, int(sortedCameras.size()));
		std::vector<uint> pickedCams(neighCount);
		for (int i = 0; i < neighCount; ++i) {
			pickedCams[i] = sortedCameras[i];
		}
		_bgRenderer->updateCameras(pickedCams);
	}

	
	std::vector<uint> pickedCams;
	if(_reflectionsViewCount > 0) {
		const int neighCount = std::min(_reflectionsViewCount, int(sortedCameras.size()));
		for (int i = 0; i < neighCount; ++i) {
			pickedCams.push_back(sortedCameras[i]);
		}
	} else {
		pickedCams = sortedCameras;
	}
	updateCameras(pickedCams);

	_debugView->updateActiveCams(pickedCams);
	
	if(dst.w() != _minBGTexture->w() || dst.h() != _minBGTexture->h()) {
		_minBGTexture.reset(new sibr::RenderTargetRGB(dst.w(), dst.h(), SIBR_GPU_LINEAR_SAMPLING));
		_ulrTexture.reset(new sibr::RenderTargetRGB(dst.w(), dst.h(), SIBR_GPU_LINEAR_SAMPLING));
		_bgRenderer->resize(dst.w(), dst.h());
	}
	
	// Shortpath for baseline comparison.
	if(_compMode == ULR) {
		dst.clear();
		glViewport(0, 0, dst.w(), dst.h());
		_bgRenderer->backfaceCull() = true;
		_bgRenderer->process(*_scene->baseUlr(), eye, dst, _scene->inputTextures(), _scene->depthTextures(), true);
		_bgRenderer->backfaceCull() = false;
		_texturedRenderer.process(*_scene->baseUlr(), eye, _scene->baseMeshTexture()->handle(), dst, false);
		handleSaving(dst);
		return;
	} 
	if(_compMode == TEXTURED) {
		dst.clear();
		glViewport(0, 0, dst.w(), dst.h());
		_texturedRenderer.process(*_scene->baseUlr(), eye, _scene->baseMeshTexture()->handle(), dst, false);
		handleSaving(dst);
		return;
	}
	
	
	// Start by rendering the min BG interior using ULR.
	glBeginQuery(GL_TIME_ELAPSED, _queries[_fid][0]);

	glViewport(0, 0, _minBGTexture->w(), _minBGTexture->h());
	_minBGTexture->clear();
	_bgRenderer->process(*_scene->inputMesh(), eye, *_minBGTexture, _scene->minBGTextures(), _scene->depthTextures());
	// Debug show.
	if (_showMinRender) {
		sibr::blit(*_minBGTexture, dst);
		return;
	}

	glEndQuery(GL_TIME_ELAPSED);

	// Then, render regular background ULR and textured mesh.
	//_bgTexture->clear();
	glViewport(0, 0, _ulrTexture->w(), _ulrTexture->h());
	_ulrTexture->clear();

	glBeginQuery(GL_TIME_ELAPSED, _queries[_fid][1]);

	if(_showBackgroundMesh) {
		_bgRenderer->process(*_scene->inputMesh(), eye, *_ulrTexture, _scene->inputTextures(), _scene->depthTextures(), true);
		_texturedRenderer.process(*_scene->inputMesh(), eye, _scene->inputMeshTexture()->handle(), *_ulrTexture, true);

		// Render using the deep blending scene.
		if (_dbScene != NULL) {
			glViewport(0, 0, _deepBlendingTexture->w(), _deepBlendingTexture->h());
			_deepBlendingTexture->clear();

			Eigen::Affine3f viewMat;
			viewMat.matrix() = eye.view();
			_deepBlendingTexture->bind();
			_dbScene->get_patch_cloud().draw(eye.znear(), eye.zfar(), _dbScene->get_scene_args().ibr_sigma, _dbScene->get_scene_args().ibr_resolution_alpha, _dbScene->get_scene_args().ibr_depth_alpha, viewMat, eye.proj(), _dbScene->get_show_wireframe());
			_deepBlendingTexture->unbind();

			sibr::GLuniform<sibr::Vector3f> deepBlendingGridMin = _dbScene->get_patch_cloud().grid_min();
			sibr::GLuniform<sibr::Vector3f> deepBlendingGridMax = _dbScene->get_patch_cloud().grid_max();
			sibr::GLuniform<float>          compositingMargin   = 0.03f; // Expressed as a percentage of the AABB size for the deep blending scene.

			deepBlendingGridMin.init(_backgroundCompo, "gridMin");
			deepBlendingGridMax.init(_backgroundCompo, "gridMax");
			compositingMargin.init(_backgroundCompo,   "compositingMargin");

			glViewport(0, 0, _ulrTexture->w(), _ulrTexture->h());
			_ulrTexture->bind();
			_backgroundCompo.begin();

			deepBlendingGridMin.send();
			deepBlendingGridMax.send();
			compositingMargin.send();

			// Textures.
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, _bgRenderer->depthHandle());

			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, _deepBlendingTexture->handle());

			bool shouldRestoreDepth = glIsEnabled(GL_DEPTH_TEST);
			glDisable(GL_DEPTH_TEST);

			sibr::RenderUtility::renderScreenQuad();

			if (shouldRestoreDepth) {
				glEnable(GL_DEPTH_TEST);
			}
		
			_backgroundCompo.end();
			_ulrTexture->unbind();
		}
	}

	glEndQuery(GL_TIME_ELAPSED);

	if(_onlyBackground) {
		sibr::blit(*_ulrTexture, dst);
		return;
	}

	_reflectionRT->clear();
	_reflectionRT->bind();
	glViewport(0, 0, _reflectionRT->w(), _reflectionRT->h());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation(GL_FUNC_ADD);

	glBeginQuery(GL_TIME_ELAPSED, _queries[_fid][2]);

	// Render the reflections.
	renderAllReflections(_currentCam, false, _useMinCompo);

	glEndQuery(GL_TIME_ELAPSED);

	glDisable(GL_BLEND);
	_reflectionRT->unbind();

	// Fill in with poisson.
	if(_usePoisson) {

		glBeginQuery(GL_TIME_ELAPSED, _queries[_fid][3]);

		_poissonRenderer->process(uint(_reflectionRT->handle()), _finalRT);

		glEndQuery(GL_TIME_ELAPSED);

	} else {
		sibr::blit(*_reflectionRT, *_finalRT);
	}

	// Final composite:
	// ULR background + mask * mix(min, reflections).

	glBeginQuery(GL_TIME_ELAPSED, _queries[_fid][4]);

	sibr::blit(*_ulrTexture, dst, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
	dst.bind();
	glViewport(0, 0, dst.w(), dst.h());
	_finalCompo.begin();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _ulrTexture->handle());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _finalRT->handle());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _minBGTexture->handle());
	_alphaOut.send();
	_mvpFinal.set(eye.viewproj());
	_scene->windows()->render(true, true);
	_finalCompo.end();
	dst.unbind();

	glEndQuery(GL_TIME_ELAPSED);


	handleSaving(dst);
	
	_fid = (_fid+1)%2;
	CHECK_GL_ERROR;
}

void SemanticReflectionsView::handleSaving(const sibr::IRenderTarget & dst) {
	// if we are recording, get back the result and save.
	if (_saveReplay && _handler->getCameraRecorder().isPlaying()) {
		// Ugly but deadline.
		sibr::RenderTargetRGB rgb(dst.w(), dst.h(), SIBR_GPU_LINEAR_SAMPLING);
		sibr::blit(dst, rgb);
		sibr::ImageRGB rgbImg;
		rgb.readBack(rgbImg);
		rgbImg.save(_scene->basePath() + "/outputteaser_" + (_compMode == OURS ? "ours" : (_compMode == ULR ? "ulr" : "textured")) + "/" + sibr::intToString<10>(_imgId) + ".png");
		++_imgId;
	}
}

void SemanticReflectionsView::registerDebugView(const std::shared_ptr<sibr::SceneDebugView>& sceneDebugView) {
	_debugView = sceneDebugView;
}

void SemanticReflectionsView::registerHandler(const std::shared_ptr<sibr::InteractiveCameraHandler>& handler) {
	_handler = handler;
}



void SemanticReflectionsView::dumpFlows(const int neighsCount) {


	// Background flow renderer, currently unused.
	sibr::GLShader bgFlowShader;
	bgFlowShader.init("BGFlowShader",
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flowrenderer.vert"),
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flowrenderer.frag"));

	sibr::GLShader passthrough;
	passthrough.init("Pass",
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_depth.vert"),
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_depth.frag"));

	sibr::GLuniform<sibr::Matrix4f> currentMVP, referenceMVP, passthroughMVP;
	sibr::GLuniform<sibr::Vector3f> referenceDir, referencePos;

	currentMVP.init(bgFlowShader, "currentMVP");
	referenceMVP.init(bgFlowShader, "referenceMVP");
	referenceDir.init(bgFlowShader, "referenceDir");
	referencePos.init(bgFlowShader, "referencePos");
	passthroughMVP.init(passthrough, "MVP");

	const std::string outputPathRoot = _scene->basePath() + "/semantic_reflections/layers_bidir/";
	sibr::makeDirectory(outputPathRoot);


	struct BatchInfos {
		unsigned int view;
		int window;

		BatchInfos(unsigned int viewId, int windowId) {
			view = viewId;
			window = windowId;
		}

	};
	// Load listing
	std::vector<BatchInfos> batches;
	for (uint i = 0; i < uint(_scene->cameras().size()); ++i) {
		batches.emplace_back(i, -1);
	}

	// For each camera.
#pragma omp parallel for
	for (int rid = 0; rid < batches.size(); ++rid) {
		const auto& batch = batches[rid];
		const int cid = batch.view;
		const auto& refCam = *_scene->cameras()[cid];

		const std::string outputPathRef = outputPathRoot + refCam.name() + "/";
		sibr::makeDirectory(outputPathRef);

		// Save the input image, the input mask.
		_scene->inputImages()[cid].save(outputPathRef + "/image.jpg", false); // For progress tracking.
		_scene->partsImages()[cid].save(outputPathRef + "/mask.png", false);

		// For some neighbours.
		const auto sortedCameras = _scene->closestCameras(refCam, cid);

		// For each of the neighboring images, we want the flow from the reference view to the neighboring image.
#pragma omp parallel for
		for (int ncid = 0; ncid < neighsCount; ++ncid) {
			const uint ocid = sortedCameras[ncid];
			const auto& selectedCam = _scene->cameras()[ocid];
			const std::string outputPathRefNeigh = outputPathRef + selectedCam->name() + "/";
			sibr::makeDirectory(outputPathRefNeigh);
			// Save the input image.
			// Since LayerSep is now in SIBR and does all of the processing at once, we can skip saving this one.
			// We still need the directory for the flow and to keep track of the neighbor names.
			//_scene->inputImages()[ocid].save(outputPathRefNeigh + "/image.jpg", false);

		}
	}


	sibr::GLShader masksShader;
	masksShader.init("Mask shader", sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flatcolor.vert"), sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/flatcolor.frag"));
	sibr::GLuniform<int> masksColor;
	sibr::GLuniform<sibr::Matrix4f> masksMVP;
	sibr::GLuniform<int> maskwinId;
	masksColor.init(masksShader, "color");
	masksMVP.init(masksShader, "MVP");
	maskwinId.init(masksShader, "windowId");



	/* Create the foreground flow renderer. */
	sibr::GLShader flowFgShader;
	sibr::GLShader::Define::List defines;
	defines.emplace_back("NUM_CAMS", _scene->cameras().size());
	flowFgShader.init("Flow shader 1", sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_base.vert"),
		sibr::loadFile(sibr::getShadersDirectory("semantic_reflections") + "/reflections_flowonly.frag", defines));

	sibr::GLuniform<sibr::Matrix4f> paramMVP;
	sibr::GLuniform<sibr::Vector3f> sphereCenter;
	sibr::GLuniform<float> sphereRadius;
	sibr::GLuniform<sibr::Vector3f> currentPos;
	sibr::GLuniform<sibr::Vector2f> flowIntensity;
	sibr::GLuniform<int> camsCount;
	sibr::GLuniform<int> windowId;
	sibr::GLuniform<sibr::Vector3f> sceneUpVector;

	paramMVP.init(flowFgShader, "MVP");
	sphereRadius.init(flowFgShader, "sphereRadius");
	sphereCenter.init(flowFgShader, "sphereCenter");
	currentPos.init(flowFgShader, "currentPos");
	camsCount.init(flowFgShader, "camsCount");
	flowIntensity.init(flowFgShader, "flowIntensity");
	windowId.init(flowFgShader, "windowId");
	sceneUpVector.init(flowFgShader, "sceneUpVector");

	sceneUpVector = _scene->upVector();

	std::vector<CameraUBOInfos> cameraInfos;
	GLuint uboIndex = 0;
	{
		const int camsCountMax = int(_scene->cameras().size());
		// Populate the cameraInfos array (will be uploaded to the GPU).
		cameraInfos.clear();
		cameraInfos.resize(camsCountMax);
		for (size_t i = 0; i < camsCountMax; ++i) {
			const auto& cam = *_scene->cameras()[i];
			cameraInfos[i].vp = cam.viewproj();
			cameraInfos[i].pos = cam.position();
			cameraInfos[i].dir = cam.dir();
			cameraInfos[i].selected = cam.isActive();
		}
		// Create UBO.
		glGenBuffers(1, &uboIndex);
		glBindBuffer(GL_UNIFORM_BUFFER, uboIndex);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraUBOInfos) * camsCountMax, &cameraInfos[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	for (int rid = 0; rid < batches.size(); ++rid) {
		const auto& batch = batches[rid];
		const int cid = batch.view;
		const auto& refCam = *_scene->cameras()[cid];

		const std::string outputPathRef = outputPathRoot + refCam.name() + "/";
		std::cout << "|" << std::flush;

		// Compute the window mask.
		sibr::RenderTargetLum maskRT(refCam.w(), refCam.h());
		maskRT.clear();
		maskRT.bind();
		glViewport(0, 0, maskRT.w(), maskRT.h());
		masksShader.begin();
		masksMVP.set(refCam.viewproj());
		masksColor.set(255);

		//glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		//_scene->inputMesh()->render(true, false);
		//glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		for (const auto& win : _scene->parameters()) {
			if (batch.window == -1 || batch.window == win.meshId) {
				maskwinId = win.meshId;
				maskwinId.send();
				_scene->windows()->render(true, true);
			}
		}
		maskRT.unbind();

		sibr::ImageL8 maskImg;
		maskRT.readBack(maskImg);
		maskImg.save(outputPathRef + "/mask.png", false);


		// For some neighbours.
		const auto sortedCameras = _scene->closestCameras(refCam, cid);



		sibr::RenderTargetLum32F depthRef(refCam.w(), refCam.h());
		depthRef.clear();
		depthRef.bind();
		glViewport(0, 0, depthRef.w(), depthRef.h());
		passthrough.begin();
		passthroughMVP.set(refCam.viewproj());
		_scene->inputMesh()->render(true, true);
		passthrough.end();
		depthRef.unbind();

		// For each of the neighboring images, we want the flow from the reference view to the neighboring image.
		for (int ncid = 0; ncid < neighsCount; ++ncid) {
			std::cout << "." << std::flush;
			const uint ocid = sortedCameras[ncid];
			const auto& selectedCam = *_scene->cameras()[ocid];
			const std::string outputPathRefNeigh = outputPathRef + selectedCam.name() + "/";

			sibr::RenderTargetLum32F depthSel(selectedCam.w(), selectedCam.h());
			depthSel.clear();
			depthSel.bind();
			glViewport(0, 0, depthSel.w(), depthSel.h());
			passthrough.begin();
			passthroughMVP.set(selectedCam.viewproj());
			_scene->inputMesh()->render(true, true);
			passthrough.end();
			depthSel.unbind();

			{
				sibr::RenderTargetRGB32F fetchMapR(refCam.w(), refCam.h());
				//sibr::RenderTargetRGB32F fetchMapBgR(refCam.w(), refCam.h());

				{
					// Compute the foreground flow.
					for (auto& caminfos : cameraInfos) {
						caminfos.selected = 0;
					}
					cameraInfos[ocid].selected = 1;
					// Update the content of the UBO.
					glBindBuffer(GL_UNIFORM_BUFFER, uboIndex);
					glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraUBOInfos) * (_scene->cameras().size()), &cameraInfos[0]);
					glBindBuffer(GL_UNIFORM_BUFFER, 0);

					fetchMapR.clear();
					fetchMapR.bind();
					glViewport(0, 0, fetchMapR.w(), fetchMapR.h());
					flowFgShader.begin();

					paramMVP.set(refCam.viewproj());
					sphereCenter.set(_sphereCenter);
					sphereRadius.set(_sphereRadius);
					currentPos.set(refCam.position());
					camsCount.set(int(_scene->cameras().size()));
					sceneUpVector.send();

					// Bind UBO to shader.
					glBindBuffer(GL_UNIFORM_BUFFER, uboIndex);
					glBindBufferBase(GL_UNIFORM_BUFFER, 0, uboIndex);
					glBindBuffer(GL_UNIFORM_BUFFER, 0);


					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->partsTextures()->handle());
					glActiveTexture(GL_TEXTURE1);
					glBindTexture(GL_TEXTURE_2D_ARRAY, _scene->idTextures()->handle());



					// Iterate and render each window separately.
					for (const auto& win : _scene->parameters()) {
						if (batch.window == -1 || batch.window == win.meshId) {
							flowIntensity = win.radii;
							windowId = win.meshId;
							flowIntensity.send();
							windowId.send();
							_scene->windows()->render(true, true);
						}
					}

					flowFgShader.end();
					fetchMapR.unbind();



					// Compute the background flow.
					/*
					fetchMapBgR.clear();
					fetchMapBgR.bind();
					glViewport(0, 0, fetchMapBgR.w(), fetchMapBgR.h());
					bgFlowShader.begin();

					currentMVP.set(refCam.viewproj());
					referenceMVP.set(selectedCam.viewproj());

					referenceDir.set(selectedCam.dir());
					referencePos.set(selectedCam.position());

					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, depthSel.handle());

					_scene->inputMesh()->render(true, false);

					bgFlowShader.end();
					fetchMapBgR.unbind();
					*/
				}


				// Read back both UV maps.
				{
					sibr::ImageRGB32F fetchImgR;
					fetchMapR.readBack(fetchImgR);
					const sibr::ImageRGBA fetchSaveR = sibr::convertRGB32FtoRGBA(fetchImgR);
					fetchSaveR.save(outputPathRefNeigh + "/foreground_uvs_in_neigh.png", false);
				}

			}
		}

	}
}