/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_SEMANTIC_HELPERS_HPP__
#define __SIBR_SEMANTIC_HELPERS_HPP__

# include "Config.hpp"
#include <core/system/Utils.hpp>
#include <core/assets/ImageListFile.hpp>
#include <core/assets/InputCamera.hpp>
#include <core/graphics/Mesh.hpp>
#include <core/graphics/Utils.hpp>
#include <map>
#include <vector>
#include <string>

/**
 * Basic utilities for retro-compatibility.
 */

namespace sibr {

	namespace semantic {

		/** Transform a mesh to the same frame as fribr.
			Do not use for new projects, only for retro-compatibility.
		*/

		SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT void transformMesh(const sibr::Mesh::Ptr& mesh, int i);

		/** Load a colmap bundle in the same frame as fribr (ie flipped compared to ours).
			Do not use for new projects, only for retro-compatibility.
		*/
		SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT std::vector<InputCamera::Ptr> loadColmapFlipped(const std::string & colmapSparsePath, const float zNear = 0.001f, const float zFar = 1000.0f);


	}
}
#endif // __SIBR_SEMANTIC_HELPERS_HPP__
