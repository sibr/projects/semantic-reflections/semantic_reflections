/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_SEMANTIC_REFLECTIONS_VIEW1_HPP__
#define __SIBR_SEMANTIC_REFLECTIONS_VIEW1_HPP__

#include "Config.hpp"

#include "projects/ulr/renderer/ULRV3Renderer.hpp"
#include "projects/inside_out_deep_blending/renderer/DeepBlendingScene.hpp"

#include "core/renderer/PoissonRenderer.hpp"
#include "core/renderer/TexturedMeshRenderer.hpp"
#include "core/view/SceneDebugView.hpp"
#include "core/view/ViewBase.hpp"

#include "SemanticReflectionsScene.hpp"

#include <memory>


/** The Seamntic Reflections View combines windows rendering as described in
 *Image-based Rendering of Cars using Semantic Labels and Approximate Reflection Flow
 *with an existing IBR technique for the background (either ULR or DeepBlending).
 **/
class SIBR_SEMANTIC_REFLECTIONS_LIB_EXPORT SemanticReflectionsView : public sibr::ViewBase {


	SIBR_DISALLOW_COPY(SemanticReflectionsView);
public:

	SIBR_CLASS_PTR(SemanticReflectionsView)

	/** Constructor that will rely on ULR for the background. */
	SemanticReflectionsView(const SemanticReflectionsScene::Ptr scene, const unsigned int w, const unsigned int h);

	/** Constructor that will rely on DeepBlending for the background. */
	SemanticReflectionsView(const SemanticReflectionsScene::Ptr scene, const sibr::DeepBlendingScene::Ptr dbScene, const unsigned int w, const unsigned int h);

	void onGUI() override;
	
	void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

	void onUpdate(sibr::Input& input, const sibr::Viewport& vp) override;

	/** Render reflections for a specific window at a specific input view, using a subset of other views, with custom ellipsoid radii.
	 * Used in preprocess for the dense parameter search.
	 *\param cid input view to render from (index in the scene camera list)
	 *\param camerasEnabled indices of the input views to use to generate the reflections
	 *\param windowId the ID of the window to render reflections for
	 *\param paramX X ellipsoid radius
	 *\param paramY Y ellipsoid radius
	 *\param dst destination render target
	 */
	void renderWarpedReflection(const unsigned int cid, const std::vector<unsigned int> & camerasEnabled, const int windowId, const float paramX, const float paramY, sibr::IRenderTarget & dst);

	/** Use the same window rendering algorithm but only output the optical flows and not the rendered images. Used during preprocess
	 *\param neighs number of camera neighbors to compute the flow from
	 *\note Will output to scene_root/layers_bidir/
	 */
	void dumpFlows(const int neighs);

	void registerDebugView(const std::shared_ptr<sibr::SceneDebugView>& sceneDebugView);
	
	void registerHandler(const std::shared_ptr<sibr::InteractiveCameraHandler>& handler);

private:

	enum AlphaMode : int {
		REGULAR = 0, ADD, PREMUL
	};

	enum ComparisonMode : int {
		OURS = 0, ULR, TEXTURED
	};

	/// Shared init.
	void initSemanticReflectionsView(const unsigned int w, const unsigned int h);

	/// Shared shaders init.
	void initShaders(bool reset);

	/// Update the selected cameras for the reflections.
	void updateCameras(const std::vector<uint> & camIds);

	/** Export a path to be composited with an offline deep blending background for high quality offline results. */
	void exportPathDBBG(const std::vector<sibr::InputCamera::Ptr> cams, const std::string path);

	/** Render a frame  to be composited with an offline deep blending background for high quality offline results. */
	void onRenderIBRDB(sibr::IRenderTarget& dst, const sibr::Camera& eye, const sibr::Texture2DRGBA::Ptr inpTexture) ;

	/** Render reflections on all visible windows.
	 * \param eye the current viewpoint
	 * \param outputFlow render optical flow instead of the color images
	 * \param useMinForeground Should the reflection layers be used, or just the full images (false will be used during dense fitting in preprocess)
	 */
	void renderAllReflections(const sibr::InputCamera & eye, const bool outputFlow, const bool useMinForeground);

	/** Save frame if needed. Created before the recorder was added to SIBR. */
	void handleSaving(const sibr::IRenderTarget & dst);

	SemanticReflectionsScene::Ptr _scene;
	sibr::DeepBlendingScene::Ptr  _dbScene;

	sibr::TexturedMeshRenderer _texturedRenderer;
	sibr::ULRV3Renderer::Ptr _bgRenderer;
	sibr::PoissonRenderer::Ptr _poissonRenderer;

	sibr::RenderTargetRGB::Ptr _minBGTexture;
	sibr::RenderTargetRGB::Ptr _ulrTexture;
	sibr::RenderTargetRGB::Ptr _deepBlendingTexture;
	sibr::RenderTargetRGB::Ptr _reflectionRT;
	sibr::RenderTargetRGBA::Ptr _finalRT;

	sibr::GLShader _flowShader;
	sibr::GLuniform<sibr::Matrix4f> _paramMVP;
	sibr::GLuniform<sibr::Vector3f> _sphereCenter;
	sibr::GLuniform<float> _sphereRadius;
	sibr::GLuniform<sibr::Vector3f> _currentPos;
	sibr::GLuniform<sibr::Vector2f> _flowIntensity;
	sibr::GLuniform<float> _mixFullAndDiff;
	sibr::GLuniform<float> _panComparison;
	sibr::GLuniform<bool> _useMinCompo;
	sibr::GLuniform<bool> _applyBackgroundMin;
	sibr::GLuniform<int> _camsCount;
	sibr::GLuniform<int> _windowId;
	sibr::GLuniform<bool> _outputFlow;
	sibr::GLuniform<bool> _stricterMasks;
	sibr::GLShader _finalCompo;
	sibr::GLShader _backgroundCompo;
	sibr::GLuniform<float> _alphaOut;
	sibr::GLuniform<sibr::Matrix4f> _mvpFinal;

	/// Camera infos data structure shared between the CPU and GPU.
	/// We have to be careful about alignment if we want to send those struct directly into the UBO.
	struct CameraUBOInfos {
		sibr::Matrix4f vp;
		sibr::Vector3f pos;
		int selected = 0;
		sibr::Vector3f dir;
		float dummy = 0.0f; // padding to a multiple of 16 bytes.
	};

	std::vector<CameraUBOInfos> _cameraInfos;
	GLuint _uboIndex;

	bool _showBackgroundMesh = true;
	int _showLayer = 0;
	bool _showMinRender = false;
	bool _performBlending = true;
	bool _onlyBackground = false;
	bool _useInteriorMesh = false;
	bool _fillWithInteriorMesh = false;
	bool _useAltWindows = false;
	bool _refMinUseNovelCam = false;
	ComparisonMode _compMode = OURS;
	AlphaMode _alphaMode = REGULAR;
	bool _usePoisson = true;
	bool _customRadii = false;

	int _backgroundViewCount = 0;
	int _minNovelViewCount = 32;
	int _minRefsViewCount = 32;
	int _reflectionsViewCount = 4;
	int _numNeighsMinFgs = 15;

	sibr::InputCamera _currentCam;
	std::shared_ptr<sibr::SceneDebugView> _debugView;
	std::shared_ptr<sibr::InteractiveCameraHandler> _handler;

	bool _saveReplay = false;

	int _imgId = 0;

	int _leaveOneOutID = -1;
	bool _leaveOneOut = false;

	std::vector<std::vector<GLuint>> _queries;
	int _fid = 0;
};


#endif // __SIBR_SEMANTIC_REFLECTIONS_VIEW_HPP__
