/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/**

@page semantic_reflectionsDeepPage Deep Blending training

\tableofcontents

\section semantic_reflections_prep Preparation

You will need :
- Tensorflow >= 1.6.1 with GPU support. (I suggest you follow the tensorflow virtual environment installation procedure described here: https://www.tensorflow.org/install/pip).  
- the `deep_blending` and `fribr_framework` repositories cloned. 

The scripts referenced below are in the `deep_blending` repository. Please note that this walkthrough specifically details how to run training and evaluation jobs on a OAR cluster. You can instead run the scripts on any machine you want.
You might also want to refer to the `deep_blending` repository documentation, as it might contain combined scripts and additional up-to-date explanations.

\section semantic_reflections_dbtrain Training

For each scene to use in the training set, run (in `sibr`, slow):
\code
	./datadump_cmd.exe path/to/dataset/ dump_flow just_colors plug_holes path_samples=3 random_samples=4
\endcode
	./datadump_cmd.exe path/to/dataset/datadump/ path_samples=3 random_samples=4 fuse_flow

You will get a `datadump/` directory, which will contain, for each input view, a series of layer renderings (you can check their validity).
Remove any `.txt` file present in this directory.

Upload the datadump directories to the cluster using scp. The following hierarchy will be used on the cluster:
\code
	train_sets/dataset_1/*.jpg,*.png
	train_sets/dataset_2/*.jpg,*.png
	...
\endcode
Move to `train_sets/` and, for each dataset run (`make_data.py` is in the DeepBlend repository):
\code
	python make_data_txt.py dataset_1 > dataset_1_train.txt
	python make_data_txt.py dataset_1 validation > dataset_1_val.txt
\endcode
Concat and shuffle the resulting txts:
\code
	cat *_train.txt | shuf > training.txt
	cat *_val.txt | shuf > validation.txt
\endcode
Update the job script `train.sh` with the proper path:
\code
	python blend.py /path/to/train_sets/ ./output_training/ --training-file /path/to/training.txt --validation-file /path/to/validation.txt
\endcode
Note: make sure `fast_network` option is set to false.

Run the training as a cluster job:
\code
	oarsub -p "gpu='YES' and gpucapability >= '5.2'" -l /nodes=1,walltime=160:00:00 -S "./train.sh"
\endcode
You can check the status of the job with:
\code
	oarpeek JOB_ID -t 
	oarpeek JOB_ID -t -e
\endcode
Once the training is done (after ~512k steps), the network is output in `output_training/model/`. If needed, you can then run `latest_blend_conv.py` to export the model.pb file for the real-time viewer (see the `deep_blending` documentation for the proper script version, and for adding export of the 1080p network).


\section semantic_reflections_offlineeval Offline evaluation

For offline evaluation:

- put your camera path (bundler format) at `/path/to/dataset/testpath/path.rd.out`
- run 
\code
		./datadump_cmd.exe /path/to/dataset/ test just_colors plug_holes clear_white
\endcode
- upload `testdump/` to the cluster (remove any `.txt`), with hierarchy `test_sets/dataset_1/testdump/`
- go to `test_sets/dataset_1/`
- run
\code
		python make_data_txt.py testdump test > test.txt
\endcode
- make sure `blend.py` is setup in the same way as for the training you want to use
- edit `eval.sh`: 
\code
		OUTPUT_DIR="./output_training/"
		DATASET_NAMES=(dataset_1)
		python ../blend.py /path/to/test_sets/${DATASET_NAMES}/ ./ --test
\endcode
- submit the job:
\code
		oarsub -p "gpu='YES' and gpucapability >= '5.2'" -l /nodes=1/gpunum=2,walltime=1:00:00 -S "./eval.sh"
\endcode
The frames will be output in `output_training/img_test/`

*/