/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>

#include <core/graphics/Window.hpp>
#include <core/view/ViewBase.hpp>
#include <core/view/MultiViewManager.hpp>

#include <core/renderer/DepthRenderer.hpp>
#include "projects/semantic_reflections/renderer/SemanticReflectionsScene.hpp"
#include "projects/semantic_reflections/renderer/SemanticReflectionsView.hpp"


#include <omp.h>


#define PROGRAM_NAME "sibr_semantic_reflections_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;



struct SemanticReflectionsDemoArgs : BasicDatasetArgs, WindowAppArgs {
	
	Arg<int> max_input_width = { "max_width", -1, "maximum width for input data in memory" };
	Arg<int> render_width = { "width", 1920, "rendering width (legacy)" };
	Arg<int> render_height = { "height", 1080, "rendering width (legacy)" };
	Arg<bool> half_depth = { "halfdepth", "geenrate input depth maps at half resolution" };
	Arg<bool> no_DB = { "nodb", "skip Deep Blending loading and rendering" };
};




int main(int ac, char** av) {
	
	CommandLineArgs::parseMainArgs(ac, av);
	SemanticReflectionsDemoArgs args;
	
	
	const std::string basePath = args.dataset_path;
	const int maxWidth = args.max_input_width;
	const int renderW = args.render_width;
	const int renderH = args.render_height;

	sibr::Window window(PROGRAM_NAME, sibr::Vector2i(50, 50), args);
	
	const bool halfResDepth = args.half_depth;
	// Setup the scene.
	DeepBlendingScene::Ptr deepBlendingScene = nullptr;
	if(!args.no_DB) {
		DeepBlendingAppArgs dbArgs;
		dbArgs.mesh_path = basePath + "/capreal/mesh_final_retextured_cleaned.obj";
		dbArgs.nchw = true;
		// Dummy values.
		dbArgs.texture_width = 720;
		dbArgs.rendering_size = { 720, 576 };
		deepBlendingScene = DeepBlendingScene::Ptr(new DeepBlendingScene(dbArgs, window));
	}

	auto scene = std::make_shared< SemanticReflectionsScene>(basePath, maxWidth, SemanticReflectionsScene::RENDERING, halfResDepth);

	SemanticReflectionsView::Ptr mainView( new SemanticReflectionsView(scene, deepBlendingScene, uint(scene->resolution()[0]), uint(scene->resolution()[1])));
	
	// The interactive camera can use a raycaster for additional interactions.
	std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
	raycaster->init();
	raycaster->addMesh(*scene->inputMesh());

	// Setup the  interactive camera.
	sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
	sibr::InputCamera startCamera = *scene->cameras()[0];
	const Viewport sceneViewport(0.0f, 0.0f,float(renderW), float(renderH));
	generalCamera->setup(scene->cameras(), sceneViewport, raycaster);
	generalCamera->setupInterpolationPath(scene->cameras());
	
	MultiViewManager		viewManager(window, false);
	// Add the view and camera to the manager.
	viewManager.addIBRSubView("Render view", mainView, sibr::Vector2u(renderW, renderH), ImGuiWindowFlags_ResizeFromAnySide);
	viewManager.addCameraForView("Render view", generalCamera);
	mainView->registerHandler(generalCamera);

	// Create and add a debug view.
	const std::shared_ptr<sibr::SceneDebugView>	topView(new SceneDebugView(scene->getIBRScene(), generalCamera, args));
	viewManager.addSubView("Top view", topView, {1280, 720});
	mainView->registerDebugView(topView);
	viewManager.getIBRSubView("Top view")->active(false);

	CHECK_GL_ERROR;

	// Main looooooop.
	while (window.isOpened()) {

		sibr::Input::poll();
		window.makeContextCurrent();
		if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
			window.close();
		}

		viewManager.onUpdate(sibr::Input::global());
		viewManager.onRender(window);

		window.swapBuffer();
		CHECK_GL_ERROR;
	}

	return EXIT_SUCCESS;
}