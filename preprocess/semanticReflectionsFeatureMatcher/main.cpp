/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#define NOMINMAX

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/ray_tracing.h"
#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/vision.h"
#include "projects/fribr_framework/renderer/io.h"
#include "projects/fribr_framework/renderer/tools/file_tools.h"
#include "projects/fribr_framework/renderer/tools/image_tools.h"
#include "projects/fribr_framework/renderer/tools/string_tools.h"
#include "projects/fribr_framework/renderer/tools/geometry_tools.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <opencv2/core.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/features2d.hpp>

#include <igl/components.h>

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
#include <unordered_map>

#include <Windows.h>
#include <WinBase.h>
#include <core\system\CommandLineArgs.hpp>

#include "dirent.h"
#include <sys/types.h>

#define DUMP_VISUALIZATIONS 0

using namespace Eigen;
using namespace fribr;

namespace
{

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error %d: %s\n", error, description);
}

inline Eigen::Vector3f unproject(float x, float y, float depth,
	const Eigen::Matrix3f &inv_intrinsics)
{
	const Eigen::Vector3f img_pos = Eigen::Vector3f(x, y, 1.0f);
	return depth * (inv_intrinsics * img_pos);
}

inline Eigen::Vector3f unproject(float x, float y, float depth,
	const Eigen::Matrix3f &inv_intrinsics,
	const Eigen::Matrix4f &cam_to_world)
{
	const Eigen::Vector3f cam_pos = unproject(x, y, depth, inv_intrinsics);
	const Eigen::Vector4f world_pos = cam_to_world * cam_pos.homogeneous();
	return Eigen::Vector3f(world_pos.x(), world_pos.y(), world_pos.z());
}

inline Eigen::Vector3f pixel_to_direction(const Eigen::Vector2f pixel_position, const Eigen::Vector2i resolution,
	                                      const Eigen::Matrix3f reference_frame, const float max_angle)
{
	const float xf = pixel_position.x() / resolution.x() - 0.5f;
	const float yf = pixel_position.y() / resolution.y() - 0.5f;

	const float           angle     = Eigen::Vector2f(xf, yf).norm() / 0.5f * max_angle;
	const Eigen::Vector3f direction = (reference_frame * Eigen::Vector3f(xf, 0.0f, yf)).normalized();

	return std::cos(angle) * reference_frame.col(1) + std::sin(angle) * direction;
}

bool project_if_visible(const Eigen::Vector3f &position, 
	                    const Eigen::Matrix4f &world_to_clip, 
	                    const CalibratedImage &camera,
	                    Eigen::Vector3f       &image_position_out)
{
	// Reject if outside view frustum
	Eigen::Vector4f p_clip = world_to_clip * position.homogeneous();
	if (p_clip.x() < -p_clip.w() || p_clip.x() > p_clip.w() ||
		p_clip.y() < -p_clip.w() || p_clip.y() > p_clip.w() ||
		p_clip.z() < -p_clip.w() || p_clip.z() > p_clip.w())
		return false;

	Eigen::Vector4f p_cam = camera.get_world_to_camera().matrix() * position.homogeneous();
	Eigen::Vector3f p_img = camera.get_intrinsics() * p_cam.head<3>();
	p_img /= p_img.z();

	// Reject if depth test fails
	if (p_img.x() < 0 || p_img.x() >= camera.get_scaled_resolution().x() ||
		p_img.y() < 0 || p_img.y() >= camera.get_scaled_resolution().y())
		return false;

	image_position_out = p_img;
	return true;
}

bool project_if_visible(const Eigen::Vector3f &position,
                        const Eigen::Matrix4f &world_to_clip,
                        const CalibratedImage &camera,
                        Eigen::Vector2i       &image_position_out)
{
	Eigen::Vector3f image_position_f;
	if (!project_if_visible(position, world_to_clip, camera, image_position_f))
		return false;

	image_position_out = image_position_f.head<2>().cast<int>();
	return true;
}

float intersect_sphere(Eigen::Vector3f sc, float sr, Eigen::Vector3f p, Eigen::Vector3f d)
{
	// intersects with sphere.
	Eigen::Vector3f OJ = p - sc;
	float dotDJ = d.dot(OJ);
	float delta2 = dotDJ*dotDJ - (OJ.dot(OJ) - sr*sr);
	if (delta2 < 0.0f)
		return -1.0f;
	float delta = sqrt(delta2);

	// Return the smallest positive root.
	float l1 = -dotDJ - delta;
	float l2 = -dotDJ + delta;
	if (l1 < 0.0f && l2 < 0.0f)
		return -1.0f;

	float lambda = l1;
	if (l1 < 0.0f)
		lambda = l2;

	return lambda;
}

// Derived by IQ: see https://www.shadertoy.com/view/MlsSzn
float intersect_aa_ellipsoid(Eigen::Vector3f ec, Eigen::Vector3f er, Eigen::Vector3f p, Eigen::Vector3f d)
{
    Eigen::Vector3f oc  = p - ec;
    Eigen::Vector3f ocn = (oc.array() / er.array()).matrix();
    Eigen::Vector3f rdn = (d.array() / er.array()).matrix();
    float a = rdn.dot(rdn);
	float b = ocn.dot(rdn);
	float c = ocn.dot(ocn);
	float h = b*b - a*(c-1.0f);
	if (h < 0.0f)
		return -1.0f;
	return (-b - std::sqrt(h)) /a;
}

Eigen::Vector3f find_reflection_position_ellipsoid(Eigen::Vector3f local_radii,
	                                               Eigen::Vector3f up_vector,
	                                               Eigen::Vector3f vertex_center,
	                                               Eigen::Vector3f plane_normal,
	                                               Eigen::Vector3f ray_dir,
	                                               Eigen::Vector3f ref_cam_pos,
	                                               Eigen::Vector3f current_pos,   // The location of the current (target) camera.
	                                               Eigen::Vector3f sphere_pos,    // For the background scene sphere.
	                                               float           sphere_radius, // ditto.
	                                               /* out */ float& loss)
{
	// We want to align planeNormal to (1,0,0), upVector to (0,1,0)
	Eigen::Vector3f a = plane_normal.normalized();
	Eigen::Vector3f b = up_vector.normalized();
	Eigen::Vector3f c = a.cross(b).normalized();

	Eigen::Matrix3f local_to_world;
	local_to_world.col(0) = a;
	local_to_world.col(1) = b;
	local_to_world.col(2) = c;
	Eigen::Matrix3f world_to_local = local_to_world.transpose();

	// Rotate all points.
	Eigen::Vector3f vertex_center_local = world_to_local * vertex_center;
	Eigen::Vector3f ref_cam_pos_local   = world_to_local * ref_cam_pos;
	Eigen::Vector3f ray_dir_local       = (world_to_local * ray_dir).normalized();
	Eigen::Vector3f current_pos_local   = world_to_local * current_pos;
	Eigen::Vector3f sphere_center_local = world_to_local * sphere_pos;

	// Assume vertex_center is at the surface of the ellipsoid, along the planeNormal direction, ie (1,0,0).
	Eigen::Vector3f ellipsoid_center = vertex_center_local - local_radii.x() * 0.95 * Eigen::Vector3f(1.0,0.0,0.0);
	float lambdaE = intersect_aa_ellipsoid(ellipsoid_center, local_radii, current_pos_local, ray_dir_local);
	Eigen::Vector3f intersection_local = current_pos_local + lambdaE * ray_dir_local;
	Eigen::Vector3f window_normal_local = ((intersection_local - ellipsoid_center).array() / local_radii.array()).matrix().normalized();

	Eigen::Vector3f ref_ray_dir = (ray_dir_local - 2.0f * (ray_dir_local.dot(window_normal_local) * window_normal_local)).normalized();
	float lambda = intersect_sphere(sphere_center_local, sphere_radius, intersection_local, ref_ray_dir);
	Eigen::Vector3f bg_intersection = intersection_local + lambda * ref_ray_dir;

	// Move to frame centered on ellipsoid center.
	Eigen::Vector3f ellipsoid_normal   = window_normal_local;
	Eigen::Vector3f ref_cam_sphere_pos = ref_cam_pos_local - ellipsoid_center;
	Eigen::Vector3f point_sphere_pos   = bg_intersection - ellipsoid_center;
	Eigen::Vector3f ellipsoid_pos      = intersection_local - ellipsoid_center;
	loss = lambdaE;

	for (int gid = 0; gid < 30; ++gid)
	{
		Eigen::Vector3f prn = point_sphere_pos - ellipsoid_pos;
		Eigen::Vector3f irn = ref_cam_sphere_pos - ellipsoid_pos;
		Eigen::Vector3f v   = prn.normalized() + irn.normalized();
		Eigen::Vector3f g   = v.normalized() - ellipsoid_normal;

		ellipsoid_normal += 0.2f * g;
		ellipsoid_normal.normalize();

		ellipsoid_pos = (local_radii.array() * ellipsoid_normal.array()).matrix();
	}

	// Final loss.
	Eigen::Vector3f prn = point_sphere_pos - ellipsoid_pos;
	Eigen::Vector3f irn = ref_cam_sphere_pos - ellipsoid_pos;
	Eigen::Vector3f v   = (prn.normalized() + irn.normalized()).normalized();
	loss = v.dot(ellipsoid_normal);

	return local_to_world * (ellipsoid_center + ellipsoid_pos);
}


void load_calibrated_images(const std::string              &path,
	                        fribr::CalibratedImage::Vector &cameras_dslr,
	                        fribr::PointCloud::Ptr         &point_cloud,
	                        fribr::PointCloudRenderer::Ptr &point_renderer,
	                        fribr::Observations            &observations)
{
	cameras_dslr.clear();
	observations.points.clear();
	observations.observations.clear();

	std::string extension = fribr::get_extension(path);
	fribr::CalibratedImage::Vector cameras_all;
	fribr::Observations            observations_all;
	if (extension == "json") {
		cameras_all = fribr::OpenMVG::load_calibrated_images(path);
		observations_all = fribr::OpenMVG::load_observations(path);
		point_cloud = fribr::OpenMVG::load_point_cloud(path);
		point_renderer.reset(new fribr::PointCloudRenderer(point_cloud));
	}
	else if (extension == "db") {
		cameras_all = fribr::COLMAP::load_calibrated_images(path);
		point_cloud = fribr::COLMAP::load_point_cloud(path);
		point_renderer.reset(new fribr::PointCloudRenderer(point_cloud));
	}
	else {
		std::cout << "Unsupported sfm format: " << extension << std::endl;
		return;
	}

	// Do not use blacklisted images in the DSLR output.
	std::string              blacklist_path = fribr::strip_extension(path) + ".blacklist";
	std::vector<std::string> blacklist = fribr::Blacklist::load_blacklist(blacklist_path);

	std::vector<int> dslr_to_all_remap;
	for (int i = 0; i < static_cast<int>(cameras_all.size()); ++i)
	{
		fribr::CalibratedImage &camera = cameras_all[i];
		if (!fribr::find_any(blacklist, camera.get_image_name()))
		{
			dslr_to_all_remap.push_back(i);
			cameras_dslr.push_back(camera);
		}
	}

	if (extension == "json") {
		observations.points = observations_all.points;
		for (size_t i = 0; i < cameras_dslr.size(); ++i)
		{
			int index_all = dslr_to_all_remap[i];
			observations.observations.push_back(observations_all.observations[index_all]);
		}
	}
}

bool save_ply(const std::string &path, const std::vector<float> &positions)
{
	std::ofstream cloud_stream(path, std::ios_base::out);
	if (!cloud_stream.good())
	{
		std::cerr << "Could not open output file: " << path << std::endl;
		return false;
	}

	cloud_stream << "ply" << std::endl;
	cloud_stream << "format ascii 1.0" << std::endl;
	cloud_stream << "element vertex " << positions.size() / 3 << std::endl;
	cloud_stream << "property float x" << std::endl;
	cloud_stream << "property float y" << std::endl;
	cloud_stream << "property float z" << std::endl;
	cloud_stream << "end_header" << std::endl;

	for (int i = 0; i < static_cast<int>(positions.size()); i += 3)
		cloud_stream << positions[i + 0] << " " << -positions[i + 1] << " " << -positions[i + 2] << std::endl;

	cloud_stream.close();
	return true;
}

cv::Mat4b convertRGB32FtoRGBA(const cv::Mat3f &imgF)
{
	cv::Mat4b out(imgF.rows, 3 * imgF.cols);
	for (int y = 0; y < imgF.rows; ++y)
	for (int x = 0; x < imgF.cols; ++x)
	for (int k = 0; k < 3; k++) 
	{
		unsigned char const * p = reinterpret_cast<unsigned char const*>(&imgF(y, x)[k]);
		for (std::size_t i = 0; i != sizeof(float); ++i)
		{
			int ii = i == 3 ? 3 : 2 - i;
			out(y, k * imgF.cols + x)[i] = p[ii];
		}
	}
	return out;
}

cv::Mat3f convertRGBAtoRGB32F(const cv::Mat4b &imgRGBA)
{
	cv::Mat3f out(imgRGBA.rows, imgRGBA.cols / 3);
	for (int y = 0; y < out.rows; ++y) 
	for (int x = 0; x < out.cols; ++x)
	for (int k = 0; k < 3; k++) {
		unsigned char * p = reinterpret_cast<unsigned char*>(&out(y, x)[k]);
		for (std::size_t i = 0; i != sizeof(float); ++i)
		{
			int ii = i == 3 ? 3 : 2 - i;
			p[i] = imgRGBA(y, k * out.cols + x)[ii];
		}
	}
	return out;
}

std::vector<std::string> list_files_in_folder(const std::string& folder)
{
	std::vector<std::string> files;
	DIR *dir = opendir(folder.c_str());
	if (!dir)
		return files;

	struct dirent *entry;
	while ((entry = readdir(dir)))
		files.push_back(entry->d_name);

	closedir(dir);
	std::sort(files.begin(), files.end());
	return files;
}

} // anonymous namespace

struct FeatureMatcherArgs : public sibr::BasicDatasetArgs {
	sibr::Arg<bool> flipMesh = { "flip_mesh", "Flip the mesh after loading it (legacyy.)" };
};

int main(int argc, char* argv[])
{
	sibr::CommandLineArgs::parseMainArgs(argc, argv);
	FeatureMatcherArgs args;

	const std::string folder_path = args.dataset_path.get();
	int num_extra = folder_path.length();
	std::string padding_string(num_extra, '=');
	std::cout << std::endl;
	std::cout << " =============================================" << padding_string << std::endl;
	std::cout << " =============================================" << padding_string << std::endl;
	std::cout << " ==== RUNNING FEATURE MATCHER FOR SCENE: " << folder_path << " ====" << std::endl;
	std::cout << " =============================================" << padding_string << std::endl;
	std::cout << " =============================================" << padding_string << std::endl;
	std::cout << std::endl;

	const std::string images_path           = folder_path + "/colmap/stereo/images";
	const std::string window_mesh_path      = folder_path + "/capreal/mesh_windows.obj";
	const std::string window_component_path = folder_path + "/semantic_reflections/masks_per_window";
	const std::string carless_images_path   = folder_path + "/semantic_reflections/carless_images";
	const CoordinateSystem mesh_frame = args.flipMesh ? RH_Y_DOWN : RH_Y_UP;

	if (!directory_exists(folder_path))
	{
		std::cout << "Could not find input images at: " << folder_path << std::endl;
		return 0;
	}

	if (!directory_exists(images_path))
	{
		std::cout << "Could not find reference images at: " << images_path << std::endl;
		return 0;
	}

	if (!file_exists(window_mesh_path))
	{
		std::cout << "Could not find window meshes at: " << window_mesh_path << std::endl;
		return 0;
	}

	if (!directory_exists(window_component_path))
	{
		std::cout << "Could not find window component masks at: " << window_component_path << std::endl;
		return 0;
	}
#if LOAD_CARLESS_BG
	if (!directory_exists(carless_images_path))
	{
		std::cout << "Could not find carless images at: " << carless_images_path << std::endl;
		return 0;
	}
#endif
	// We need a GL context for CalibratedImage and PatchCloud
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		std::cerr << "Could not initialize glfw." << std::endl;
		return 1;
	}

	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	GLFWwindow* window = glfwCreateWindow(1280, 720, "SemanticIBR feature matcher", NULL, NULL);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);

	glewExperimental = GL_TRUE;
	GLenum glew_error = glewInit();
	if (GLEW_OK != glew_error)
	{
		std::cerr << "glew init failed: " << glewGetErrorString(glew_error) << std::endl;
		return 1;
	}

	std::cout << "Loading SfM scene..." << std::endl;
	CalibratedImage::Vector cameras;
	PointCloud::Ptr         point_cloud;
	PointCloudRenderer::Ptr point_renderer;
	Observations            observations;
	try
	{
		const std::string sfm_path = folder_path + "/colmap/database.db";
		load_calibrated_images(sfm_path, cameras, point_cloud, point_renderer, observations);
	}
	catch (const std::exception &e)
	{
		std::cerr << "Could not load SfM reconstruction " << e.what() << std::endl;
		return 1;
	}

	std::unordered_map<std::string, int> name_to_sfm_index_map;
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
		name_to_sfm_index_map[cameras[i].get_image_name()] = i;

	std::cout << "Loading mesh..." << std::endl;
	Scene::Ptr scene;
	try
	{
		scene.reset(new Scene(folder_path + "/capreal/mesh_final.obj", mesh_frame, true));
	}
	catch (const std::exception &e)
	{
		std::cout << e.what() << std::endl;
		std::cerr << "Could not find mesh /capreal/mesh_final.obj in the scene directory: " << folder_path << std::endl;
		return 1;
	}

	std::cout << "Initializing mesh ray caster..." << std::endl;
	patcher::MeshRayTracer mesh_ray_tracer;
	mesh_ray_tracer.set_scene(scene);

	std::cout << "Estimating scene radius and up vector..." << std::endl;
	Eigen::Vector3f avg_cam_up(0.0f, 0.0f, 0.0f);
	for (CalibratedImage &camera : cameras)
		avg_cam_up += camera.get_orientation() * Eigen::Vector3f(0.0f, 1.0f, 0.0f);
	avg_cam_up.normalize();

	if (scene->get_meshes().size() > 1)
		std::cout << "Warning: Found more than one mesh in the scene. Using only mesh 0..." << std::endl;

	std::vector<float> scene_positions = scene->get_meshes()[0]->get_vertices();
	std::vector<float> scene_normals   = scene->get_meshes()[0]->get_normals();
	if (scene_positions.empty() || scene_normals.empty())
	{
		std::cout << "Unexpected mesh format. Expected to have normals and positions." << std::endl;
		if (scene_positions.empty())
			std::cout << " Did not find positions.";
		if (scene_normals.empty())
			std::cout << " Did not find normals.";
		std::cout << std::endl;
		return 0;
	}

	Eigen::Vector3f up(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < static_cast<int>(scene_normals.size() / 3); i++)
	{
		int index = i * 3;
		Eigen::Vector3f normal(scene_normals[index + 0], scene_normals[index + 1], scene_normals[index + 2]);
		if (normal.dot(avg_cam_up) > 0.7f)
			up += normal;
	}
	up.normalize();
	std::cout << "Up vector: " << up.transpose() << std::endl;

	Eigen::Array3f bbox_min(1e21f, 1e21f, 1e21f);
	Eigen::Array3f bbox_max(-1e21f, -1e21f, -1e21f);
	for (int i = 0; i < static_cast<int>(scene_positions.size() / 3); i++)
	{
		int index = i * 3;
		Eigen::Array3f v(scene_positions[index + 0], scene_positions[index + 1], scene_positions[index + 2]);
		bbox_min = bbox_min.min(v);
		bbox_max = bbox_max.max(v);
	}
	float scene_diagonal = (bbox_max - bbox_min).matrix().norm();
	

	std::cout << "Loading window meshes..." << std::endl;
	Scene::Ptr window_mesh;
	try
	{
		window_mesh.reset(new Scene(window_mesh_path, mesh_frame, true));
	}
	catch (const std::exception &e)
	{
		std::cout << e.what() << std::endl;
		std::cerr << "Could not find window mesh at: " << window_mesh_path << std::endl;
		return 0;
	}

	if (window_mesh->get_meshes().size() > 1)
		std::cout << "Warning: Found more than one mesh in the window file. Using only mesh 0..." << std::endl;

	std::vector<float>        window_positions = window_mesh->get_meshes()[0]->get_vertices();
	std::vector<float>        window_normals   = window_mesh->get_meshes()[0]->get_normals();
	std::vector<float>        window_texcoords = window_mesh->get_meshes()[0]->get_texture_coordinates();
	std::vector<unsigned int> window_indices   = window_mesh->get_meshes()[0]->get_indices();
	if (window_positions.empty() || window_indices.empty() || window_texcoords.empty())
	{
		std::cout << "Unexpected mesh format. Expected window mesh to have positions,"
			      << " texture coordinates, normals and indices.";
		if (window_positions.empty())
			std::cout << " Did not find positions.";
		if (window_normals.empty())
			std::cout << " Did not find normals.";
		if (window_texcoords.empty())
			std::cout << " Did not find texture coordinates.";
		if (window_indices.empty())
			std::cout << " Did not find indices.";
		std::cout << std::endl;
		return 0;
	}

	std::cout << "Finding connected window components..." << std::endl;
	Eigen::MatrixXi indices_igl(window_indices.size() / 3, 3);
	for (int i = 0; i < static_cast<int>(window_indices.size()); i += 3)
	{
		indices_igl(i / 3, 0) = window_indices[i + 0];
		indices_igl(i / 3, 1) = window_indices[i + 1];
		indices_igl(i / 3, 2) = window_indices[i + 2];
	}
	Eigen::MatrixXi window_components;
	igl::components(indices_igl, window_components);

	int num_window_components = 0;
	for (int i = 0; i < static_cast<int>(window_positions.size() / 3); i++)
		num_window_components = std::max(window_components(i, 0) + 1, num_window_components);
	
	std::cout << "Found " << num_window_components << " components..." << std::endl;
	std::cout << "Computing window centroids..." << std::endl;
	std::vector<Eigen::Vector3f> window_centroids(num_window_components, Eigen::Vector3f::Zero());
	std::vector<Eigen::Vector3f> centroid_normals(num_window_components, Eigen::Vector3f::Zero());
	std::vector<int>             window_ids      (num_window_components, -1);
	std::vector<int>             window_ids_skip	  (num_window_components, 0);
	std::vector<float>           window_centroid_samples(num_window_components, 0.0f);
	for (int i = 0; i < static_cast<int>(window_positions.size() / 3); i++)
	{
		Eigen::Vector3f v(window_positions[i * 3 + 0],
		                  window_positions[i * 3 + 1],
		                  window_positions[i * 3 + 2]);

		Eigen::Vector3f n(window_normals[i * 3 + 0],
		                  window_normals[i * 3 + 1],
		                  window_normals[i * 3 + 2]);

		int component_index = window_components(i, 0);
		window_centroids[component_index]        += v;
		centroid_normals[component_index]        += n;
		window_centroid_samples[component_index] += 1.0f;

		Eigen::Vector2f uv(window_texcoords[i * 2 + 0],
		                   window_texcoords[i * 2 + 1]);

		int window_id = static_cast<int>(uv.x());
		if (window_id != window_ids[component_index] && window_ids[component_index] >= 0)
		{
			std::cout << "Error inconsistent IDs within window component " << component_index
			          << ": " << window_id << " vs " << window_ids[component_index] << "." << std::endl;
		}
		window_ids[component_index] = window_id;
	}

	for (int i = 0; i < num_window_components; ++i)
	{
		if (window_centroid_samples[i] <= 0.0f)
			continue;

		window_centroids[i] /= window_centroid_samples[i];
		centroid_normals[i].normalize();
	}

	std::cout << "Loading carless images and window component masks..." << std::endl;
	std::vector<cv::Mat1b> window_component_masks(cameras.size());
	std::vector<cv::Mat3b> carless_images(cameras.size());
	#ifdef NDEBUG
	#pragma omp parallel for 
	#endif
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		CalibratedImage &camera = cameras[i];
		camera.load_image(CalibratedImage::CorrectRadialDistortion, CalibratedImage::CPUOnly);
		
		const std::string mask_path = window_component_path + "/" + camera.get_image_name() + "_mask_out.png";
		window_component_masks[i] = cv::imread(mask_path, cv::IMREAD_UNCHANGED);
		cv::resize(window_component_masks[i], window_component_masks[i], camera.get_image().size(), 0.0, 0.0, cv::INTER_NEAREST);
#if LOAD_CARLESS_BG
		const std::string carless_path = carless_images_path + "/" + camera.get_image_name() + ".jpg";
		carless_images[i] = cv::imread(carless_path, cv::IMREAD_UNCHANGED);
#endif
	}

	std::cout << "Initializing car window ray caster..." << std::endl;
	patcher::MeshRayTracer window_ray_tracer;
	window_ray_tracer.set_scene(window_mesh);

	std::vector<Eigen::Matrix4f> world_to_clip_list(cameras.size());
	for (int i = 0; i < static_cast<int>(cameras.size()); i++)
	{
		Eigen::Matrix4f cam_to_clip = cameras[i].get_camera_to_clip(0.01f, 2000.0f);
		Eigen::Matrix4f world_to_cam = cameras[i].get_world_to_camera().matrix();
		world_to_clip_list[i] = cam_to_clip * world_to_cam;
	}

	std::cout << "Computing average baseline..." << std::endl;
	float average_baseline = 0.0f;
	for (int i = 0; i < static_cast<int>(cameras.size()); i++)
	{
		float best_baseline = 1e21f;
		for (int j = 0; j < static_cast<int>(cameras.size()); j++)
			if (i != j) best_baseline = std::min(best_baseline, (cameras[i].get_position() - cameras[j].get_position()).norm());
		average_baseline += best_baseline;
	}
	average_baseline /= cameras.size();

	struct CameraWithNeighbors
	{
		int                                    ref_cam_index;
		float                                  window_area_pixels;
		int                                    feature_count;
		std::vector<int>                       neigh_cam_indices;
		std::vector<std::vector<cv::KeyPoint>> ref_matched_per_neighbor;
		std::vector<std::vector<cv::KeyPoint>> neigh_matched_per_neighbor;
	};

	struct WindowMatchingCameras
	{
		std::vector<CameraWithNeighbors> reference_cams;
	};

	static const int   NUM_REFS_PER_WINDOW           = 10;
	static const int   NUM_NEIGHBORS_PER_REF         = 10;
	static const int   MIN_MATCHES_PER_WINDOW        = 16;    // Discard small outlier windows with very few feature matches.
	static const float NEIGH_FRONT_COSINE_THRESHOLD  = 0.8f;  // Expressed as a cosine, i.e. ~36 degrees
	static const float HORIZONTAL_BIAS               = 0.25f; // Biases the distance comparison to only consider horizontal differences (0 = normal euclidean, 1 = horizontal only)
	static const float BASELINE_RATIO_THRESHOLD      = 0.0f;  // Forces new reference cameras to be at least this distance away from existing ones. 1.5 seems to work well in most scenes. 0.0 disables this.
	static const float COSINE_THRESHOLD_IMAGE_CENTER = 0.7f;  // Ensure that the line-o-sight and the front vector are similar, to keep the window in the image center.
	
	std::cout << "Finding images for window shape estimation..." << std::endl;
	std::vector<WindowMatchingCameras> ref_cams_per_window(num_window_components);
	for (int i = 0; i < num_window_components; ++i)
	{
		const int window_id = window_ids[i];
		if (window_id < 0)
			continue;

		std::vector<float> heights;
		for (int j = 0; j < static_cast<int>(cameras.size()); ++j)
		{
			CalibratedImage &camera = cameras[j];
			heights.emplace_back(up.dot(camera.get_position()));
		}
		std::sort(heights.begin(), heights.end());

		std::vector<float> distances;
		for (int j = 0; j < static_cast<int>(cameras.size()); ++j)
		{
			CalibratedImage &camera       = cameras[j];
			Eigen::Vector3f front         = camera.get_forward();
			Eigen::Vector3f line_of_sight = (window_centroids[i] - camera.get_position()).normalized();
			Eigen::Vector3f normal        = centroid_normals[i];

			if (-front.dot(normal) <= 0.0f || -line_of_sight.dot(normal) <= 0.0f || line_of_sight.dot(front) < COSINE_THRESHOLD_IMAGE_CENTER)
				continue;

			distances.emplace_back((window_centroids[i] - camera.get_position()).norm());
		}
		std::sort(distances.begin(), distances.end());
		const float distance_threshold = 2.0f * distances[distances.size() / 4];

		typedef std::pair<float, int> RankedIndex;
		std::vector<RankedIndex> ranked_ref_indices;
		for (int j = 0; j < static_cast<int>(cameras.size()); ++j)
		{
			CalibratedImage &camera = cameras[j];

			#define DISCARD_TOP_AND_BOTTOM 1
			#if DISCARD_TOP_AND_BOTTOM
			// Discard cameras in the top and bottom rows -- we want feature point matches in all directions
			// (top, down, bottom, left)
			float height = up.dot(camera.get_position());
			if (std::abs(height - heights[static_cast<int>(0.05f * heights.size())]) < average_baseline ||
				std::abs(height - heights[static_cast<int>(0.95f * heights.size())]) < average_baseline)
				continue;
			#endif

			#define RANK_FROM_LINE_OF_SIGHT 1
			#define RANK_FROM_FEATURES      0
			#define RANK_FROM_BG_DIFFERENCE 0
			#if RANK_FROM_LINE_OF_SIGHT
			Eigen::Vector3f front         = camera.get_forward();
			Eigen::Vector3f line_of_sight = (window_centroids[i] - camera.get_position()).normalized();
			Eigen::Vector3f normal        = centroid_normals[i];

			float distance = (window_centroids[i] - camera.get_position()).norm();
			if (distance > distance_threshold)
				continue;

			if (-front.dot(normal) <= 0.0f || -line_of_sight.dot(normal) <= 0.0f || line_of_sight.dot(front) < COSINE_THRESHOLD_IMAGE_CENTER)
				continue;

			ranked_ref_indices.emplace_back(line_of_sight.dot(normal), j);
			#elif RANK_FROM_FEATURES
			cv::Mat3b reference_image = camera.get_image();
			cv::Mat1b reference_mask  = window_component_masks[j] == (1 + window_id);

			cv::Mat1b reference_grey;
			cv::cvtColor(reference_image, reference_grey, CV_BGR2GRAY);

			cv::Ptr<cv::ORB> orb = cv::ORB::create(4096, 1.2, 8, 31, 0, 2, 0, 31, 8);
			std::vector<cv::KeyPoint> reference_keypoints;
			orb->detect(reference_grey, reference_keypoints, reference_mask);
			ranked_ref_indices.emplace_back(-1.0f * reference_keypoints.size(), j);
			#elif RANK_FROM_BG_DIFFERENCE
			cv::Mat3b lowres_img;
			cv::resize(camera.get_image(), lowres_img, carless_images[j].size(), 0.0, 0.0, cv::INTER_AREA);

			cv::Mat1b highres_mask = window_component_masks[j] == (1 + window_id);
			cv::Mat1b lowres_mask;
			cv::resize(highres_mask, lowres_mask, carless_images[j].size(), 0.0, 0.0, cv::INTER_NEAREST);

			cv::Mat3b diff;
			cv::absdiff(lowres_img, carless_images[j], diff);

			float SSE = 0.0f;
			static const unsigned char DIFF_CLAMP = 50;
			for (int y = 0; y < lowres_img.rows; ++y)
			for (int x = 0; x < lowres_img.cols; ++x)
			{
				for (int c = 0; c < 3; ++c)
					diff(y, x)[c] = static_cast<unsigned char>(std::min(DIFF_CLAMP, diff(y, x)[c]) * 255.0f / DIFF_CLAMP);

				if (lowres_mask(y, x)) 
					SSE += (diff(y, x)[0] +  diff(y, x)[1] + diff(y, x)[2]) / 3.0f;
			}

			ranked_ref_indices.emplace_back(-SSE, j);
			#endif
		}
		std::sort(ranked_ref_indices.begin(), ranked_ref_indices.end());

		for (int jj = 0; ; ++jj)
		{
			if (jj >= static_cast<int>(ranked_ref_indices.size()) ||
				static_cast<int>(ref_cams_per_window[i].reference_cams.size()) >= NUM_REFS_PER_WINDOW)
				break;

			const int j = ranked_ref_indices[jj].second;

			bool too_close = false;
			for (CameraWithNeighbors &other : ref_cams_per_window[i].reference_cams)
			{
				if ((cameras[j].get_position() - cameras[other.ref_cam_index].get_position()).norm() < BASELINE_RATIO_THRESHOLD * average_baseline)
				{
					too_close = true;
					break;
				}
			}
			if (too_close)
				continue;

			CalibratedImage &camera = cameras[j];
			Eigen::Vector3f front = camera.get_forward();

			CameraWithNeighbors ref_cam;
			ref_cam.ref_cam_index = j;

			typedef std::pair<float, int> RankedIndex;
			std::vector<RankedIndex> ranked_neighbor_indices;
			for (int k = 0; k < static_cast<int>(cameras.size()); ++k)
			{
				if (k == j)
					continue;

				CalibratedImage &neighbor   = cameras[k];
				Eigen::Vector3f neigh_front = neighbor.get_forward();

				if (neigh_front.dot(front) < NEIGH_FRONT_COSINE_THRESHOLD)
					continue;

				Eigen::Vector3f ref_position   = camera.get_position();
				Eigen::Vector3f neigh_position = neighbor.get_position();

				Eigen::Vector3f horizontal_ref_pos   = (ref_position   - HORIZONTAL_BIAS * ref_position.dot(up)   * up);
				Eigen::Vector3f horizontal_neigh_pos = (neigh_position - HORIZONTAL_BIAS * neigh_position.dot(up) * up);

				ranked_neighbor_indices.emplace_back((horizontal_ref_pos - horizontal_neigh_pos).norm(), k);
			}
			std::sort(ranked_neighbor_indices.begin(), ranked_neighbor_indices.end());

			#define RANK_NEIGHBORS_FROM_BG_DIFFERENCE 0
			#if RANK_NEIGHBORS_FROM_BG_DIFFERENCE
			// Rerank nearby cameras according to bg difference, this multiplier tells us how many of the nearby cameras we should include for consideration.
			static const int PROXIMITY_MULTIPLIER = 4; 
			std::vector<RankedIndex> reranked_neighbor_indices;
			for (int kk = 0; kk < std::min(static_cast<int>(ranked_neighbor_indices.size()), PROXIMITY_MULTIPLIER * NUM_NEIGHBORS_PER_REF); ++kk)
			{
				const int k = ranked_neighbor_indices[kk].second;
				cv::Mat3b lowres_img;
				cv::resize(cameras[k].get_image(), lowres_img, carless_images[k].size(), 0.0, 0.0, cv::INTER_AREA);

				cv::Mat1b highres_mask = window_component_masks[k] == (1 + window_id);
				cv::Mat1b lowres_mask;
				cv::resize(highres_mask, lowres_mask, carless_images[k].size(), 0.0, 0.0, cv::INTER_NEAREST);

				cv::Mat3b diff;
				cv::absdiff(lowres_img, carless_images[k], diff);

				float SSE = 0.0f;
				static const unsigned char DIFF_CLAMP = 50;
				for (int y = 0; y < lowres_img.rows; ++y)
				for (int x = 0; x < lowres_img.cols; ++x)
					if (lowres_mask(y, x))
						SSE += (std::min(DIFF_CLAMP, diff(y, x)[0]) +
						        std::min(DIFF_CLAMP, diff(y, x)[1]) +
						        std::min(DIFF_CLAMP, diff(y, x)[2])) / 3.0f;

				reranked_neighbor_indices.emplace_back(-SSE, k);
			}
			std::sort(reranked_neighbor_indices.begin(), reranked_neighbor_indices.end());
			ranked_neighbor_indices.swap(reranked_neighbor_indices);
			#endif

			for (int kk = 0; kk < std::min(static_cast<int>(ranked_neighbor_indices.size()), NUM_NEIGHBORS_PER_REF); ++kk)
			{
				const int k = ranked_neighbor_indices[kk].second;
				ref_cam.neigh_cam_indices.emplace_back(k);
			}
			
			ref_cam.ref_matched_per_neighbor.resize(ref_cam.neigh_cam_indices.size());
			ref_cam.neigh_matched_per_neighbor.resize(ref_cam.neigh_cam_indices.size());
			ref_cams_per_window[i].reference_cams.emplace_back(ref_cam);
		}
	}

#if DUMP_VISUALIZATIONS
	const std::string output_directory = folder_path + "/semantic_reflections/feature_matcher";
	if (!directory_exists(output_directory))
		make_directory(output_directory);
#endif


	
	for (int window_idx = 0; window_idx < num_window_components; ++window_idx)
	{
		const int window_id = window_ids[window_idx];
		if (window_id < 0)
			continue;

#if DUMP_VISUALIZATIONS
		const std::string window_output_directory = output_directory + "/" + std::to_string(window_id);
		if (!fribr::directory_exists(window_output_directory))
			fribr::make_directory(window_output_directory);
#endif

		std::cout << "Feature point matching for window: " << window_id << std::endl;
		#ifdef NDEBUG
		#pragma omp parallel for
		#endif
		for (int ri = 0; ri < static_cast<int>(ref_cams_per_window[window_idx].reference_cams.size()); ++ri)
		{
			CameraWithNeighbors          &ref_cam_data   = ref_cams_per_window[window_idx].reference_cams[ri];
			int                          reference_index = ref_cam_data.ref_cam_index;
			const fribr::CalibratedImage &ref_cam        = cameras[reference_index];

			cv::Mat3b reference_image = ref_cam.get_image();
			cv::Mat1b reference_mask  = window_component_masks[reference_index] == (1 + window_id);


			Eigen::Matrix4f ref_world_to_cam   = ref_cam.get_world_to_camera().matrix();
			Eigen::Matrix4f ref_cam_to_world   = ref_world_to_cam.inverse();
			Eigen::Matrix3f ref_intrinsics     = ref_cam.get_intrinsics();
			Eigen::Matrix3f ref_inv_intrinsics = ref_intrinsics.inverse();
			Eigen::Vector2i ref_resolution     = ref_cam.get_scaled_resolution();
			Eigen::Vector3f ref_orig           = ref_cam.get_position();

			cv::Mat1b reference_grey;
			cv::cvtColor(reference_image, reference_grey, cv::COLOR_BGR2GRAY);

			cv::Ptr<cv::ORB> orb = cv::ORB::create(4096, 1.2, 8, 31, 0, 2, cv::ORB::ScoreType::HARRIS_SCORE, 31, 8);
			cv::Mat                   reference_descriptors;
			std::vector<cv::KeyPoint> reference_keypoints;
			orb->detectAndCompute(reference_grey, reference_mask, reference_keypoints, reference_descriptors);

			ref_cam_data.window_area_pixels = cv::countNonZero(reference_mask);
			ref_cam_data.feature_count      = static_cast<int>(reference_keypoints.size());

			if (reference_keypoints.size() < MIN_MATCHES_PER_WINDOW)
				continue;

			cv::FlannBasedMatcher matcher(cv::makePtr<cv::flann::LshIndexParams>(12, 20, 2));
			matcher.add(reference_descriptors);
			matcher.train();

			#if DUMP_VISUALIZATIONS

			const std::string ref_cam_output_directory = window_output_directory + "/" + ref_cam.get_image_name();
			if (!fribr::directory_exists(ref_cam_output_directory))
				fribr::make_directory(ref_cam_output_directory);
			
			{
				cv::Mat3b debug_image = reference_image.clone();
				int r = 2;
				for (int i = 0; i < reference_keypoints.size(); i++)
				{
					cv::circle(debug_image, reference_keypoints[i].pt, r, cv::Scalar(0, 0, 255), -1, 8, 0);
				}
				cv::imwrite(ref_cam_output_directory + "/debug_corners.jpg", debug_image);
			}
			#endif

			for (int ni = 0; ni < static_cast<int>(ref_cam_data.neigh_cam_indices.size()); ++ni)
			{
				const int                    neighbor_index = ref_cam_data.neigh_cam_indices[ni];
				const fribr::CalibratedImage &neigh_cam     = cameras[neighbor_index];

				cv::Mat3b neighbor_image = neigh_cam.get_image();
				cv::Mat1b neighbor_mask  = window_component_masks[neighbor_index] == (1 + window_id);
				cv::Mat1b neighbor_grey;
				cv::cvtColor(neighbor_image, neighbor_grey, cv::COLOR_BGR2GRAY);

				Eigen::Matrix4f neigh_world_to_cam   = neigh_cam.get_world_to_camera().matrix();
				Eigen::Matrix4f neigh_cam_to_world   = neigh_world_to_cam.inverse();
				Eigen::Matrix3f neigh_intrinsics     = neigh_cam.get_intrinsics();
				Eigen::Matrix3f neigh_inv_intrinsics = neigh_intrinsics.inverse();
				Eigen::Vector2i neigh_resolution     = neigh_cam.get_scaled_resolution();
				Eigen::Vector3f neigh_orig           = neigh_cam.get_position();

				cv::Mat                   neighbor_descriptors;
				std::vector<cv::KeyPoint> neighbor_keypoints;
				orb->detectAndCompute(neighbor_grey, neighbor_mask, neighbor_keypoints, neighbor_descriptors);
				if (neighbor_keypoints.size() < MIN_MATCHES_PER_WINDOW)
					continue;

				std::vector<std::vector<cv::DMatch>> matches;
				matcher.knnMatch(neighbor_descriptors, matches, 2);

				#define USE_BEST_BUDDY_MATCHING 1
				#if USE_BEST_BUDDY_MATCHING
				cv::FlannBasedMatcher neigh_matcher(cv::makePtr<cv::flann::LshIndexParams>(12, 20, 2));
				neigh_matcher.add(neighbor_descriptors);
				neigh_matcher.train();
				std::vector<std::vector<cv::DMatch>> neigh_matches;
				neigh_matcher.knnMatch(reference_descriptors, neigh_matches, 2);
				#endif

				std::vector<cv::KeyPoint> ref_matched, neigh_matched;
				for (int j = 0; j < static_cast<int>(matches.size()); j++)
				{
					if (matches[j].size() < 2)
						continue;

					cv::DMatch first  = matches[j][0];
					cv::DMatch second = matches[j][1];

					static const double RATIO_THRESHOLD = 0.95;
					if (first.distance >= RATIO_THRESHOLD * second.distance)
						continue;

					#if USE_BEST_BUDDY_MATCHING
					bool is_best_buddy = false;
					for (int k = 0; k < static_cast<int>(neigh_matches.size()); k++)
					{
						if (neigh_matches[k].size() < 2)
							continue;

						cv::DMatch neigh_first  = neigh_matches[k][0];
						cv::DMatch neigh_second = neigh_matches[k][1];

						if (neigh_first.queryIdx != first.trainIdx ||
							neigh_first.trainIdx != first.queryIdx)
							continue;

						if (neigh_first.distance >= RATIO_THRESHOLD * neigh_second.distance)
							continue;

						is_best_buddy = true;
						break;
					}

					if (!is_best_buddy)
						continue;
					#endif

					neigh_matched.push_back(neighbor_keypoints[first.queryIdx]);
					ref_matched.push_back(reference_keypoints[first.trainIdx]);
				}

				std::vector<cv::KeyPoint> ref_cleaned, neigh_cleaned;
				for (int j = 0; j < static_cast<int>(ref_matched.size()); ++j)
				{
					cv::Point2f ref_pt   = ref_matched[j].pt;
					cv::Point2f neigh_pt = neigh_matched[j].pt;

					// Remove any feature points whose motion can be explained by the window geometry
					#define FILTER_WINDOW 1
					#if FILTER_WINDOW
					static const float WINDOW_TOLERANCE = 30.0f;
					Eigen::Vector4f p_window;
					{
						fribr::RTResult r;
						r.tri = 0;

						Eigen::Vector3f far_plane = unproject(ref_pt.x, ref_resolution.y() - ref_pt.y - 1, 1000.0f,
							ref_inv_intrinsics, ref_cam_to_world);

						r = window_ray_tracer.ray_cast(ref_orig, far_plane - ref_orig);
						if (!r.tri)
							continue;

						p_window = Eigen::Vector4f(r.p[0], r.p[1], r.p[2], 1.0f);
					}

					Eigen::Vector3f n_image_window_pos;
					if (!project_if_visible(p_window.head<3>(), world_to_clip_list[neighbor_index], neigh_cam, n_image_window_pos))
						continue;

					n_image_window_pos.y() = neigh_resolution.y() - 1.0f - n_image_window_pos.y();
					if ((Eigen::Vector2f(neigh_pt.x, neigh_pt.y) - n_image_window_pos.head<2>()).norm() < WINDOW_TOLERANCE)
						continue;
					#endif

					// Remove any feature points whose motion can be explained by the reconstructed 3D geometry of the scene.
					#define FILTER_BACKGROUND 0
					#if FILTER_BACKGROUND
					static const float BACKGROUND_TOLERANCE = 30.0f;
					Eigen::Vector4f p_mesh;
					{
						fribr::RTResult r;
						r.tri = 0;

						Eigen::Vector3f far_plane = unproject(ref_pt.x, ref_resolution.y() - ref_pt.y - 1, 1000.0f,
			 				ref_inv_intrinsics, ref_cam_to_world);

						r = mesh_ray_tracer.ray_cast(ref_orig, far_plane - ref_orig);
						if (!r.tri)
							continue;

						p_mesh = Eigen::Vector4f(r.p[0], r.p[1], r.p[2], 1.0f);
					}

					Eigen::Vector3f n_image_mesh_pos;
					if (!project_if_visible(p_mesh.head<3>(), world_to_clip_list[neighbor_index], neigh_cam, n_image_mesh_pos))
						continue;

					n_image_mesh_pos.y() = neigh_resolution.y() - 1.0f - n_image_mesh_pos.y();
					if ((Eigen::Vector2f(neigh_pt.x, neigh_pt.y) - n_image_mesh_pos.head<2>()).norm() < BACKGROUND_TOLERANCE)
						continue;
					#endif

					neigh_cleaned.push_back(neigh_matched[j]);
					ref_cleaned.push_back(ref_matched[j]);
				}

				// Remove outlier feature points, whose motion is very different from the overall (median) motion
				#define FILTER_MEDIAN 0
				#if FILTER_MEDIAN
				static const float MEDIAN_TOLERANCE = 30.0f;
				std::vector<float> delta_x, delta_y;
				for (int j = 0; j < static_cast<int>(ref_cleaned.size()); ++j)
				{
					cv::Point2f ref_pt = ref_cleaned[j].pt;
					cv::Point2f neigh_pt = neigh_cleaned[j].pt;
					delta_x.emplace_back(ref_pt.x - neigh_pt.x);
					delta_y.emplace_back(ref_pt.y - neigh_pt.y);
				}
				std::sort(delta_x.begin(), delta_x.end());
				std::sort(delta_y.begin(), delta_y.end());

				std::vector<cv::KeyPoint> ref_median, neigh_median;
				for (int j = 0; j < static_cast<int>(ref_cleaned.size()); ++j)
				{
					cv::Point2f ref_pt   = ref_cleaned[j].pt;
					cv::Point2f neigh_pt = neigh_cleaned[j].pt;

					float dx = ref_pt.x - neigh_pt.x;
					float dy = ref_pt.y - neigh_pt.y;

					float err_x = dx - delta_x[delta_x.size() / 2];
					float err_y = dy - delta_y[delta_y.size() / 2];

					if (std::sqrt(err_x * err_x + err_y * err_y) > MEDIAN_TOLERANCE)
						continue;

					neigh_median.push_back(neigh_cleaned[j]);
					ref_median.push_back(ref_cleaned[j]);
				}
				#else
				std::vector<cv::KeyPoint> ref_median   = ref_cleaned;
				std::vector<cv::KeyPoint> neigh_median = neigh_cleaned;
				#endif

				// Discard images with too few matches
				#define FILTER_MATCHES 0
				#if FILTER_MATCHES
				static const int MINIMUM_MATCHES = 5;
				if (ref_median.size() < MINIMUM_MATCHES)
				{
					ref_median.clear();
					neigh_median.clear();
				}
				#endif

				ref_cam_data.ref_matched_per_neighbor[ni]   = ref_median;
				ref_cam_data.neigh_matched_per_neighbor[ni] = neigh_median;

				#if DUMP_VISUALIZATIONS
				{
					cv::Mat3b side_by_side(reference_image.rows, 2 * reference_image.cols);
					reference_image.copyTo(side_by_side(cv::Rect(0, 0, reference_image.cols, reference_image.rows)));
					neighbor_image.copyTo(side_by_side(cv::Rect(reference_image.cols, 0, reference_image.cols, reference_image.rows)));

					for (int j = 0; j < static_cast<int>(ref_median.size()); ++j)
					{
						cv::Point2f neigh_pt = neigh_median[j].pt;
						neigh_pt.x += reference_image.cols;
						cv::line(side_by_side, ref_median[j].pt, neigh_pt, cv::Scalar(0, 255, 0));
						cv::circle(side_by_side, ref_median[j].pt, 2, cv::Scalar(0, 0, 255), -1, 8, 0);
						cv::circle(side_by_side, neigh_pt, 2, cv::Scalar(255, 0, 0), -1, 8, 0);
					}

					cv::imwrite(ref_cam_output_directory + "/matched_with_" + neigh_cam.get_image_name() + ".jpg", side_by_side);
				}
				#endif
			}
		}

		bool is_invalid_window = true;
		for (int ri = 0; ri < static_cast<int>(ref_cams_per_window[window_idx].reference_cams.size()); ++ri)
		{
			CameraWithNeighbors &ref_cam_data = ref_cams_per_window[window_idx].reference_cams[ri];
			for (int ni = 0; ni < static_cast<int>(ref_cam_data.ref_matched_per_neighbor.size()); ++ni)
			if (ref_cam_data.ref_matched_per_neighbor[ni].size() >= MIN_MATCHES_PER_WINDOW)
			{
				is_invalid_window = false;
				break;
			}
		}
		if (is_invalid_window) {
			std::cout << "Not enough features for window ID: " << window_ids[window_idx] << "." << std::endl;
			window_ids_skip[window_idx] = -1;
		}
	}


	struct WindowParameters {
		int id;
		float radx;
		float rady;
	};
	std::vector<WindowParameters> result_params;

	for (int window_idx = 0; window_idx < num_window_components; ++window_idx)
	{
		const int window_id = window_ids[window_idx];
		if (window_ids_skip[window_idx] < 0) {
			WindowParameters result = { window_id, -1.0f, -1.0f };
			result_params.push_back(result);
			continue;
		}

		std::cout << std::endl << "================================================================"
		          << std::endl << std::endl;

		std::cout << "Estimating shape for window: " << window_id << std::endl;

		// For each window, we assume that if the closest window neighbor has the same orientation, the current window is a side window.
		float           nearest_centroid_distance = 1e21f;
		Eigen::Vector3f delta_to_centroid = centroid_normals[window_idx];
		for (int owi = 0; owi < static_cast<int>(window_centroids.size()); ++owi)
		{
			if (owi == window_idx)
				continue;

			if (window_ids_skip[owi] < 0)
				continue;

			Eigen::Vector3f delta = window_centroids[owi] - window_centroids[window_idx];
			if (delta.norm() < nearest_centroid_distance)
			{
				nearest_centroid_distance = delta.norm();
				delta_to_centroid = delta.normalized();
			}
		}

		static const float SIDE_WINDOW_COSINE_THRESHOLD = 0.2f;
		const float cosine_angle_to_centroid = centroid_normals[window_idx].dot(delta_to_centroid);
		const bool  is_side_window = cosine_angle_to_centroid > -SIDE_WINDOW_COSINE_THRESHOLD && cosine_angle_to_centroid < SIDE_WINDOW_COSINE_THRESHOLD;
		std::cout << "Side window: " << (is_side_window ? "Yes." : "No.") << std::endl;

		static const float INLIER_THRESHOLD = 10.0f; // Measured in pixels.
		static const float MIN_RADIUS       = 1.0f;
		static const float MAX_RADIUS       = 40.0f;
		static const int   NUM_HYPOTHESES   = 66;

		int   radii_progress = 0;
		float best_prior     = 1.0f;
		float best_score     = 0.0f;
		int   best_inliers   = 0;
		float best_cost      = 0.0f;
		float best_rad_x     = MAX_RADIUS;
		float best_rad_y     = MAX_RADIUS;
		 
		std::cout << "\rEstimating ellipsoid radii: 0%                " << std::flush;
		#ifdef NDEBUG
		#pragma omp parallel for
		#endif
		for (int rxi = 0; rxi < NUM_HYPOTHESES; ++rxi)
		for (int ryi = 0; ryi < NUM_HYPOTHESES; ++ryi)
		{
			// Using quadratic steps, test all the radii.
			const float rxf   = static_cast<float>(rxi) / (NUM_HYPOTHESES - 1.0f);
			const float ryf   = static_cast<float>(ryi) / (NUM_HYPOTHESES - 1.0f);
			const float rad_x = (MAX_RADIUS - MIN_RADIUS) * rxf * rxf + MIN_RADIUS;
			const float rad_y = (MAX_RADIUS - MIN_RADIUS) * ryf * ryf + MIN_RADIUS;

			Eigen::Vector3f ellipsoid_radii(rad_x, rad_x, rad_y);

			float cost    = 0.0f;
			int   inliers = 0;
			for (CameraWithNeighbors& ref_cam_data : ref_cams_per_window[window_idx].reference_cams)
			{
				int                          reference_index = ref_cam_data.ref_cam_index;
				const fribr::CalibratedImage &ref_cam = cameras[reference_index];

				Eigen::Matrix4f ref_world_to_cam = ref_cam.get_world_to_camera().matrix();
				Eigen::Matrix4f ref_cam_to_world = ref_world_to_cam.inverse();
				Eigen::Matrix3f ref_intrinsics = ref_cam.get_intrinsics();
				Eigen::Matrix3f ref_inv_intrinsics = ref_intrinsics.inverse();
				Eigen::Vector2i ref_resolution = ref_cam.get_scaled_resolution();
				Eigen::Vector3f ref_orig = ref_cam.get_position();

				for (int ni = 0; ni < static_cast<int>(ref_cam_data.neigh_cam_indices.size()); ++ni)
				{
					const int                    neighbor_index = ref_cam_data.neigh_cam_indices[ni];
					const fribr::CalibratedImage &neigh_cam     = cameras[neighbor_index];

					Eigen::Matrix4f neigh_world_to_cam   = neigh_cam.get_world_to_camera().matrix();
					Eigen::Matrix4f neigh_cam_to_world   = neigh_world_to_cam.inverse();
					Eigen::Matrix3f neigh_intrinsics     = neigh_cam.get_intrinsics();
					Eigen::Matrix3f neigh_inv_intrinsics = neigh_intrinsics.inverse();
					Eigen::Vector2i neigh_resolution     = neigh_cam.get_scaled_resolution();
					Eigen::Vector3f neigh_orig           = neigh_cam.get_position();

					const std::vector<cv::KeyPoint> &ref_matched   = ref_cam_data.ref_matched_per_neighbor[ni];
					const std::vector<cv::KeyPoint> &neigh_matched = ref_cam_data.neigh_matched_per_neighbor[ni];

					for (int i = 0; i < static_cast<int>(ref_matched.size()); ++i)
					{
						cv::Point2f ref_pt   = ref_matched[i].pt;
						cv::Point2f neigh_pt = neigh_matched[i].pt;

						fribr::RTResult r;
						r.tri = 0;

						Eigen::Vector3f far_plane = unproject(ref_pt.x, ref_resolution.y() - ref_pt.y - 1, 1000.0f,
							ref_inv_intrinsics, ref_cam_to_world);

						r = window_ray_tracer.ray_cast(ref_orig, far_plane - ref_orig);
						if (!r.tri)
							continue;

						Eigen::Vector3f a(r.tri->m_vertices[0][0], r.tri->m_vertices[0][1], r.tri->m_vertices[0][2]);
						Eigen::Vector3f b(r.tri->m_vertices[1][0], r.tri->m_vertices[1][1], r.tri->m_vertices[1][2]);
						Eigen::Vector3f c(r.tri->m_vertices[2][0], r.tri->m_vertices[2][1], r.tri->m_vertices[2][2]);

						Eigen::Vector3f n_window = (b - a).cross(c - a).normalized();
						Eigen::Vector4f p_window = Eigen::Vector4f(r.p[0], r.p[1], r.p[2], 1.0f);

						Eigen::Vector3f ray_dir = (far_plane - ref_orig).normalized();
						Eigen::Vector3f local_up = up - up.dot(n_window) * n_window;

						float loss = 0.0f;
						Eigen::Vector3f position_ellipsoid = find_reflection_position_ellipsoid(
							ellipsoid_radii, local_up, window_centroids[window_idx], n_window, ray_dir,
							neigh_orig, ref_orig, window_centroids[window_idx], scene_diagonal * 0.5f, loss);

						Eigen::Vector3f neigh_position_image;
						if (project_if_visible(position_ellipsoid, world_to_clip_list[neighbor_index], neigh_cam, neigh_position_image))
						{
							neigh_position_image.y() = neigh_resolution.y() - 1.0f - neigh_position_image.y();
							float l2error = (Eigen::Vector2f(neigh_pt.x, neigh_pt.y) - neigh_position_image.head<2>()).squaredNorm();
							if (l2error <= INLIER_THRESHOLD * INLIER_THRESHOLD)
							{
								cost += l2error;
								inliers++;
							}
						}
					}
				}
			}

			float prior = 1.0f;
			#define USE_PER_WINDOW_SHAPE_PRIOR 0
			#define USE_RATIO_BASED_PRIOR 1
			#if USE_PER_WINDOW_SHAPE_PRIOR
			// Indexed for carpenter & vine, means from all cars
			static const Eigen::Vector2f prior_means[4] = {
				Eigen::Vector2f(1.58f, 14.87f), // Driver window
				Eigen::Vector2f(1.74f, 12.05f), // Passenger window
				Eigen::Vector2f(3.16f, 2.62f),  // Windshield
				Eigen::Vector2f(2.17f, 2.11f),  // Rear window
			};

			static const Eigen::Vector2f prior_stdds[4] = {
				Eigen::Vector2f(0.40f, 9.80f), // Driver window
				Eigen::Vector2f(0.42f, 6.16f), // Passenger window
				Eigen::Vector2f(1.96f, 0.8f),  // Windshield
				Eigen::Vector2f(1.86f, 0.66f), // Rear window
			};

			inliers *= 100.0f * 
				std::exp(
					-(rad_x - prior_means[window_idx].x()) * (rad_x - prior_means[window_idx].x()) / 
					(2.0f * prior_stdds[window_idx].x() * prior_stdds[window_idx].x())
				) *
				std::exp(
					-(rad_y - prior_means[window_idx].y()) * (rad_y - prior_means[window_idx].y()) /
					(2.0f * prior_stdds[window_idx].y() * prior_stdds[window_idx].y())
				);
			#elif USE_RATIO_BASED_PRIOR
			#if 0 // Indexed for carpenter & vine, means from all cars
			static const float y_over_x_means[4] = {
				8.78f, // Driver window
				6.80f, // Passenger window
				1.01f, // Windshield
				1.35f, // Rear window
			};

			static const float y_over_x_stdds[4] = {
				3.13f, // Driver window
				2.22f, // Passenger window
				0.45f, // Windshield
				0.61f, // Rear window
			};
			const float ratio_mean = y_over_x_means[window_idx];
			const float ratio_stdd = y_over_x_stdds[window_idx];
			#elif 0 // Hand crafted general distributions (indexed for carpenter & vine)
			static const float y_over_x_means[4] = {
				7.5f, // Driver window
				7.5f, // Passenger window
				1.0f, // Windshield
				1.0f, // Rear window
			};

			static const float y_over_x_stdds[4] = {
				5.0f,  // Driver window
				5.0f,  // Passenger window
				99.0f, // Windshield
				99.0f, // Rear window
			};
			const float ratio_mean = y_over_x_means[window_idx];
			const float ratio_stdd = y_over_x_stdds[window_idx];
			#else // Hand crafted general distributions (with hacky check for side windows))
			const float ratio_mean = is_side_window ? 8.0f : 1.0f;
			const float ratio_stdd = is_side_window ? 3.5f : 99.0f;
			#endif

			float y_over_x = rad_y / rad_x;
			prior = std::exp(-(y_over_x - ratio_mean) * (y_over_x - ratio_mean) / (2.0f * ratio_stdd * ratio_stdd));
			#endif

			float score = prior * inliers;

			if (inliers > 0)
				cost /= inliers;

			auto lambda_critical = [&]()
			{
				radii_progress++;
				if (radii_progress % 20 == 1)
					std::cout << "\rEstimating ellipsoid radii: "
					          << radii_progress * 100.0f / (NUM_HYPOTHESES * NUM_HYPOTHESES)
					          << "%                 " << std::flush;

				if (score > best_score)
				{
					best_prior   = prior;
					best_score   = score;
					best_cost    = cost;
					best_inliers = inliers;
					best_rad_x   = rad_x;
					best_rad_y   = rad_y;
				}
			};
			#pragma omp critical
			lambda_critical();
		}

		int   num_matched_images      = 0;
		int   max_matched_features    = 0;
		int   max_detected_features   = 0;
		float max_normalized_features = 0.0f;
		float avg_normalized_features = 0.0f;
		float avg_window_area         = 0.0f;
		for (int ri = 0; ri < static_cast<int>(ref_cams_per_window[window_idx].reference_cams.size()); ++ri)
		{
			CameraWithNeighbors &ref_cam_data = ref_cams_per_window[window_idx].reference_cams[ri];

			int max_matched_features_for_ref_cam = 0;
			for (int ni = 0; ni < static_cast<int>(ref_cam_data.ref_matched_per_neighbor.size()); ++ni)
			{
				num_matched_images++;
				max_matched_features_for_ref_cam =
					std::max(max_matched_features_for_ref_cam, static_cast<int>(ref_cam_data.ref_matched_per_neighbor[ni].size()));
			}

			max_matched_features     = std::max(max_matched_features, max_matched_features_for_ref_cam);
			max_detected_features    = std::max(max_detected_features, ref_cam_data.feature_count);
			max_normalized_features  = std::max(max_normalized_features, static_cast<float>(max_matched_features_for_ref_cam) / ref_cam_data.window_area_pixels);
			avg_normalized_features += static_cast<float>(max_matched_features_for_ref_cam) / ref_cam_data.window_area_pixels;
			avg_window_area         += ref_cam_data.window_area_pixels;
		}
		avg_normalized_features /= ref_cams_per_window[window_idx].reference_cams.size();
		avg_window_area         /= ref_cams_per_window[window_idx].reference_cams.size();

		std::cout << "\rEstimating ellipsoid radii: 100%                " << std::endl;
		std::cout << "rad_x: "    << best_rad_x   << " rad_y: " << best_rad_y
			      << " inliers: " << best_inliers << " mse: "   << best_cost
		          << " prior: "   << best_prior   << " score: " << best_score
		          << std::endl;
		std::cout << "max_detected_features: "    << max_detected_features
		          << " max_matched_features: "    << max_matched_features
		          << " avg_inliers: "             << static_cast<float>(best_inliers) / num_matched_images
		          << " avg_window_area: "         << avg_window_area
		          << " avg_normalized_features: " << avg_normalized_features
		          << " max_normalized_features: " << max_normalized_features
		          << std::endl;

		// If the number of matched features make up less than 0.1% of the window area, then fall back to dense photoconsistency.
		static const float FEATURE_RELIABILITY_THRESHOLD = 0.001f;
		if (avg_normalized_features < FEATURE_RELIABILITY_THRESHOLD)
			best_rad_x = best_rad_y = -1.0f;

		WindowParameters result = {window_id, best_rad_x, best_rad_y};
		result_params.push_back(result);

		#if DUMP_VISUALIZATIONS
		std::cout << "Visualizing the estimated ellisoid parameters... " << std::endl;
		Eigen::Vector3f ellipsoid_radii(best_rad_x, best_rad_x, best_rad_y);
		#ifdef NDEBUG
		#pragma omp parallel for
		#endif
		for (int ri = 0; ri < static_cast<int>(ref_cams_per_window[window_idx].reference_cams.size()); ++ri)
		{
			CameraWithNeighbors& ref_cam_data = ref_cams_per_window[window_idx].reference_cams[ri];

			int                          reference_index = ref_cam_data.ref_cam_index;
			const fribr::CalibratedImage &ref_cam        = cameras[reference_index];

			Eigen::Matrix4f ref_world_to_cam   = ref_cam.get_world_to_camera().matrix();
			Eigen::Matrix4f ref_cam_to_world   = ref_world_to_cam.inverse();
			Eigen::Matrix3f ref_intrinsics     = ref_cam.get_intrinsics();
			Eigen::Matrix3f ref_inv_intrinsics = ref_intrinsics.inverse();
			Eigen::Vector2i ref_resolution     = ref_cam.get_scaled_resolution();
			Eigen::Vector3f ref_orig           = ref_cam.get_position();

			const std::string ref_cam_output_directory = output_directory + "/" + std::to_string(window_id) + "/" + ref_cam.get_image_name();

			for (int ni = 0; ni < static_cast<int>(ref_cam_data.neigh_cam_indices.size()); ++ni)
			{
				const int                    neighbor_index = ref_cam_data.neigh_cam_indices[ni];
				const fribr::CalibratedImage &neigh_cam     = cameras[neighbor_index];
				cv::Mat3b                    neighbor_image = neigh_cam.get_image().clone();

				Eigen::Matrix4f neigh_world_to_cam   = neigh_cam.get_world_to_camera().matrix();
				Eigen::Matrix4f neigh_cam_to_world   = neigh_world_to_cam.inverse();
				Eigen::Matrix3f neigh_intrinsics     = neigh_cam.get_intrinsics();
				Eigen::Matrix3f neigh_inv_intrinsics = neigh_intrinsics.inverse();
				Eigen::Vector2i neigh_resolution     = neigh_cam.get_scaled_resolution();
				Eigen::Vector3f neigh_orig           = neigh_cam.get_position();

				const std::vector<cv::KeyPoint> &ref_matched   = ref_cam_data.ref_matched_per_neighbor[ni];
				const std::vector<cv::KeyPoint> &neigh_matched = ref_cam_data.neigh_matched_per_neighbor[ni];

				for (int i = 0; i < static_cast<int>(ref_matched.size()); ++i)
				{
					cv::Point2f ref_pt   = ref_matched[i].pt;
					cv::Point2f neigh_pt = neigh_matched[i].pt;
					cv::circle(neighbor_image, neigh_pt, 1, cv::Scalar(0, 0, 255), -1, 8, 0);

					fribr::RTResult r;
					r.tri = 0;

					Eigen::Vector3f far_plane = unproject(ref_pt.x, ref_resolution.y() - ref_pt.y - 1, 1000.0f,
						ref_inv_intrinsics, ref_cam_to_world);

					r = window_ray_tracer.ray_cast(ref_orig, far_plane - ref_orig);
					if (!r.tri)
						continue;

					Eigen::Vector3f a(r.tri->m_vertices[0][0], r.tri->m_vertices[0][1], r.tri->m_vertices[0][2]);
					Eigen::Vector3f b(r.tri->m_vertices[1][0], r.tri->m_vertices[1][1], r.tri->m_vertices[1][2]);
					Eigen::Vector3f c(r.tri->m_vertices[2][0], r.tri->m_vertices[2][1], r.tri->m_vertices[2][2]);

					Eigen::Vector3f n_window = (b - a).cross(c - a).normalized();
					Eigen::Vector4f p_window = Eigen::Vector4f(r.p[0], r.p[1], r.p[2], 1.0f);

					Eigen::Vector3f ray_dir = (far_plane - ref_orig).normalized();
					Eigen::Vector3f local_up = up - up.dot(n_window) * n_window;

					float loss = 0.0f;
					Eigen::Vector3f position_ellipsoid = find_reflection_position_ellipsoid(
						ellipsoid_radii, local_up, window_centroids[window_idx], n_window, ray_dir,
						neigh_orig, ref_orig, window_centroids[window_idx], scene_diagonal * 0.5f, loss);

					Eigen::Vector3f neigh_position_image;
					if (project_if_visible(position_ellipsoid, world_to_clip_list[neighbor_index], neigh_cam, neigh_position_image))
					{
						neigh_position_image.y() = neigh_resolution.y() - 1.0f - neigh_position_image.y();
						cv::Point2f opt_pt(neigh_position_image.x(), neigh_position_image.y());
						if ((neigh_position_image.head<2>() - Eigen::Vector2f(neigh_pt.x, neigh_pt.y)).norm() <= INLIER_THRESHOLD)
						{
							cv::line(neighbor_image, opt_pt, neigh_pt, cv::Scalar(0, 255, 0));
							cv::circle(neighbor_image, opt_pt, 2, cv::Scalar(255, 0, 0), -1, 8, 0);
						}
					}
				}
				cv::imwrite(ref_cam_output_directory + "/optimized_for_" + neigh_cam.get_image_name() + ".jpg", neighbor_image);
			}
		}
		#endif
	}

	// Save the file to reflections/parameters_features.txt
	if (!directory_exists(folder_path + "/semantic_reflections/reflections/"))
		make_directory(folder_path + "/semantic_reflections/reflections/");

	const std::string out_path = folder_path + "/semantic_reflections/reflections/parameters_features.txt";
	std::ofstream out_file(out_path);
	if(out_file.is_open())
	{
		for(const auto & res : result_params)
			out_file << res.id << " " << res.radx << " " << res.rady << "\n";
		out_file.close();
	}
	else
		std::cerr << "Error saving to output file " << out_path << "." << std::endl;

	glfwTerminate();
    return 0;
}
