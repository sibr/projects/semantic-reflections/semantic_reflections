/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#define NOMINMAX

#include "projects/fribr_framework/renderer/tools/file_tools.h"
#include "projects/fribr_framework/renderer/tools/image_tools.h"
#include "projects/fribr_framework/renderer/tools/string_tools.h"

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Sparse>

#include <opencv2/core.hpp>
#include <opencv2/ximgproc.hpp>

#include "GCoptimization.h"

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>

#include "dirent.h"
#include <sys/types.h>
#include <core\system\Utils.hpp>
#include <core\system\CommandLineArgs.hpp>

using namespace Eigen;
using namespace fribr;

#define DEBUG_VISUALIZATION 0

namespace
{

cv::Mat4b convertRGB32FtoRGBA(const cv::Mat3f &imgF)
{
	cv::Mat4b out(imgF.rows, 3 * imgF.cols);
	for (int y = 0; y < imgF.rows; ++y)
	for (int x = 0; x < imgF.cols; ++x)
	for (int k = 0; k < 3; k++) 
	{
		unsigned char const * p = reinterpret_cast<unsigned char const*>(&imgF(y, x)[k]);
		for (std::size_t i = 0; i != sizeof(float); ++i)
		{
			int ii = i == 3 ? 3 : 2 - i;
			out(y, k * imgF.cols + x)[i] = p[ii];
		}
	}
	return out;
}

cv::Mat3f convertRGBAtoRGB32F(const cv::Mat4b &imgRGBA)
{
	cv::Mat3f out(imgRGBA.rows, imgRGBA.cols / 3);
	for (int y = 0; y < out.rows; ++y) {
		for (int x = 0; x < out.cols; ++x) {
			for (int k = 0; k < 3; k++) {
				unsigned char* p = reinterpret_cast<unsigned char*>(&out(y, x)[k]);
				for (std::size_t i = 0; i != sizeof(float); ++i)
				{
					int ii = i == 3 ? 3 : 2 - i;
					p[i] = imgRGBA(y, k * out.cols + x)[ii];
				}
			}
		}
	}
	return out;
}


cv::Vec3f bilinear_fetch(float x, float y, const cv::Mat3b &image)
{
	cv::Vec3f       color  (0.0f, 0.0f, 0.0f);
	Eigen::Vector2i floored(x, y);
	Eigen::Vector2f frac   (x - floored.x(), y - floored.y());

	for (int dy = 0; dy < 2; ++dy)
	for (int dx = 0; dx < 2; ++dx)
	{
		Eigen::Vector2f coord = floored.cast<float>() + Eigen::Vector2f(dx, dy);
		coord.x() = std::min(coord.x(), image.cols - 1.0f);
		coord.y() = std::min(coord.y(), image.rows - 1.0f);

		float weight = (1.0f - std::abs(dy - frac.y())) * (1.0f - std::abs(dx - frac.x()));
		color += weight * image(coord.y(), coord.x());
	}

	return color;
}

cv::Vec3f bilinear_fetch(float x, float y, const cv::Mat3f &image)
{
	cv::Vec3f       color(0.0f, 0.0f, 0.0f);
	Eigen::Vector2i floored(x, y);
	Eigen::Vector2f frac(x - floored.x(), y - floored.y());

	for (int dy = 0; dy < 2; ++dy)
	for (int dx = 0; dx < 2; ++dx)
	{
		Eigen::Vector2f coord = floored.cast<float>() + Eigen::Vector2f(dx, dy);
		coord.x() = std::min(coord.x(), image.cols - 1.0f);
		coord.y() = std::min(coord.y(), image.rows - 1.0f);

		float weight = (1.0f - std::abs(dy - frac.y())) * (1.0f - std::abs(dx - frac.x()));
		color += weight * image(coord.y(), coord.x());
	}

	return color;
}

float bilinear_fetch(float x, float y, const cv::Mat1b &image)
{
	float value = 0.0f;
	Eigen::Vector2i floored(x, y);
	Eigen::Vector2f frac   (x - floored.x(), y - floored.y());

	for (int dy = 0; dy < 2; ++dy)
	for (int dx = 0; dx < 2; ++dx)
	{
		Eigen::Vector2f coord = floored.cast<float>() + Eigen::Vector2f(dx, dy);
		coord.x() = std::min(coord.x(), image.cols - 1.0f);
		coord.y() = std::min(coord.y(), image.rows - 1.0f);

		float weight = (1.0f - std::abs(dy - frac.y())) * (1.0f - std::abs(dx - frac.x()));
		value += weight * image(coord.y(), coord.x());
	}

	return value;
}

void printSolverStatus(Eigen::ComputationInfo infos)
{
	if (infos == Eigen::Success)
	{
		std::cout << "Solved successfully!" << std::endl;
		return;
	}

	std::string errorReason;
	switch (infos) {
	case Eigen::NumericalIssue:
		errorReason = "NumericalIssue";
		break;
	case Eigen::NoConvergence:
		errorReason = "NoConvergence";
		break;
	case Eigen::InvalidInput:
		errorReason = "InvalidInput";
		break;
	default:
		errorReason = "Unknown";
	}
	std::cout << "Solving FAILED: " << errorReason << std::endl;
}

cv::Mat3b poisson_smooth_image(const cv::Mat1b& mask,
                               const cv::Mat3f& data,  const cv::Mat1f& data_w,
                               const cv::Mat3f& gradx, const cv::Mat1f& gradx_w,
                               const cv::Mat3f& grady, const cv::Mat1f& grady_w)
{
	std::vector<Eigen::Vector2i> var_to_pixel_remap;
	cv::Mat1i                    pixel_to_var_remap = cv::Mat1i::zeros(mask.size());
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		pixel_to_var_remap(y, x) = -1;
		if (mask(y, x) == 0)
			continue;

		pixel_to_var_remap(y, x) = var_to_pixel_remap.size();
		var_to_pixel_remap.emplace_back(Eigen::Vector2i(x, y));
	}

	// Compute the sparse matrix using triplets.
	std::vector<Eigen::Triplet<double>> triplets;
	std::vector<double> bs;
	int current_row = 0;

	// Collect unary costs
	for (int i = 0; i < static_cast<int>(var_to_pixel_remap.size()); ++i)
	{
		int x = var_to_pixel_remap[i].x();
		int y = var_to_pixel_remap[i].y();

		const float w = data_w(y, x);
		const float r = data(y, x)[2];
		const float g = data(y, x)[1];
		const float b = data(y, x)[0];

		if (w < 0.0f)
			continue;

		const int pixel_id = i;
		const int base_row = current_row;
		current_row += 3;

		triplets.emplace_back(base_row + 0, 3 * pixel_id + 0, w);
		bs.push_back(w * r);

		triplets.emplace_back(base_row + 1, 3 * pixel_id + 1, w);
		bs.push_back(w * g);

		triplets.emplace_back(base_row + 2, 3 * pixel_id + 2, w);
		bs.push_back(w * b);
	}

	// Collect pairwise costs
	for (int i = 0; i < static_cast<int>(var_to_pixel_remap.size()); ++i)
	{
		int x = var_to_pixel_remap[i].x();
		int y = var_to_pixel_remap[i].y();

		const int this_id   = i;
		const int right_id  = x >= mask.cols - 1 ? -1 : pixel_to_var_remap(y, x + 1);
		const int bottom_id = y >= mask.rows - 1 ? -1 : pixel_to_var_remap(y + 1, x);

		float xw = gradx_w(y, x);
		if (right_id >= 0 && xw > 0.0)
		{
			const int base_row = current_row;
			current_row += 3;

			triplets.emplace_back(base_row + 0, 3 * this_id + 0, -xw);
			triplets.emplace_back(base_row + 0, 3 * right_id + 0, xw);
			bs.push_back(gradx(y, x)[2] * xw);

			triplets.emplace_back(base_row + 1, 3 * this_id + 1, -xw);
			triplets.emplace_back(base_row + 1, 3 * right_id + 1, xw);
			bs.push_back(gradx(y, x)[1] * xw);

			triplets.emplace_back(base_row + 2, 3 * this_id + 2, -xw);
			triplets.emplace_back(base_row + 2, 3 * right_id + 2, xw);
			bs.push_back(gradx(y, x)[0] * xw);
		}

		float yw = grady_w(y, x);
		if (bottom_id >= 0 && yw > 0.0)
		{
			const int base_row = current_row;
			current_row += 3;

			triplets.emplace_back(base_row + 0, 3 * this_id + 0, -yw);
			triplets.emplace_back(base_row + 0, 3 * bottom_id + 0, yw);
			bs.push_back(grady(y, x)[2] * yw);

			triplets.emplace_back(base_row + 1, 3 * this_id + 1, -yw);
			triplets.emplace_back(base_row + 1, 3 * bottom_id + 1, yw);
			bs.push_back(grady(y, x)[1] * yw);

			triplets.emplace_back(base_row + 2, 3 * this_id + 2, -yw);
			triplets.emplace_back(base_row + 2, 3 * bottom_id + 2, yw);
			bs.push_back(grady(y, x)[0] * yw);
		}
	}

	Eigen::SparseMatrix<double > A(current_row, 3 * var_to_pixel_remap.size());
	A.setFromTriplets(triplets.begin(), triplets.end());
	Eigen::MatrixXd b(current_row, 1);
	for (int i = 0; i < current_row; ++i) {
		b(i, 0) = bs[i];
	}

	const Eigen::SparseMatrix<double> At = A.transpose();
	const Eigen::SparseMatrix<double> AtA = At * A;
	const Eigen::MatrixXd Atb = At * b;
	Eigen::MatrixXd solution(3 * var_to_pixel_remap.size(), 1);

	#define USE_DIRECT_SOLVER 0
	#if USE_DIRECT_SOLVER
	const Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver(AtA);
	solution = solver.solve(Atb);
	#else
	Eigen::ConjugateGradient<Eigen::SparseMatrix<double>, Eigen::Lower | Eigen::Upper> solver;
	solver.setMaxIterations(500);
	solver.setTolerance(1e-5);
	solver.compute(AtA);
	solution = solver.solve(Atb);
	#endif
	//std::cout << "SOLVER ERROR: " << solver.error() << " ITERATIONS: " << solver.iterations() << std::endl;

	cv::Mat3b output = cv::Mat3b::zeros(mask.size());
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		if (mask(y, x) == 0)
			continue;
		int var_id = pixel_to_var_remap(y, x);

		output(y, x)[0] = std::max(0.0, std::min(255.0, solution(3 * var_id + 2)));
		output(y, x)[1] = std::max(0.0, std::min(255.0, solution(3 * var_id + 1)));
		output(y, x)[2] = std::max(0.0, std::min(255.0, solution(3 * var_id + 0)));
	}

	return output;
}

cv::Mat3b min_stitch_separation(const cv::Mat3b &input, const cv::Mat1b& mask, const std::vector<const cv::Mat3b *> &neighbor_views, const std::vector<cv::Mat3f> &remaps)
{
	std::vector<Eigen::Vector2i> var_to_pixel_remap;
	cv::Mat1i                    pixel_to_var_remap = cv::Mat1i::zeros(input.size());
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		pixel_to_var_remap(y, x) = -1;
		if (mask(y, x) == 0)
			continue;

		pixel_to_var_remap(y, x) = var_to_pixel_remap.size();
		var_to_pixel_remap.emplace_back(Eigen::Vector2i(x, y));
	}

	std::vector<cv::Mat4b> candidates;
	cv::Mat4b reference = cv::Mat4b::zeros(input.size());
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		if (mask(y, x) == 0)
			continue;

		cv::Vec3b c = input(y, x);
		reference(y, x) = cv::Vec4b(c[0], c[1], c[2], 255);
	}
	candidates.emplace_back(reference);

	for (int i = 0; i < static_cast<int>(neighbor_views.size()); ++i)
	{
		cv::Mat4b warped = cv::Mat4b::zeros(input.size());
		for (int y = 0; y < mask.rows; ++y)
		for (int x = 0; x < mask.cols; ++x)
		{
			if (mask(y, x) == 0)
				continue;

			cv::Vec3f warped_coords = remaps[i](y, x);
			if (warped_coords[2] > 0.01f &&
				warped_coords[0] >= 0.00f && warped_coords[0] < neighbor_views[i]->cols - 1 &&
				warped_coords[1] >= 0.00f && warped_coords[1] < neighbor_views[i]->rows - 1)
			{
				cv::Vec3f c = bilinear_fetch(warped_coords[0], warped_coords[1], *neighbor_views[i]);
				warped(y, x) = cv::Vec4b(c[0], c[1], c[2], 255);
			}
		}
		candidates.emplace_back(warped);
	}

	std::vector<cv::Mat1b> softmasks;
	cv::Mat1b softmask_reference;
	cv::distanceTransform(mask, softmask_reference, cv::DIST_L1, 3, CV_8U);
	softmasks.emplace_back(softmask_reference);

	for (int i = 0; i < static_cast<int>(neighbor_views.size()); ++i)
	{
		cv::Mat1b hardmask = cv::Mat1b::zeros(input.size());
		for (int y = 0; y < mask.rows; ++y)
		for (int x = 0; x < mask.cols; ++x)
		{
			if (mask(y, x) == 0)
				continue;

			cv::Vec3f warped_coords = remaps[i](y, x);
			if (warped_coords[2] > 0.5f &&
				warped_coords[0] >= 0.00f && warped_coords[0] < neighbor_views[i]->cols - 1 &&
				warped_coords[1] >= 0.00f && warped_coords[1] < neighbor_views[i]->rows - 1)
				hardmask(y, x) = 255;
		}

		cv::Mat1b softmask;
		cv::distanceTransform(hardmask, softmask, cv::DIST_L1, 3, CV_8U);
		softmasks.emplace_back(softmask);
	}

	std::unique_ptr<GCoptimizationGeneralGraph> gc =
		std::make_unique<GCoptimizationGeneralGraph>(var_to_pixel_remap.size(), candidates.size());

	for (int y = 0; y < reference.rows; ++y)
	for (int x = 0; x < reference.cols; ++x)
	for (int l = 0; l < candidates.size(); ++l)
	{
		if (mask(y, x) == 0)
			continue;

		cv::Vec4b c       = candidates[l](y, x);
		float     nearest = 1000.0f;
		for (int ll = 0; ll < candidates.size(); ++ll)
		{
			if (ll == l)
				continue;

			cv::Vec4b cc = candidates[ll](y, x);
			if (cc[3] > 0)
			{
				float delta = 0.0f;
				for (int ci = 0; ci < 3; ++ci)
					delta += std::abs(c[ci] - cc[ci]);
				nearest = std::min(nearest, delta);
			}
		}

		float luminance = (c[0] + c[1] + c[2]) / 3.0f;
		int   cost      = 8192;
		if (c[3] > 0)
			cost = luminance + nearest * 1 + std::max(0, 32 - static_cast<int>(softmasks[l](y, x))) * 2;

		gc->setDataCost(pixel_to_var_remap(y, x), l, cost);
	}

	for (int y = 0; y < reference.rows; y++)
	for (int x = 1; x < reference.cols; x++)
	{
		if (mask(y, x) == 0 || mask(y, x - 1) == 0)
			continue;

		gc->setNeighbors(pixel_to_var_remap(y, x), pixel_to_var_remap(y, x - 1));
	}

	for (int y = 1; y < reference.rows; y++)
	for (int x = 0; x < reference.cols; x++)
	{
		if (mask(y, x) == 0 || mask(y - 1, x) == 0)
			continue;

		gc->setNeighbors(pixel_to_var_remap(y, x), pixel_to_var_remap(y - 1, x));
	}

	struct SeamHidingSmoothFunctor : public GCoptimization::SmoothCostFunctor {
		GCoptimization::EnergyTermType compute(GCoptimization::SiteID s1, GCoptimization::SiteID s2,
			                                   GCoptimization::LabelID l1, GCoptimization::LabelID l2)
		{
			int x1 = (*vars_to_pixels)[s1].x();
			int y1 = (*vars_to_pixels)[s1].y();
			int x2 = (*vars_to_pixels)[s2].x();
			int y2 = (*vars_to_pixels)[s2].y();

			cv::Vec4f s1l1 = (*vec)[l1](y1, x1);
			cv::Vec4f s2l1 = (*vec)[l1](y2, x2);

			cv::Vec4f s1l2 = (*vec)[l2](y1, x1);
			cv::Vec4f s2l2 = (*vec)[l2](y2, x2);

			cv::Vec4f delta_s1 = s1l1 - s1l2;
			cv::Vec4f delta_s2 = s2l1 - s2l2;

			int cost = std::abs(delta_s1[0]) + std::abs(delta_s1[1]) + std::abs(delta_s1[2]) +
			           std::abs(delta_s2[0]) + std::abs(delta_s2[1]) + std::abs(delta_s2[2]);

			return cost;
		}
		cv::Mat4b                    *ref;
		std::vector<cv::Mat4b>       *vec;
		std::vector<Eigen::Vector2i> *vars_to_pixels;
	};

	SeamHidingSmoothFunctor functor;
	functor.vec            = &candidates;
	functor.ref            = &reference;
	functor.vars_to_pixels = &var_to_pixel_remap;

	gc->setSmoothCostFunctor(&functor);
	gc->setVerbosity(0);
	gc->expansion(2);

	#define USE_FEATHERING 0
	#define USE_POISSON    1
	#if USE_FEATHERING
	std::vector<cv::Mat1f> feather_masks;
	for (int l = 0; l < static_cast<int>(candidates.size()); ++l)
		feather_masks.emplace_back(cv::Mat1f::zeros(reference.size()));

	for (int y = 0; y < reference.rows; ++y)
	for (int x = 0; x < reference.cols; ++x)
	{
		if (mask(y, x) == 0)
			continue;

		int l = gc->whatLabel(pixel_to_var_remap(y, x));
		feather_masks[l](y, x) = 1.0f;
	}

	cv::Mat3f total_image  = cv::Mat3f::zeros(reference.size());
	cv::Mat1f total_weight = cv::Mat1f::zeros(reference.size());
	for (int l = 0; l < static_cast<int>(candidates.size()); ++l)
	{
		cv::blur(feather_masks[l], feather_masks[l], cv::Size(33, 33));
		for (int y = 0; y < reference.rows; ++y)
		for (int x = 0; x < reference.cols; ++x)
		{
			if (candidates[l](y, x)[3] == 0)
				feather_masks[l](y, x) = 0.0f;
		}

		for (int y = 0; y < reference.rows; ++y)
		for (int x = 0; x < reference.cols; ++x)
		{
			for (int c = 0; c < 3; ++c)
				total_image(y, x)[c] += feather_masks[l](y, x) * candidates[l](y, x)[c];
			total_weight(y, x) += feather_masks[l](y, x);
		}
	}

	cv::Mat3b output = cv::Mat3b::zeros(reference.size());
	for (int y = 0; y < reference.rows; ++y)
	for (int x = 0; x < reference.cols; ++x)
	{
		if (mask(y, x) == 0)
			continue;

		for (int c = 0; c < 3; ++c)
			output(y, x)[c] = total_image(y, x)[c] / std::max(0.0001f, total_weight(y, x));
	}
	#elif USE_POISSON
	cv::Mat3f data    = cv::Mat3f::zeros(reference.size());
	cv::Mat1f data_w  = cv::Mat1f::zeros(reference.size());
	cv::Mat3f gradx   = cv::Mat3f::zeros(reference.size());
	cv::Mat1f gradx_w = cv::Mat1f::zeros(reference.size());
	cv::Mat3f grady   = cv::Mat3f::zeros(reference.size());
	cv::Mat1f grady_w = cv::Mat1f::zeros(reference.size());

	for (int y = 0; y < reference.rows; ++y)
	for (int x = 0; x < reference.cols; ++x)
	{
		if (mask(y, x) == 0)
			continue;

		int l = gc->whatLabel(pixel_to_var_remap(y, x));
		for (int c = 0; c < 3; ++c)
			data(y, x)[c] = candidates[l](y, x)[c];

		if (x < reference.cols - 1 && mask(y, x + 1) != 0)
		{
			int ll = gc->whatLabel(pixel_to_var_remap(y, x + 1));
			cv::Vec3f gx (0.0f, 0.0f, 0.0f);
			cv::Vec3f ggx(0.0f, 0.0f, 0.0f);
			for (int c = 0; c < 3; ++c)
			{
				gx[c] += candidates[l](y, x + 1)[c]  - candidates[l](y, x)[c];
				if (candidates[ll](y, x + 1)[3] > 0)
					ggx[c] += candidates[ll](y, x + 1)[c] - candidates[ll](y, x)[c];
				gradx(y, x)[c] = std::min(gx[c], ggx[c]);
			}
		}

		if (y < reference.rows - 1 && mask(y + 1, x) != 0)
		{
			int ll = gc->whatLabel(pixel_to_var_remap(y + 1, x));

			cv::Vec3f gy (0.0f, 0.0f, 0.0f);
			cv::Vec3f ggy(0.0f, 0.0f, 0.0f);
			for (int c = 0; c < 3; ++c)
			{
				gy[c]  += candidates[l](y + 1, x)[c]  - candidates[l](y, x)[c];
				if (candidates[ll](y + 1, x)[3] > 0)
					ggy[c] += candidates[ll](y + 1, x)[c] - candidates[ll](y, x)[c];
				grady(y, x)[c] = std::min(gy[c], ggy[c]);
			}
		}

		data_w(y, x)  = 0.01f;
		gradx_w(y, x) = 1.0f;
		grady_w(y, x) = 1.0f;
	}

	cv::Mat3b output = poisson_smooth_image(mask, data, data_w, gradx, gradx_w, grady, grady_w);
	#else
	cv::Mat3b output = cv::Mat3b::zeros(reference.size());
	for (int y = 0; y < reference.rows; ++y)
	for (int x = 0; x < reference.cols; ++x)
	{
		if (mask(y, x) == 0)
			continue;

		int l = gc->whatLabel(pixel_to_var_remap(y, x));
		for (int c = 0; c < 3; ++c)
			output(y, x)[c] = candidates[l](y, x)[c];
	}
	#endif

	return output;
}

cv::Mat3f smooth_flow(const cv::Mat3f &input_flow, const cv::Mat1b &input_mask, int radius)
{
	cv::Mat1b mask = cv::Mat1b::zeros(input_flow.size());
	for (int y = 0; y < input_flow.rows; ++y)
	for (int x = 0; x < input_flow.cols; ++x)
		mask(y, x) = input_flow(y, x)[2] > 0.01f ? 255 : 0;
	cv::erode(mask, mask, cv::Mat(), cv::Point(-1, -1), 8);

	cv::Mat1b big_mask = mask.clone();
	for (int i = 0; i < radius; ++i)
	{
		cv::dilate(big_mask, big_mask, cv::Mat(), cv::Point(-1, -1), 1);
		for (int y = 0; y < mask.rows; ++y)
		for (int x = 0; x < mask.cols; ++x)
			big_mask(y, x) = std::min(big_mask(y, x), input_mask(y, x));
	}

	cv::Mat1b var_mask = cv::Mat1b::zeros(mask.size());
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		if (mask(y, x) != 0 || big_mask(y, x) == 0)
			continue;
		var_mask(y, x) = 255;
	}

	cv::Mat1i label_image;
	int num_components = cv::connectedComponents(var_mask, label_image, 4);
	std::vector<bool> is_connected(num_components, false);
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		if (mask(y, x) == 0)
			continue;

		if (x > 0)
			is_connected[label_image(y, x - 1)] = true;
		if (x < mask.rows - 1)
			is_connected[label_image(y, x + 1)] = true;

		if (y > 0)
			is_connected[label_image(y - 1, x)] = true;
		if (y < mask.rows - 1)
			is_connected[label_image(y + 1, x)] = true;
	}

	std::vector<Eigen::Vector2i> var_to_pixel_remap;
	cv::Mat1i                    pixel_to_var_remap = cv::Mat1i::zeros(mask.size());
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		pixel_to_var_remap(y, x) = -1;
		if (mask(y, x) != 0 || big_mask(y, x) == 0 || !is_connected[label_image(y, x)])
			continue;

		pixel_to_var_remap(y, x) = var_to_pixel_remap.size();
		var_to_pixel_remap.emplace_back(Eigen::Vector2i(x, y));
	}

	// Compute the sparse matrix using triplets.
	std::vector<Eigen::Triplet<double>> triplets;
	std::vector<double> bs;
	int current_row = 0;

	// Collect unary costs
	for (int i = 0; i < static_cast<int>(var_to_pixel_remap.size()); ++i)
	{
		int x = var_to_pixel_remap[i].x();
		int y = var_to_pixel_remap[i].y();
		
		const int this_id   = i;
		const int right_id  = x >= mask.cols - 1 ? -2 : pixel_to_var_remap(y, x + 1);
		const int bottom_id = y >= mask.rows - 1 ? -2 : pixel_to_var_remap(y + 1, x);
		const int top_id    = y <= 0             ? -2 : pixel_to_var_remap(y - 1, x);
		const int left_id   = x <= 0             ? -2 : pixel_to_var_remap(y, x - 1);

		if (right_id == -1 && mask(y, x + 1) != 0)
		{
			const int base_row = current_row;
			current_row += 2;

			for (int j = 0; j < 2; ++j)
			{
				triplets.emplace_back(base_row + j, 2 * this_id + j, 1.0f);
				bs.push_back(input_flow(y, x + 1)[j]);
			}
		}

		if (bottom_id == -1 && mask(y + 1, x) != 0)
		{
			const int base_row = current_row;
			current_row += 2;

			for (int j = 0; j < 2; ++j)
			{
				triplets.emplace_back(base_row + j, 2 * this_id + j, 1.0f);
				bs.push_back(input_flow(y + 1, x)[j]);
			}
		}

		if (left_id == -1 && mask(y, x - 1) != 0)
		{
			const int base_row = current_row;
			current_row += 2;

			for (int j = 0; j < 2; ++j)
			{
				triplets.emplace_back(base_row + j, 2 * this_id + j, 1.0f);
				bs.push_back(input_flow(y, x - 1)[j]);
			}
		}

		if (top_id == -1 && mask(y - 1, x) != 0)
		{
			const int base_row = current_row;
			current_row += 2;

			for (int j = 0; j < 2; ++j)
			{
				triplets.emplace_back(base_row + j, 2 * this_id + j, 1.0f);
				bs.push_back(input_flow(y - 1, x)[j]);
			}
		}
	}

	//std::cout << "Collecting pairwise" << std::endl;
	for (int i = 0; i < static_cast<int>(var_to_pixel_remap.size()); ++i)
	{
		int x = var_to_pixel_remap[i].x();
		int y = var_to_pixel_remap[i].y();

		const int this_id   = i;
		const int right_id  = x >= mask.cols - 1 ? -1 : pixel_to_var_remap(y, x + 1);
		const int bottom_id = y >= mask.rows - 1 ? -1 : pixel_to_var_remap(y + 1, x);

		if (right_id >= 0)
		{
			const int base_row = current_row;
			current_row += 2;

			for (int j = 0; j < 2; ++j)
			{
				triplets.emplace_back(base_row + j, 2 * this_id  + j,  1.0f);
				triplets.emplace_back(base_row + j, 2 * right_id + j, -1.0f);
				bs.push_back(0.0f);
			}
		}

		if (bottom_id >= 0)
		{
			const int base_row = current_row;
			current_row += 2;

			for (int j = 0; j < 2; ++j)
			{
				triplets.emplace_back(base_row + j, 2 * this_id   + j,  1.0f);
				triplets.emplace_back(base_row + j, 2 * bottom_id + j, -1.0f);
				bs.push_back(0.0f);
			}
		}
	}

	//std::cout << "Sparse matrix" << std::endl;
	Eigen::SparseMatrix<double > A(current_row, 2 * var_to_pixel_remap.size());
	A.setFromTriplets(triplets.begin(), triplets.end());
	Eigen::MatrixXd b(current_row, 1);
	for (int i = 0; i < current_row; ++i) {
		b(i, 0) = bs[i];
	}

	//std::cout << "Building system." << std::endl;
	const Eigen::SparseMatrix<double> At = A.transpose();
	const Eigen::SparseMatrix<double> AtA = At * A;
	const Eigen::MatrixXd Atb = At * b;
	Eigen::MatrixXd solution(2 * var_to_pixel_remap.size(), 1);

	// Start with a direct solver.
	//const Eigen::CholmodSupernodalLLT<Eigen::SparseMatrix<double>> solver(AtA);
	const Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver(AtA);
	solution = solver.solve(Atb);
	//printSolverStatus(solver.info());

	//Eigen::ConjugateGradient<Eigen::SparseMatrix<double>, Eigen::Lower | Eigen::Upper> solver;
	//solver.setMaxIterations(500);
	//solver.setTolerance(1e-5);
	//solver.compute(AtA);
	//solution = solver.solve(Atb);
	//std::cout << "SOLVER ERROR: " << solver.error() << " ITERATIONS: " << solver.iterations() << std::endl;

	cv::Mat3f output = input_flow.clone();
	for (int y = 0; y < input_flow.rows; ++y)
	for (int x = 0; x < input_flow.cols; ++x)
	{
		if (mask(y, x) != 0 || big_mask(y, x) == 0 || !is_connected[label_image(y, x)])
			continue;

		int var_id = pixel_to_var_remap(y, x);

		output(y, x)[0] = solution(2 * var_id + 0);
		output(y, x)[1] = solution(2 * var_id + 1);
		output(y, x)[2] = 0.5f;
	}

	return output;
}

cv::Mat3b poisson_extend_image(const cv::Mat3b &image, const cv::Mat1b &mask, int radius)
{
	int median_radius = 4;
	cv::Mat1b border_mask;
	cv::erode(mask, border_mask, cv::Mat1b(), cv::Point(-1, -1), median_radius);

	cv::Mat3b median_image = image.clone();
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		if (mask(y, x) == 0 || border_mask(y, x) != 0)
			continue;

		std::vector<int> reds, greens, blues;
		for (int dx = -median_radius; dx <= median_radius; ++dx)
		for (int dy = -median_radius; dy <= median_radius; ++dy)
		{
			int xx = x + dx;
			int yy = y + dy;
			if (xx < 0 || xx >= mask.cols || yy < 0 || yy >= mask.rows)
				continue;

			if (mask(yy, xx) == 0)
				continue;

			reds.emplace_back(image(yy, xx)[2]);
			greens.emplace_back(image(yy, xx)[1]);
			blues.emplace_back(image(yy, xx)[0]);
		}

		std::sort(reds.begin(), reds.end());
		std::sort(greens.begin(), greens.end());
		std::sort(blues.begin(), blues.end());

		if (!reds.empty())
		{
			median_image(y, x)[2] = reds[reds.size() / 2];
			median_image(y, x)[1] = greens[reds.size() / 2];
			median_image(y, x)[0] = blues[reds.size() / 2];
		}
	}

	cv::Mat1b big_mask;
	cv::dilate(mask, big_mask, cv::Mat(), cv::Point(-1, -1), radius);

	std::vector<Eigen::Vector2i> var_to_pixel_remap;
	cv::Mat1i                    pixel_to_var_remap = cv::Mat1i::zeros(mask.size());
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		pixel_to_var_remap(y, x) = -1;
		if (mask(y, x) != 0 || big_mask(y, x) == 0)
			continue;

		pixel_to_var_remap(y, x) = var_to_pixel_remap.size();
		var_to_pixel_remap.emplace_back(Eigen::Vector2i(x, y));
	}

	// Compute the sparse matrix using triplets.
	std::vector<Eigen::Triplet<double>> triplets;
	std::vector<double> bs;
	int current_row = 0;

	// Collect unary costs
	for (int i = 0; i < static_cast<int>(var_to_pixel_remap.size()); ++i)
	{
		int x = var_to_pixel_remap[i].x();
		int y = var_to_pixel_remap[i].y();
		
		const int this_id   = i;
		const int right_id  = x >= mask.cols - 1 ? -2 : pixel_to_var_remap(y, x + 1);
		const int bottom_id = y >= mask.rows - 1 ? -2 : pixel_to_var_remap(y + 1, x);
		const int left_id   = x <= 0             ? -2 : pixel_to_var_remap(y, x - 1);
		const int top_id    = y <= 0             ? -2 : pixel_to_var_remap(y - 1, x);

		if (right_id == -1 && mask(y, x + 1) != 0)
		{
			const int base_row = current_row;
			current_row += 3;

			for (int j = 0; j < 3; ++j)
			{
				triplets.emplace_back(base_row + j, 3 * this_id + j, 1.0f);
				bs.push_back(median_image(y, x + 1)[j]);
			}
		}

		if (bottom_id == -1 && mask(y + 1, x) != 0)
		{
			const int base_row = current_row;
			current_row += 3;

			for (int j = 0; j < 3; ++j)
			{
				triplets.emplace_back(base_row + j, 3 * this_id + j, 1.0f);
				bs.push_back(median_image(y + 1, x)[j]);
			}
		}

		if (left_id == -1 && mask(y, x - 1) != 0)
		{
			const int base_row = current_row;
			current_row += 3;

			for (int j = 0; j < 3; ++j)
			{
				triplets.emplace_back(base_row + j, 3 * this_id + j, 1.0f);
				bs.push_back(median_image(y, x - 1)[j]);
			}
		}

		if (top_id == -1 && mask(y - 1, x) != 0)
		{
			const int base_row = current_row;
			current_row += 3;

			for (int j = 0; j < 3; ++j)
			{
				triplets.emplace_back(base_row + j, 3 * this_id + j, 1.0f);
				bs.push_back(median_image(y - 1, x)[j]);
			}
		}
	}

	//std::cout << "Collecting pairwise" << std::endl;
	for (int i = 0; i < static_cast<int>(var_to_pixel_remap.size()); ++i)
	{
		int x = var_to_pixel_remap[i].x();
		int y = var_to_pixel_remap[i].y();

		const int this_id   = i;
		const int right_id  = x >= mask.cols - 1 ? -1 : pixel_to_var_remap(y, x + 1);
		const int bottom_id = y >= mask.rows - 1 ? -1 : pixel_to_var_remap(y + 1, x);

		if (right_id >= 0)
		{
			const int base_row = current_row;
			current_row += 3;

			for (int j = 0; j < 3; ++j)
			{
				triplets.emplace_back(base_row + j, 3 * this_id  + j,  1.0f);
				triplets.emplace_back(base_row + j, 3 * right_id + j, -1.0f);
				bs.push_back(0.0f);
			}
		}

		if (bottom_id >= 0)
		{
			const int base_row = current_row;
			current_row += 3;

			for (int j = 0; j < 3; ++j)
			{
				triplets.emplace_back(base_row + j, 3 * this_id   + j,  1.0f);
				triplets.emplace_back(base_row + j, 3 * bottom_id + j, -1.0f);
				bs.push_back(0.0f);
			}
		}
	}

	Eigen::SparseMatrix<double > A(current_row, 3 * var_to_pixel_remap.size());
	A.setFromTriplets(triplets.begin(), triplets.end());
	Eigen::MatrixXd b(current_row, 1);
	for (int i = 0; i < current_row; ++i) {
		b(i, 0) = bs[i];
	}

	const Eigen::SparseMatrix<double> At = A.transpose();
	const Eigen::SparseMatrix<double> AtA = At * A;
	const Eigen::MatrixXd Atb = At * b;
	Eigen::MatrixXd solution(3 * var_to_pixel_remap.size(), 1);

	#define USE_DIRECT_SOLVER_COLOR 1
	#if USE_DIRECT_SOLVER_COLOR
	const Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver(AtA);
	solution = solver.solve(Atb);
	#else
	Eigen::ConjugateGradient<Eigen::SparseMatrix<double>, Eigen::Lower | Eigen::Upper> solver;
	solver.setMaxIterations(150);
	solver.setTolerance(1e-5);
	solver.compute(AtA);
	solution = solver.solve(Atb);
	std::cout << "SOLVER ERROR: " << solver.error() << " ITERATIONS: " << solver.iterations() << std::endl;
	#endif

	cv::Mat3b output = image.clone();
	for (int y = 0; y < mask.rows; ++y)
	for (int x = 0; x < mask.cols; ++x)
	{
		if (mask(y, x) != 0 || big_mask(y, x) == 0)
			continue;

		int var_id = pixel_to_var_remap(y, x);

		output(y, x)[0] = std::max(0.0, std::min(255.0, solution(3 * var_id + 0)));
		output(y, x)[1] = std::max(0.0, std::min(255.0, solution(3 * var_id + 1)));
		output(y, x)[2] = std::max(0.0, std::min(255.0, solution(3 * var_id + 2)));
	}

	return output;
}

}

void process(const cv::Mat3b & input_image, const cv::Mat1b & mask, const std::string & inputPath, const std::map<std::string, size_t> namesToIds, 
	const std::vector<cv::Mat3b> & images, const std::string & outputDir, const std::string & outputName)
{
  
	std::vector<std::string> dirs = sibr::listSubdirectories(inputPath, false);

	std::vector<std::string> neigh_paths;
	for (const std::string &neighbor : dirs)
	{
		if (neighbor == "." || neighbor == ".." ||
			neighbor.find("fg_warps") != std::string::npos ||
			neighbor.find("bg_warps") != std::string::npos) {

			continue;
		}
		neigh_paths.emplace_back(neighbor);
	}
	std::vector<cv::Mat3f> fg_uvs_in_neighs(neigh_paths.size());
	std::vector<const cv::Mat3b*> neighbor_views(neigh_paths.size());

	for (int i = 0; i < static_cast<int>(neigh_paths.size()); ++i)
	{
		const std::string neighbor = neigh_paths[i];
		// We now read the image one level above.
		const cv::Mat3b& neighbor_image = images[namesToIds.at(neighbor)];
		cv::Mat4b fg_uvs_in_neigh_encoded = cv::imread(inputPath + "/" + neighbor + "/foreground_uvs_in_neigh.png", cv::IMREAD_UNCHANGED);
		fg_uvs_in_neighs[i] = convertRGBAtoRGB32F(fg_uvs_in_neigh_encoded);
		fg_uvs_in_neigh_encoded.release();

		for (int y = 0; y < input_image.rows; ++y)
		for (int x = 0; x < input_image.cols; ++x)
		{
			fg_uvs_in_neighs[i](y, x)[0] *= neighbor_image.cols;
			fg_uvs_in_neighs[i](y, x)[1] *= neighbor_image.rows;
		}

		neighbor_views[i]   = &images[namesToIds.at(neighbor)];
		
	}
	std::cout << ".";
	cv::Mat3b stitched_fg = min_stitch_separation(input_image, mask, neighbor_views, fg_uvs_in_neighs);
	cv::Mat3b filled_fg = poisson_extend_image(stitched_fg, mask, 64);
	cv::imwrite(outputDir + "/" + outputName, filled_fg);

#if DEBUG_VISUALIZATION

	cv::Mat3b min_fg = input_image.clone();
	cv::Mat4f sum_fg = cv::Mat4f::zeros(input_image.size());
	for (int y = 0; y < input_image.rows; ++y)
		for (int x = 0; x < input_image.cols; ++x)
		{
			cv::Vec3b c = input_image(y, x);
			sum_fg(y, x) = cv::Vec4f(c[0], c[1], c[2], 1.0f);
		}

	const std::string fg_warps_path = inputPath + "/fg_warps";
	if (!directory_exists(fg_warps_path))
		make_directory(fg_warps_path);

	std::cout << "Creating min composite (" << inputPath << ")..." << std::endl;
	for (int i = 0; i < static_cast<int>(neighbor_views.size()); ++i)
	{
		std::string  neighbor        = neigh_paths[i];
		const cv::Mat3b   &neighbor_image  = *neighbor_views[i];
		cv::Mat3f   &fg_uvs_in_neigh = fg_uvs_in_neighs[i];
		

		cv::Mat3b fg_warp = cv::Mat3b::zeros(input_image.size());
		
		for (int y = 0; y < input_image.rows; ++y)
		for (int x = 0; x < input_image.cols; ++x)
		{
			if (mask(y, x) == 0)
				continue;
			
			cv::Vec3f fg_coords = fg_uvs_in_neigh(y, x);
			if (fg_coords[2] >  0.01f &&
				fg_coords[0] >= 0.00f && fg_coords[0] < neighbor_image.cols - 1 &&
				fg_coords[1] >= 0.00f && fg_coords[1] < neighbor_image.rows - 1)
			{
				cv::Vec3f color = bilinear_fetch(fg_coords[0], fg_coords[1], neighbor_image);
				sum_fg(y, x) += cv::Vec4f(color[0], color[1], color[2], 1.0f);
				fg_warp(y, x) = color;

				for (int c = 0; c < 3; ++c)
					min_fg(y, x)[c] = std::min(min_fg(y, x)[c], static_cast<unsigned char>(color[c]));
			}

		}


		cv::imwrite(fg_warps_path + "/" + neighbor + ".jpg", fg_warp);

	}

	cv::Mat3b avg_fg = cv::Mat3b::zeros(input_image.size());
	
	for (int y = 0; y < input_image.rows; ++y)
	for (int x = 0; x < input_image.cols; ++x)
	for (int c = 0; c < 3; ++c)
	{
		if (mask(y, x) == 0)
			continue;

		if (sum_fg(y, x)[3] > 0.0f)
			avg_fg(y, x)[c] = sum_fg(y, x)[c] / sum_fg(y, x)[3];

	}
	cv::imwrite(inputPath + "/avg_fg.png", avg_fg);
	cv::imwrite(inputPath + "/min_fg.png", min_fg);
#endif

    return;
}

int main(int argc, const char** argv) {
	sibr::CommandLineArgs::parseMainArgs(argc, argv);
	sibr::BasicDatasetArgs args;
	const std::string basePath = args.dataset_path.get() + "/semantic_reflections/layers_bidir/";
	// RAM and I/O heavy, limit number of threads
	omp_set_num_threads(24);
	
	// Get list of images.
	const auto dirs = sibr::listSubdirectories(basePath, false);
	// Reverse mapping for later.
	std::map<std::string, size_t> namesToIds;
	for (size_t iid = 0; iid < dirs.size(); ++iid) {
		namesToIds[dirs[iid]] = iid;
	}

	// Load all input images and masks.
	SIBR_LOG << "Loading input images..." << std::flush;
	std::vector<cv::Mat3b> images(dirs.size());
	std::vector<cv::Mat1b> masks(dirs.size());
#pragma omp parallel for
	for (int iid = 0; iid < int(dirs.size()); ++iid) {
		const std::string folder_path = basePath + dirs[iid];
		const std::string reference_path = folder_path + "/image.jpg";
		const std::string mask_path = folder_path + "/mask.png";
		images[iid] = cv::imread(reference_path, cv::IMREAD_UNCHANGED);
		masks[iid] = cv::imread(mask_path, cv::IMREAD_GRAYSCALE);
	}
	SIBR_LOG << "Done." << std::endl;

	// Perform separation, output to
	SIBR_LOG << "First separation pass..." << std::flush;
#pragma omp parallel for
	for (int iid = 0; iid < int(dirs.size()); ++iid) {
		const std::string folder_path = basePath + dirs[iid] + "/";
		process(images[iid], masks[iid], folder_path, namesToIds, images, folder_path, "stitched_fg.png");
	}
	SIBR_LOG << "Done." << std::endl;

	// Clear the input images.
	images.clear();
	SIBR_LOG << "Loading separated images..." << std::flush;
	images.resize(dirs.size());
#pragma omp parallel for
	for (int iid = 0; iid < int(dirs.size()); ++iid) {
		const std::string folder_path = basePath + dirs[iid] ;
		const std::string reference_path = folder_path + "/stitched_fg.png";
		images[iid] = cv::imread(reference_path, cv::IMREAD_UNCHANGED);
	}
	SIBR_LOG << "Done." << std::endl;
	// TODO: right now we load the flows twice, it might be worth storing them instead if available RAM allows it.
	
	// Perform separation, output to final directory.
	const std::string finalOutputDir = args.dataset_path.get() + "/semantic_reflections/min_foregrounds/";
	sibr::makeDirectory(finalOutputDir);
	SIBR_LOG << "Second separation pass..." << std::flush;
#pragma omp parallel for
	for (int iid = 0; iid < int(dirs.size()); ++iid) {
		const std::string folder_path = basePath + dirs[iid] + "/";
		process(images[iid], masks[iid], folder_path, namesToIds, images, finalOutputDir, dirs[iid] + ".png");
	}
	SIBR_LOG << "Done." << std::endl;
}
