# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


cmake_minimum_required(VERSION 2.8.12)
project(gco)

add_library(gco STATIC "maxflow.cpp" "graph.cpp" "GCoptimization.cpp" "LinkedBlockList.cpp")
set_target_properties(gco PROPERTIES OUTPUT_NAME "gco")
target_include_directories(gco PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "projects/semantic_reflections/preprocess")

## High level macro to install in an homogen way all our ibr targets
include(install_runtime)
ibr_install_target(${PROJECT_NAME}
    INSTALL_PDB                         ## mean install also MSVC IDE *.pdb file (DEST according to target type)
    COMPONENT   ${PROJECT_NAME}_install ## will create custom target to install only this project
)