/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>

#include <core/graphics/Window.hpp>
#include <core/view/ViewBase.hpp>
#include <core/view/MultiViewManager.hpp>

#include <core/renderer/DepthRenderer.hpp>
#include "projects/semantic_reflections/renderer/SemanticReflectionsScene.hpp"
#include "projects/semantic_reflections/renderer/SemanticReflectionsView.hpp"
#include "core/system/SimpleTimer.hpp"
#include "core/system/String.hpp"


#define PROGRAM_NAME "sibr_semantic_reflections_legacy_fitting"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;

struct SemanticReflectionsPreprocessArgs : BasicDatasetArgs, WindowAppArgs {

	Arg<int> max_input_width = { "max_width", -1, "maximum width for input data in memory" };
	Arg<int> neighcount = { "ncount", 25, "Number of neighbours to use for parameter fitting each window" };
	Arg<bool> half_depth = { "halfdepth" , "generate input depth maps at half resolution"};
};

//#define OUTPUT_VISU 1
#define ONLY_MISSING_PARAMS 1

int main(int ac, char** av) {


	CommandLineArgs::parseMainArgs(ac, av);
	SemanticReflectionsPreprocessArgs args;
	const std::string basePath = args.dataset_path;
	const int maxWidth = args.max_input_width;

	sibr::Window window(PROGRAM_NAME, sibr::Vector2i(50, 50), args);

	const bool halfResDepth = args.half_depth;

	// Setup the scene.
	auto scene = std::make_shared< SemanticReflectionsScene>(basePath, maxWidth, SemanticReflectionsScene::FLOWS_PREPROCESS, halfResDepth);

	SemanticReflectionsView::Ptr mainView(new SemanticReflectionsView(scene, uint(scene->resolution()[0]), uint(scene->resolution()[1])));


	struct BatchInfos {
		unsigned int view;
		int window;
		int params;
		std::vector<unsigned int> neighs;

		BatchInfos(unsigned int viewId, int windowId, int paramsId) {
			view = viewId;
			window = windowId;
			params = paramsId;
		}

	};

	// Load listing
	std::vector<BatchInfos> batches;
	// Load existing parameters.
	std::vector<SemanticReflectionsScene::WindowParameters> parameters;
#ifdef ONLY_MISSING_PARAMS
	const std::string existingParameters = scene->basePath() + "/semantic_reflections/reflections/parameters_features.txt";
#else
	const std::string existingParameters = scene->basePath() + "/semantic_reflections/reflections/parameters.txt";
#endif

	std::ifstream paramsFile(existingParameters);
	if (paramsFile.is_open()) {
		std::string line;
		while (std::getline(paramsFile, line)) {
			if (line.empty() || line[0] == '#') {
				continue;
			}
			const auto tokens = sibr::split(line, ' ');
			const int winId = std::stoi(tokens[0]);
			const float paramX = std::stof(tokens[1]);
			const float paramY = std::stof(tokens[2]);
			parameters.emplace_back();
			parameters.back().meshId = winId;
			parameters.back().radii = {paramX, paramY};
		}
		paramsFile.close();
	}

	
	const auto & windowMesh = scene->windows();

	// Compute per window normal and centroid.
	const int numWindows = scene->numWindows();
	std::vector<sibr::Vector3f> windowNormals(numWindows, sibr::Vector3f(0.0f, 0.0f, 0.0f));
	std::vector<sibr::Vector3f> windowCenters(numWindows, sibr::Vector3f(0.0f, 0.0f, 0.0f));
	std::vector<int> windowCounts(numWindows, 0);
	for (int vid = 0; vid < windowMesh->vertices().size(); ++vid) {
		const int winId = int(windowMesh->texCoords()[vid][0]);
		if (winId < 0 || winId >= numWindows) {
			continue;
		}
		windowCenters[winId] += windowMesh->vertices()[vid];
		windowNormals[winId] += windowMesh->normals()[vid];
		windowCounts[winId] += 1;
	}

	// For each component we want to pick one (or N in the future) cameras that are facing it the best.
	// To do this, we compute the average normals among all triangles, this assume normals are properly oriented (garanteed by shrinkwrap output).
	// We can then pick the camera that is the most 'in front' of the window, and not too far in distance.
	for (int winId = 0; winId < numWindows; ++winId) {
		if (windowCounts[winId] == 0) {
			continue;
		}
		// Windows which already have non negative parameters shouldn't be modified.
		int pid = 0;
		for(; pid < parameters.size(); ++pid) {
			const auto & param = parameters[pid];
			if(param.meshId == winId) {
				break;
			}
		}


#ifdef ONLY_MISSING_PARAMS
		// Skip windows which already have parameters.
		if (parameters[pid].radii[0] >= 0.0f && parameters[pid].radii[1] >= 0.0f) {
			continue;
		}
#endif

		windowCenters[winId] /= float(windowCounts[winId]);
		windowNormals[winId] /= float(windowCounts[winId]);
		windowNormals[winId] = windowNormals[winId].normalized();
		// Now we have the approximate plane of the window, centered on the window.
		// Find the camera which is the most fronto parallel, ie dot product between window normal and (camera pos - window center) is the highest.
		struct CameraScores {
			float affinity;
			int cid;
		};
		std::vector<CameraScores> sortedCameras;
		for (int cid = 0; cid < scene->cameras().size(); ++cid) {
			const auto & cam = scene->cameras()[cid];
			
			// Check that we are on the right side of the window.
			const sibr::Vector3f camPosDir = (cam->position() - windowCenters[winId]).normalized();
			if (camPosDir.dot(windowNormals[winId]) < 0.1f) {
				continue;
			}
			const float affinity = cam->dir().dot(-windowNormals[winId]);
			sortedCameras.emplace_back();
			sortedCameras.back().affinity = affinity;
			sortedCameras.back().cid = cid;
		}
		if (sortedCameras.empty()) {
			SIBR_ERR << "Unable to find a good enough camera for window " << winId << " estimation." << std::endl;
			continue;
		}
		// Higher affinity is better.
		std::sort(sortedCameras.begin(), sortedCameras.end(), [](const CameraScores & a, const CameraScores & b) { return a.affinity > b.affinity; });
		
		std::vector<float> dists(sortedCameras.size());
		for (int i = 0; i < sortedCameras.size(); ++i) {
			dists[i] = (scene->cameras()[sortedCameras[i].cid]->position() - windowCenters[winId]).norm();
		}
		std::vector<float> distsSort(dists);
		std::sort(distsSort.begin(), distsSort.end());
		const float maxDist = distsSort[distsSort.size()/4];

		int bid = 0;
		for (; bid < int(sortedCameras.size()); ++bid) {
			if (dists[bid] < maxDist) {
				break;
			}
		}
		
		batches.emplace_back(sortedCameras[bid].cid, winId, pid);
		std::cout << "Camera " << scene->cameras()[batches.back().view]->name() << " selected for window " << winId << "." << std::endl;
	}
	

	
	for (auto & batch : batches) {
		batch.neighs.clear();
		const auto selects = scene->closestCameras(*scene->cameras()[batch.view], batch.view);
		for (int cid = 0; cid < args.neighcount; ++cid) {
			batch.neighs.push_back(selects[cid]);
		}
	}


	const int downscaling = 1;
	const float minX = 1.0f;
	const float maxX = 40.0f;
	const int countX = 66;
	const float minY = 1.0f;
	const float maxY = 40.0f;
	const int countY = 66;
	const float clampValue = 10.0f / 255.0f;

	// Exhaustive search in parameter space.
	for (const auto & batch : batches) {
		const auto & mainCam = scene->cameras()[batch.view];
		std::cout << "Main camera: " << mainCam->name() << ": ";

		const int windowId = batch.window;

		// Compute the separate reflections.
		cv::Mat1f medianScores = cv::Mat1f::zeros(countX, countY);
		cv::Mat1f percScores = cv::Mat1f::zeros(countX, countY);

		std::cout << "Clamp " << clampValue << "." << std::endl;
		std::cout << "X (" << minX << "," << maxX << ") / " << countX << "." << std::endl;
		std::cout << "Y (" << minY << "," << maxY << ") / " << countY << "." << std::endl;


		const cv::Size workSize(mainCam->w() / downscaling, mainCam->h() / downscaling);
		const cv::Size renderSize(mainCam->w() / downscaling, mainCam->h() / downscaling);
		const unsigned int neighCount = int(batch.neighs.size());

		for (int xid = 0; xid < countX; ++xid) {
			std::cout << "|" << std::flush;

			const float dpX = xid / float(countX - 1.0f);
			const float pX = minX + (maxX - minX) * dpX * dpX;


			for (int yid = 0; yid < countY; ++yid) {
				std::cout << "." << std::flush;
				const float dpY = yid / float(countY - 1.0f);
				const float pY = minY + dpY * dpY * (maxY - minY);

				std::vector<sibr::ImageRGB> dstImgs(neighCount);

				
				for (uint i = 0; i < neighCount; ++i) {
					const auto & id = batch.neighs[i];
					sibr::RenderTargetRGB dst(renderSize.width, renderSize.height);
					mainView->renderWarpedReflection(batch.view, { id }, windowId, pX, pY, dst);
					dst.readBack(dstImgs[i]);
				}
		
				// Compute median color.
				cv::Mat3f medianImg32 = cv::Mat3f::zeros(workSize);
#pragma omp parallel for
				for (int y = 0; y < workSize.height; ++y) {
#pragma omp parallel for
					for (int x = 0; x < workSize.width; ++x) {
						std::vector<sibr::Vector3f> pixels;
						for (const auto & img : dstImgs) {
							const sibr::Vector3ub & col = img(x, y);
							if (col.isNull()) {
								continue;
							}
							pixels.emplace_back(col.cast<float>());
						}
						if (pixels.empty()) {
							continue;
						}

						std::sort(pixels.begin(), pixels.end(), [](const sibr::Vector3f & a, const sibr::Vector3f & b)
						{
							const float la = a.dot(sibr::Vector3f(1.0, 1.0, 1.0));
							const float lb = b.dot(sibr::Vector3f(1.0, 1.0, 1.0));
							return la < lb;
						});
						const sibr::Vector3f & medianCol = pixels[pixels.size() / 2];
						medianImg32(y, x)[0] = medianCol[0] / 255.0f;
						medianImg32(y, x)[1] = medianCol[1] / 255.0f;
						medianImg32(y, x)[2] = medianCol[2] / 255.0f;
					}
				}
				
				std::vector<cv::Mat1f> costs(neighCount);
				std::vector<cv::Mat1b> costsMasks(neighCount);
#pragma omp parallel for
				for (int nid = 0; nid < int(neighCount); ++nid) {
					costs[nid] = cv::Mat1f::zeros(workSize);
					costsMasks[nid] = cv::Mat1b::zeros(workSize);
				}

#pragma omp parallel for
				for (int y = 0; y < workSize.height; ++y) {
#pragma omp parallel for
					for (int x = 0; x < workSize.width; ++x) {
#pragma omp parallel for
						for (int nid = 0; nid < int(neighCount); ++nid) {

							const sibr::Vector3ub & baseCol = dstImgs[nid](x, y);
							if (baseCol.isNull()) {
								continue;
							}
							const sibr::Vector3f warpedCol = baseCol.cast<float>() / 255.0f;
							const cv::Vec3f & medianImgCol = medianImg32(y, x);
							const sibr::Vector3f medianCol(medianImgCol[0], medianImgCol[1], medianImgCol[2]);
							const sibr::Vector3f absDiff = (medianCol - warpedCol).cwiseAbs();
							const float mSad = absDiff.dot(sibr::Vector3f(1.0f / 3.0f, 1.0f / 3.0f, 1.0f / 3.0f));
							const float metric = std::min(mSad, clampValue);
							costs[nid](y, x) = metric;
							costsMasks[nid](y, x) = 1;
						}
					}
				}

				// Per-pixel cost data.
				struct Score {
					float score;
					float weight;
					int id;
				};
				// Scores and weight.
				cv::Mat1f medianScore(workSize, 0.0f);

#pragma omp parallel for
				for (int y = 0; y < workSize.height; ++y) {
#pragma omp parallel for
					for (int x = 0; x < workSize.width; ++x) {
						// Collect valid scores for this pixel.
						std::vector<Score> perPixScores;
						for (int nid = 0; nid < batch.neighs.size(); ++nid) {

							if (costsMasks[nid](y, x) == 0) {
								continue;
							}
							perPixScores.emplace_back();
							perPixScores.back().score = costs[nid](y, x);
							perPixScores.back().weight = 1.0f;
							perPixScores.back().id = nid;
						}
						if (perPixScores.empty()) {
							continue;
						}
						// Sort the scores.
						std::sort(perPixScores.begin(), perPixScores.end(), [](const Score & a, const Score & b)
						{
							return a.score < b.score;
						});
						// Pick median score.
						const size_t scoresSize = perPixScores.size();
						const float medScore = perPixScores[scoresSize / 2].score;
						medianScore(y, x) = medScore;
						
					}
				}

				// Blur the resulting scores.
				cv::blur(medianScore, medianScore, cv::Size(5, 5));
				// Re-normalize for clarity.
				medianScore /= clampValue;


				// Reduce these cost maps in a global score.
				{
					const cv::Scalar finalScoreMed = cv::sum(medianScore);
					medianScores(xid, yid) = float(finalScoreMed[0]);
				}

			}

		}
		std::cout << std::endl;
		// Compute exponential score.
		cv::Mat1f expScore(countX, countY, 0.0f);
		for (int xid = 0; xid < countX; ++xid) {
			for (int yid = 0; yid < countY; ++yid) {
				expScore(xid, yid) = std::exp(-medianScores(xid, yid));
			}
		}


		sibr::Vector2f bestRadii;
		
		{
			double mini, maxi;
			cv::Point minLoc(-1, -1);
			cv::Point maxLoc(-1, -1);
			cv::minMaxLoc(expScore, &mini, &maxi, &minLoc, &maxLoc);
			// The highest exponential score corresponds to the lowest median error.
			// X is row ID, Y is column ID. The cv::Point returned is in column/row order so we have to flip.
			const float dpX = maxLoc.y / float(countX - 1.0f);
			const float dpY = maxLoc.x / float(countY - 1.0f);
			const float pX = minX + dpX * dpX * (maxX - minX);
			const float pY = minY + dpY * dpY * (maxY - minY);
			std::cout << "Median exp score: " << maxi << " at (" << pX << ", " << pY << ")," << std::endl;

			bestRadii = {pX, pY};
		}


		parameters[batch.params].radii = bestRadii;
		
	}

	std::ofstream outputFile(scene->basePath() + "/semantic_reflections/reflections/parameters.txt");
	outputFile << "# Params: " << args.neighcount << " neigh, clamp " << clampValue << ",  X (" << minX << "," << maxX << ") / " << countX << ", Y (" << minY << "," << maxY << ") / " << countY << "." << std::endl;
	for(const auto & param: parameters) {
		outputFile << param.meshId << " " << param.radii[0] << " " << param.radii[1] << std::endl;
	}
	
	outputFile.close();
	return EXIT_SUCCESS;
}