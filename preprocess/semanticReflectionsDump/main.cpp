/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>

#include <core/graphics/Window.hpp>
#include <core/view/ViewBase.hpp>
#include <core/view/MultiViewManager.hpp>

#include <core/renderer/DepthRenderer.hpp>
#include "projects/semantic_reflections/renderer/SemanticReflectionsScene.hpp"
#include "projects/semantic_reflections/renderer/SemanticReflectionsView.hpp"
#include "core/system/SimpleTimer.hpp"
#include "core/system/String.hpp"
#include "core/raycaster/KdTree.hpp"
#include "ICP.hpp"

#define PROGRAM_NAME "sibr_semantic_reflections_dump"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;




struct SemanticReflectionsDumpArgs : BasicDatasetArgs {

	Arg<int> max_input_width = { "max_width", -1, "maximum width for input data in memory" };
	Arg<bool> dumpGeometry = {"geometry", "Perform the geometry generation preprocess"};
	Arg<bool> dumpBG = { "background", "Perform the background generation preprocess" };
	Arg<bool> dumpFlows = { "flows", "Perform the flows computation preprocess" };
	Arg<bool> performIcp = { "icp", "Perform ICP preprocess" };
	Arg<int> neighcount = { "ncount", 4,  "Number of neighbors to dump for the flows" };
};

sibr::Mesh::Ptr alignMesh(const sibr::Mesh::Ptr & source, const sibr::Mesh::Ptr & target, const int subsampling, const float p, const int iterations) {


	const int step = subsampling;
	SICP::Parameters pars;
	pars.p = p;           /// p norm
	//double mu = 10.0;         /// penalty weight
	//double alpha = 1.2;       /// penalty increase factor
	//double max_mu = 1e5;      /// max penalty
	pars.max_icp = iterations;        /// max ICP iteration
	//int max_outer = 100;      /// max outer iteration
	//int max_inner = 1;        /// max inner iteration. If max_inner=1 then ADMM else ALM
	//double stop = 1e-5;       /// stopping criteria
	pars.print_icpn = true;  /// (debug) print ICP iteration 

	/// ICP
	target->generateNormals();
	const std::vector<sibr::Vector3f> srcVertices = source->vertices();
	const std::vector<sibr::Vector3f> tgtVertices = target->vertices();
	const std::vector<sibr::Vector3f> tgtNormals = target->normals();
	const size_t vCount = srcVertices.size() / step;
	Eigen::Matrix<double, 3, Eigen::Dynamic> src(3, vCount);
	Eigen::Matrix<double, 3, Eigen::Dynamic> srcBack(3, vCount);
	Eigen::Matrix<double, 3, Eigen::Dynamic> tgt(3, vCount);
	Eigen::Matrix<double, 3, Eigen::Dynamic> tgtN(3, vCount);
	for (int vid = 0; vid < vCount; ++vid) {
		const int svid = step * vid;
		src(0, vid) = srcVertices[svid][0];
		src(1, vid) = srcVertices[svid][1];
		src(2, vid) = srcVertices[svid][2];
		srcBack(0, vid) = srcVertices[svid][0];
		srcBack(1, vid) = srcVertices[svid][1];
		srcBack(2, vid) = srcVertices[svid][2];

		tgt(0, vid) = tgtVertices[svid][0];
		tgt(1, vid) = tgtVertices[svid][1];
		tgt(2, vid) = tgtVertices[svid][2];
		tgtN(0, vid) = target->normals()[svid][0];
		tgtN(1, vid) = target->normals()[svid][1];
		tgtN(2, vid) = target->normals()[svid][2];
	}

	SICP::point_to_plane(src, tgt, tgtN, pars);

	// We now transfer the transformation to the full mesh.
	sibr::Vector3d cBefore(0.0f, 0.0f, 0.0f);
	sibr::Vector3d cAfter(0.0f, 0.0f, 0.0f);
	Eigen::Matrix3d rotation = Eigen::Matrix3d::Identity();
	{
		// Center src point cloud before and after ICP.
		for (int vid = 0; vid < vCount; ++vid) {
			cBefore += srcBack.col(vid);
			cAfter += src.col(vid);
		}
		cBefore /= float(vCount);
		cAfter /= float(vCount);
		Eigen::Matrix<double, 3, Eigen::Dynamic> mBefore(3, vCount);
		Eigen::Matrix<double, 3, Eigen::Dynamic> mAfter(3, vCount);
		for (int vid = 0; vid < vCount; ++vid) {
			mBefore.col(vid) = srcBack.col(vid) - cBefore;
			mAfter.col(vid) = src.col(vid) - cAfter;
		}
		// Build aggregate.
		Eigen::Matrix3d W = Eigen::Matrix3d::Zero(3, 3);
		for (int i = 0; i < srcBack.cols(); ++i) {
			W = W + mAfter.col(i) * mBefore.col(i).transpose();
		}
		// SVD decomposition.
		Eigen::JacobiSVD<Eigen::Matrix3d> svd(W, Eigen::ComputeFullU | Eigen::ComputeFullV);
		rotation = (svd.matrixV()*svd.matrixU().transpose()).transpose();
	}
	auto finalMesh = source->clone();
	// Transform the mesh.
	std::vector<sibr::Vector3f> finalVertices(srcVertices.size());
	for (int vid = 0; vid < srcVertices.size(); ++vid) {
		finalVertices[vid] = (rotation * (srcVertices[vid].cast<double>() - cBefore) + cAfter).cast<float>();
	}
	finalMesh->vertices(finalVertices);
	return finalMesh;
}

int main(int ac, char** av) {


	CommandLineArgs::parseMainArgs(ac, av);
	SemanticReflectionsDumpArgs args;
	const std::string basePath = args.dataset_path;
	const int maxWidth = args.max_input_width;

	sibr::Window window("Semantic dump");
	window.makeContextCurrent();

	if(args.performIcp) {
		const auto files = sibr::listFiles(basePath + "/semantic_reflections/shrinkwrap/", false, false, { "ply" });
		const std::string meshPrefix = "smooth_mesh_";
		const std::string meshSuffix = "_final.ply";
		const std::string meshFlipSuffix = "_final_flip.ply";

		for (const auto & file : files) {
			if (file.size() <= meshPrefix.size() || file.substr(0, meshPrefix.size()) != meshPrefix || file.find(meshSuffix) == std::string::npos) {
				continue;
			}
			std::cout << "Processing mesh " << file << "..." << std::endl;
			const std::string carId = file.substr(meshPrefix.size(), 3);

			const std::string fullCarName = meshPrefix + carId + meshSuffix;
			sibr::Mesh::Ptr fullCar(new sibr::Mesh(false));
			fullCar->load(basePath + "/semantic_reflections/shrinkwrap/" + fullCarName);
			
			const std::string flipCarName = meshPrefix + carId + meshFlipSuffix;
			sibr::Mesh::Ptr flipCar(new sibr::Mesh(false));
			flipCar->load(basePath + "/semantic_reflections/shrinkwrap/" + flipCarName);

			const auto finalMesh = alignMesh(flipCar, fullCar, 50, 0.2f, 250);
			finalMesh->save(basePath + "/semantic_reflections/shrinkwrap/smooth_mesh_" + carId + "_final_flip_aligned_auto.ply", true);
		}
	}

	if(args.dumpGeometry){
		auto scene = std::make_shared<SemanticReflectionsScene>(basePath, maxWidth, SemanticReflectionsScene::GEOMETRY_PREPROCESS, false);
		// Lazy loading already did the job.
		return EXIT_SUCCESS;
	}

	if (args.dumpBG) {
		auto scene = std::make_shared<SemanticReflectionsScene>(basePath, maxWidth, SemanticReflectionsScene::BG_PREPROCESS, false);
		// Lazy loading already did the job.
		return EXIT_SUCCESS;
	}
	
	// Dump flow for reflections layer extraction.
	if (args.dumpFlows) {
		auto scene = std::make_shared<SemanticReflectionsScene>(basePath, maxWidth, SemanticReflectionsScene::FLOWS_PREPROCESS, false);
		SemanticReflectionsView::Ptr mainView(new SemanticReflectionsView(scene, uint(scene->resolution()[0]), uint(scene->resolution()[1])));
		const int numSelectedNeighs = args.neighcount;
		mainView->dumpFlows(numSelectedNeighs);
	}
	
	return EXIT_SUCCESS;
}