/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#define NOMINMAX

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/ray_tracing.h"
#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/vision.h"
#include "projects/fribr_framework/renderer/io.h"
#include "projects/fribr_framework/renderer/tools/file_tools.h"
#include "projects/fribr_framework/renderer/tools/image_tools.h"
#include "projects/fribr_framework/renderer/tools/string_tools.h"
#include "projects/fribr_framework/renderer/tools/geometry_tools.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "GCoptimization.h"

#include <opencv2/core.hpp>
#include <opencv2/ximgproc.hpp>

#include <igl/components.h>

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>

#include <Windows.h>
#include <WinBase.h>
#include <core\system\CommandLineArgs.hpp>

using namespace Eigen;
using namespace fribr;

namespace
{

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error %d: %s\n", error, description);
}

inline Eigen::Vector3f unproject(float x, float y, float depth,
	const Eigen::Matrix3f &inv_intrinsics)
{
	const Eigen::Vector3f img_pos = Eigen::Vector3f(x, y, 1.0f);
	return depth * (inv_intrinsics * img_pos);
}

inline Eigen::Vector3f unproject(float x, float y, float depth,
	const Eigen::Matrix3f &inv_intrinsics,
	const Eigen::Matrix4f &cam_to_world)
{
	const Eigen::Vector3f cam_pos = unproject(x, y, depth, inv_intrinsics);
	const Eigen::Vector4f world_pos = cam_to_world * cam_pos.homogeneous();
	return Eigen::Vector3f(world_pos.x(), world_pos.y(), world_pos.z());
}

inline Eigen::Vector3f pixel_to_direction(const Eigen::Vector2f pixel_position, const Eigen::Vector2i resolution,
	                                      const Eigen::Matrix3f reference_frame, const float max_angle)
{
	const float xf = pixel_position.x() / resolution.x() - 0.5f;
	const float yf = pixel_position.y() / resolution.y() - 0.5f;

	const float           angle     = Eigen::Vector2f(xf, yf).norm() / 0.5f * max_angle;
	const Eigen::Vector3f direction = (reference_frame * Eigen::Vector3f(xf, 0.0f, yf)).normalized();

	return std::cos(angle) * reference_frame.col(1) + std::sin(angle) * direction;
}

bool project_if_visible(const Eigen::Vector3f              &position, 
	                    const Eigen::Matrix4f              &world_to_clip, 
	                    const CalibratedImage              &camera,
	                    const std::vector<Eigen::Vector3f> &camera_points,
	                    const float                        depth_tolerance,
	                    Eigen::Vector3f                    &image_position_out)
{
	// Reject if outside view frustum
	Eigen::Vector4f p_clip = world_to_clip * position.homogeneous();
	if (p_clip.x() < -p_clip.w() || p_clip.x() > p_clip.w() ||
		p_clip.y() < -p_clip.w() || p_clip.y() > p_clip.w() ||
		p_clip.z() < -p_clip.w() || p_clip.z() > p_clip.w())
		return false;

	Eigen::Vector4f p_cam = camera.get_world_to_camera().matrix() * position.homogeneous();
	Eigen::Vector3f p_img = camera.get_intrinsics() * p_cam.head<3>();
	p_img /= p_img.z();

	// Reject if depth test fails
	int index = (camera.get_scaled_resolution().y() - int(p_img.y()) - 1) *
		         camera.get_scaled_resolution().x() + int(p_img.x());
	if (p_img.x() < 0 || p_img.x() >= camera.get_scaled_resolution().x() ||
		p_img.y() < 0 || p_img.y() >= camera.get_scaled_resolution().y() ||
		p_cam.z() / camera_points[index].z() > depth_tolerance)
		return false;

	image_position_out = p_img;
	return true;
}

bool project_if_visible(const Eigen::Vector3f              &position,
                        const Eigen::Matrix4f              &world_to_clip,
                        const CalibratedImage              &camera,
                        const std::vector<Eigen::Vector3f> &camera_points,
                        const float                        depth_tolerance,
                        Eigen::Vector2i                    &image_position_out)
{
	Eigen::Vector3f image_position_f;
	if (!project_if_visible(position, world_to_clip, camera, camera_points, depth_tolerance, image_position_f))
		return false;

	image_position_out = image_position_f.head<2>().cast<int>();
	return true;
}

void load_calibrated_images(const std::string              &path,
	                        fribr::CalibratedImage::Vector &cameras_dslr,
	                        fribr::PointCloud::Ptr         &point_cloud,
	                        fribr::PointCloudRenderer::Ptr &point_renderer,
	                        fribr::Observations            &observations)
{
	cameras_dslr.clear();
	observations.points.clear();
	observations.observations.clear();

	std::string extension = fribr::get_extension(path);
	fribr::CalibratedImage::Vector cameras_all;
	fribr::Observations            observations_all;
	if (extension == "db") {
		cameras_all = fribr::COLMAP::load_calibrated_images(path);
		point_cloud = fribr::COLMAP::load_point_cloud(path);
		point_renderer.reset(new fribr::PointCloudRenderer(point_cloud));
	}
	else {
		std::cout << "Unsupported sfm format: " << extension << std::endl;
		return;
	}

	// Do not use blacklisted images in the DSLR output.
	std::string              blacklist_path = fribr::strip_extension(path) + ".blacklist";
	std::vector<std::string> blacklist = fribr::Blacklist::load_blacklist(blacklist_path);

	std::vector<int> dslr_to_all_remap;
	for (int i = 0; i < static_cast<int>(cameras_all.size()); ++i)
	{
		fribr::CalibratedImage &camera = cameras_all[i];
		if (!fribr::find_any(blacklist, camera.get_image_name()))
		{
			dslr_to_all_remap.push_back(i);
			cameras_dslr.push_back(camera);
		}
	}

	if (extension == "json") {
		observations.points = observations_all.points;
		for (size_t i = 0; i < cameras_dslr.size(); ++i)
		{
			int index_all = dslr_to_all_remap[i];
			observations.observations.push_back(observations_all.observations[index_all]);
		}
	}
}

void compute_depth_maps_from_mesh(fribr::CalibratedImage::Vector &cameras,
                                  patcher::MeshRayTracer         &ray_tracer,
                                  std::vector<std::vector<Eigen::Vector3f> > &camera_points,
                                  std::vector<std::vector<Eigen::Vector3f> > &camera_normals)
{
	std::cout << "Constructing depth maps: 0%" << std::flush;
	camera_points.assign(cameras.size(), std::vector<Eigen::Vector3f>());
	camera_normals.assign(cameras.size(), std::vector<Eigen::Vector3f>());
	size_t progress = 0;

	#ifdef NDEBUG
	#pragma omp parallel for
	#endif
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		fribr::CalibratedImage &camera = cameras[i];
		Eigen::Matrix4f         world_to_cam = camera.get_world_to_camera().matrix();
		Eigen::Matrix4f         cam_to_world = world_to_cam.inverse();
		Eigen::Matrix3f         intrinsics = camera.get_intrinsics();
		Eigen::Matrix3f         inv_intrinsics = intrinsics.inverse();
		Eigen::Vector2i         resolution = camera.get_scaled_resolution();
		Eigen::Vector3f         orig = camera.get_position();

		camera_points[i].assign(resolution.x() * resolution.y(), Eigen::Vector3f(0.0f, 0.0f, 1.0f));
		camera_normals[i].assign(resolution.x() * resolution.y(), Eigen::Vector3f(0.0f, 0.0f, 1.0f));

		for (int y = 0; y < resolution.y(); ++y)
		for (int x = 0; x < resolution.x(); ++x)
		{
			fribr::RTResult r;
			r.tri = 0;

			// Our ray-triangle test is not watertight. Use these offets to maximize our chances
			// of always hitting a triangle, even if the ray is very close to a triangle edge.
			float offsets_x[] = { 0.0f, 0.1f, -0.1f,  0.0f, -0.1f };
			float offsets_y[] = { 0.0f, 0.1f,  0.1f, -0.1f,  0.1f };
			for (size_t j = 0; j < 5; ++j)
			{
				int xx = x;
				int yy = resolution.y() - y - 1;
				Eigen::Vector3f far_plane = unproject(xx + offsets_x[j], yy + offsets_y[j], 1000.0f,
					inv_intrinsics, cam_to_world);

				r = ray_tracer.ray_cast(orig, far_plane - orig);
				if (r.tri)
					break;
			}

			if (!r.tri)
			{
				camera_normals[i][y * resolution.x() + x] = Eigen::Vector3f(0.0f, 0.0f, 1.0f);
				continue;
			}

			Eigen::Vector4f pc = world_to_cam * Eigen::Vector4f(r.p[0], r.p[1], r.p[2], 1.0f);
			camera_points[i][y * resolution.x() + x] = pc.head<3>();

			Eigen::Vector3f a(r.tri->m_vertices[0][0], r.tri->m_vertices[0][1], r.tri->m_vertices[0][2]);
			Eigen::Vector3f b(r.tri->m_vertices[1][0], r.tri->m_vertices[1][1], r.tri->m_vertices[1][2]);
			Eigen::Vector3f c(r.tri->m_vertices[2][0], r.tri->m_vertices[2][1], r.tri->m_vertices[2][2]);
			Eigen::Vector3f n = (b - a).cross(c - a).normalized();
			camera_normals[i][y * resolution.x() + x] = world_to_cam.block<3, 3>(0, 0) * n;
		}

		size_t prev_progress = InterlockedExchangeAdd(&progress, 1);
		#pragma omp critical
		{
			if (prev_progress % 2 == 1)
				std::cout << "\rConstructing depth maps: " << prev_progress * 100.0f / cameras.size() << "%                 " << std::flush;
		}
	}
	std::cout << "\rConstructing depth maps: 100%                " << std::endl;
}

void update_depth_maps_from_mesh(fribr::CalibratedImage::Vector &cameras,
	                             const std::vector<int>         &camera_indices,
                                 fribr::RayTracer               &ray_tracer,
	                             const std::vector<float>       &vertices,
                                 std::vector<std::vector<Eigen::Vector3f> > &camera_points,
                                 std::vector<std::vector<Eigen::Vector3f> > &camera_normals)
{
	std::cout << "Updating depth maps: 0%" << std::flush;
	size_t progress = 0;

	#ifdef NDEBUG
	#pragma omp parallel for
	#endif
	for (int cam_i = 0; cam_i < static_cast<int>(camera_indices.size()); ++cam_i)
	{
		const int i = camera_indices[cam_i];

		fribr::CalibratedImage &camera = cameras[i];
		Eigen::Matrix4f         world_to_cam = camera.get_world_to_camera().matrix();
		Eigen::Matrix4f         cam_to_world = world_to_cam.inverse();
		Eigen::Matrix3f         intrinsics = camera.get_intrinsics();
		Eigen::Matrix3f         inv_intrinsics = intrinsics.inverse();
		Eigen::Vector2i         resolution = camera.get_scaled_resolution();
		Eigen::Vector3f         orig = camera.get_position();

		// Only update the depth map in a bbox
		Eigen::Array2i min_pos(resolution.x(), resolution.y());
		Eigen::Array2i max_pos(0, 0);

		for (int j = 0; j < static_cast<int>(vertices.size()); j += 3)
		{
			Eigen::Vector3f p_world(vertices[j + 0], vertices[j + 1], vertices[j + 2]);
			Eigen::Vector4f p_cam = world_to_cam * p_world.homogeneous();
			if (p_cam.z() >= 0.0f)
				continue;

			Eigen::Vector3f p_img = intrinsics * p_cam.head<3>();
			p_img /= p_img.z();

			Eigen::Vector2i i_img = p_img.head<2>().cast<int>();
			i_img.y() = std::max(0, std::min(resolution.y() - 1, resolution.y() - i_img.y() - 1));

			min_pos = min_pos.min(i_img.array());
			max_pos = max_pos.max(i_img.array() + 1);
		}

		min_pos = min_pos.max(Eigen::Array2i(0, 0));
		max_pos = max_pos.min(resolution.array());

		for (int y = min_pos.y(); y < max_pos.y(); ++y)
		for (int x = min_pos.x(); x < max_pos.x(); ++x)
		{
			fribr::RTResult r;
			r.tri = 0;

			// Our ray-triangle test is not watertight. Use these offets to maximize our chances
			// of always hitting a triangle, even if the ray is very close to a triangle edge.
			float offsets_x[] = { 0.0f, 0.1f, -0.1f,  0.0f, -0.1f };
			float offsets_y[] = { 0.0f, 0.1f,  0.1f, -0.1f,  0.1f };
			for (size_t j = 0; j < 5; ++j)
			{
				int xx = x;
				int yy = resolution.y() - y - 1;
				Eigen::Vector3f far_plane = unproject(xx + offsets_x[j], yy + offsets_y[j], 1000.0f,
					inv_intrinsics, cam_to_world);

				r = ray_tracer.ray_cast(orig, far_plane - orig);
				if (r.tri)
					break;
			}

			if (!r.tri)
			{
				camera_normals[i][y * resolution.x() + x] = Eigen::Vector3f(0.0f, 0.0f, 1.0f);
				continue;
			}

			Eigen::Vector4f pc = world_to_cam * Eigen::Vector4f(r.p[0], r.p[1], r.p[2], 1.0f);
			if (1.01f * pc.z() <= camera_points[i][y * resolution.x() + x].z())
				continue;
			camera_points[i][y * resolution.x() + x] = pc.head<3>();

			Eigen::Vector3f a(r.tri->m_vertices[0][0], r.tri->m_vertices[0][1], r.tri->m_vertices[0][2]);
			Eigen::Vector3f b(r.tri->m_vertices[1][0], r.tri->m_vertices[1][1], r.tri->m_vertices[1][2]);
			Eigen::Vector3f c(r.tri->m_vertices[2][0], r.tri->m_vertices[2][1], r.tri->m_vertices[2][2]);
			Eigen::Vector3f n = (b - a).cross(c - a).normalized();
			camera_normals[i][y * resolution.x() + x] = world_to_cam.block<3, 3>(0, 0) * n;
		}

		size_t prev_progress = InterlockedExchangeAdd(&progress, 1);
		#pragma omp critical
		{
			if (prev_progress % 2 == 1)
				std::cout << "\rUpdating depth maps: " << prev_progress * 100.0f / cameras.size() << "%                 " << std::flush;
		}
	}
	std::cout << "\rUpdating depth maps: 100%                " << std::endl;
}

bool load_depth_maps(std::string depth_map_folder,
                     fribr::CalibratedImage::Vector &cameras,
                     std::vector<std::vector<Eigen::Vector3f> > &camera_points,
                     std::vector<std::vector<Eigen::Vector3f> > &camera_normals)
{
	if (!fribr::directory_exists(depth_map_folder))
		return false;

	camera_points.assign(cameras.size(), std::vector<Eigen::Vector3f>());
	camera_normals.assign(cameras.size(), std::vector<Eigen::Vector3f>());

	size_t progress = 0;
	std::cout << "\rLoading depth maps: 0% " << std::flush;
	#ifdef NDEBUG
	#pragma omp parallel for 
	#endif
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		std::stringstream filename_stream;
		filename_stream << i << ".bin";
		std::string   depth_map_name = fribr::append_path(depth_map_folder, filename_stream.str());
		std::ifstream depth_map_file(depth_map_name, std::ifstream::binary);
		if (!depth_map_file.good())
		{
			std::cout << "Failed to open " << depth_map_name << " for reading " << std::endl;
			continue;
		}

		int size[2];
		depth_map_file.read((char*)size, sizeof(size));

		camera_points[i].assign(size[0] * size[1], Eigen::Vector3f(0.0f, 0.0f, 1.0f));
		camera_normals[i].assign(size[0] * size[1], Eigen::Vector3f(0.0f, 0.0f, 1.0f));

		depth_map_file.read((char*)camera_points[i].data(), camera_points[i].size() * sizeof(float) * 3);
		depth_map_file.read((char*)camera_normals[i].data(), camera_normals[i].size() * sizeof(float) * 3);
		depth_map_file.close();

		size_t prev_progress = InterlockedExchangeAdd(&progress, 1);
		#pragma omp critical
		{
			if (prev_progress % 2 == 1)
				std::cout << "\rLoading depth maps: " << (prev_progress + 1) * 100.0f / cameras.size()
				          << "%                     " << std::flush;
		}
	}
	std::cout << "\rLoading depth maps: 100%                " << std::endl;

	return true;
}

void save_depth_maps(std::string depth_map_folder,
                     fribr::CalibratedImage::Vector &cameras,
                     std::vector<std::vector<Eigen::Vector3f> > &camera_points,
                     std::vector<std::vector<Eigen::Vector3f> > &camera_normals)
{
	if (!fribr::directory_exists(depth_map_folder))
		fribr::make_directory(depth_map_folder);

	size_t progress = 0;
	std::cout << "\rSaving depth maps: 0% " << std::flush;
	#ifdef NDEBUG
	#pragma omp parallel for 
	#endif
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		std::stringstream filename_stream;
		filename_stream << i << ".bin";
		std::string   depth_map_name = fribr::append_path(depth_map_folder, filename_stream.str());
		std::ofstream depth_map_file(depth_map_name, std::ofstream::binary | std::ofstream::trunc);
		if (!depth_map_file.good())
		{
			std::cout << "Failed to open " << depth_map_name << " for writing " << std::endl;
			continue;
		}

		int size[2] = { cameras[i].get_scaled_resolution().x(), cameras[i].get_scaled_resolution().y() };
		depth_map_file.write((char*)size, sizeof(size));

		depth_map_file.write((char*)camera_points[i].data(), camera_points[i].size() * sizeof(float) * 3);
		depth_map_file.write((char*)camera_normals[i].data(), camera_normals[i].size() * sizeof(float) * 3);
		depth_map_file.close();

		size_t prev_progress = InterlockedExchangeAdd(&progress, 1);
		#pragma omp critical
		{
			if (prev_progress % 2 == 1)
				std::cout << "\rSaving depth maps: " << (prev_progress + 1) * 100.0f / cameras.size()
				          << "%                     " << std::flush;
		}
	}
	std::cout << "\rSaving depth maps: 100%                " << std::endl;
}

void strip_mesh(const std::vector<bool> &vertex_mask, const std::vector<float> &positions, const std::vector<float> &normals, const std::vector<unsigned int> &indices,
	            std::vector<float> &stripped_positions, std::vector<float> &stripped_normals, std::vector<unsigned int> &stripped_indices)
{
	const bool has_normals = !normals.empty();
	std::vector<int> full_to_removed_position_remap;
	for (int i = 0; i < static_cast<int>(positions.size()); i += 3)
	{
		if (!vertex_mask[i / 3])
		{
			full_to_removed_position_remap.emplace_back(-1);
			continue;
		}

		full_to_removed_position_remap.emplace_back(stripped_positions.size() / 3);
		stripped_positions.emplace_back(positions[i + 0]);
		stripped_positions.emplace_back(positions[i + 1]);
		stripped_positions.emplace_back(positions[i + 2]);
		if(has_normals) {
			stripped_normals.emplace_back(normals[i + 0]);
			stripped_normals.emplace_back(normals[i + 1]);
			stripped_normals.emplace_back(normals[i + 2]);
		}
	}

	for (int i = 0; i < static_cast<int>(indices.size()); i += 3)
	{
		int i0 = indices[i + 0];
		int i1 = indices[i + 1];
		int i2 = indices[i + 2];

		int ri0 = full_to_removed_position_remap[i0];
		int ri1 = full_to_removed_position_remap[i1];
		int ri2 = full_to_removed_position_remap[i2];
		if (ri0 < 0 || ri1 < 0 || ri2 < 0)
			continue;

		stripped_indices.emplace_back(ri0);
		stripped_indices.emplace_back(ri1);
		stripped_indices.emplace_back(ri2);
	}
}

void strip_mesh(const std::vector<bool> &vertex_mask, const std::vector<float> &positions, const std::vector<unsigned int> &indices,
	std::vector<float> &stripped_positions, std::vector<unsigned int> &stripped_indices) {
	std::vector<float> dummy_normals_in, dummy_normals_out;
	strip_mesh(vertex_mask, positions, dummy_normals_in, indices, stripped_positions, dummy_normals_out, stripped_indices);	
}
bool save_ply(const std::string& path, const std::vector<float>& positions)
{
	std::ofstream mesh_stream(path, std::ios_base::out);
	if (!mesh_stream.good())
	{
		std::cerr << "Could not open output file: " << path << std::endl;
		return false;
	}

	mesh_stream << "ply" << std::endl;
	mesh_stream << "format ascii 1.0" << std::endl;
	mesh_stream << "element vertex " << positions.size() / 3 << std::endl;
	mesh_stream << "property float x" << std::endl;
	mesh_stream << "property float y" << std::endl;
	mesh_stream << "property float z" << std::endl;
	mesh_stream << "element face " << positions.size() / 9 << std::endl;
	mesh_stream << "property list uchar int vertex_indices" << std::endl;
	mesh_stream << "end_header" << std::endl;

	for (int i = 0; i < static_cast<int>(positions.size()); i += 3)
		mesh_stream << positions[i + 0] << " " << -positions[i + 1] << " " << -positions[i + 2] << std::endl;

	for (int i = 0; i < static_cast<int>(positions.size() / 3); i += 3)
		mesh_stream << "3 " << i + 0 << " " << i + 1 << " " << i + 2 << std::endl;

	mesh_stream.close();
	return true;
}
bool save_ply(const std::string &path, const std::vector<unsigned int> &indices, const std::vector<float> &positions)
{
	std::ofstream mesh_stream(path, std::ios_base::out);
	if (!mesh_stream.good())
	{
		std::cerr << "Could not open output file: " << path << std::endl;
		return false;
	}

	mesh_stream << "ply" << std::endl;
	mesh_stream << "format ascii 1.0" << std::endl;
	mesh_stream << "element vertex " << positions.size() / 3 << std::endl;
	mesh_stream << "property float x" << std::endl;
	mesh_stream << "property float y" << std::endl;
	mesh_stream << "property float z" << std::endl;
	mesh_stream << "element face " << indices.size() / 3 << std::endl;
	mesh_stream << "property list uchar int vertex_indices" << std::endl;
	mesh_stream << "end_header" << std::endl;

	for (int i = 0; i < static_cast<int>(positions.size()); i += 3)
		mesh_stream << positions[i + 0] << " " << positions[i + 1] << " " << positions[i + 2] << std::endl;

	for (size_t i = 0; i < indices.size(); i += 3)
		mesh_stream << "3 " << indices[i + 0] << " " << indices[i + 1] << " " << indices[i + 2] << std::endl;

	mesh_stream.close();
	return true;
}

bool save_ply_colors(const std::string& path, const std::vector<unsigned int>& indices, const std::vector<float>& positions, const std::vector<float>& colors)
{
	std::ofstream mesh_stream(path, std::ios_base::out);
	if (!mesh_stream.good())
	{
		std::cerr << "Could not open output file: " << path << std::endl;
		return false;
	}

	mesh_stream << "ply" << std::endl;
	mesh_stream << "format ascii 1.0" << std::endl;
	mesh_stream << "element vertex " << positions.size() / 3 << std::endl;
	mesh_stream << "property float x" << std::endl;
	mesh_stream << "property float y" << std::endl;
	mesh_stream << "property float z" << std::endl;
	mesh_stream << "property uchar red" << std::endl;
	mesh_stream << "property uchar green" << std::endl;
	mesh_stream << "property uchar blue" << std::endl;
	mesh_stream << "element face " << indices.size() / 3 << std::endl;
	mesh_stream << "property list uchar int vertex_indices" << std::endl;
	mesh_stream << "end_header" << std::endl;

	for (int i = 0; i < static_cast<int>(positions.size()); i += 3)
		mesh_stream << positions[i + 0] << " " << -positions[i + 1] << " " << -positions[i + 2] << " "
		<< std::max(0, std::min(255, static_cast<int>(colors[i + 0] * 255.0f))) << " "
		<< std::max(0, std::min(255, static_cast<int>(colors[i + 1] * 255.0f))) << " "
		<< std::max(0, std::min(255, static_cast<int>(colors[i + 2] * 255.0f))) << " "
		<< std::endl;

	for (size_t i = 0; i < indices.size(); i += 3)
		mesh_stream << "3 " << indices[i + 0] << " " << indices[i + 1] << " " << indices[i + 2] << std::endl;

	mesh_stream.close();
	return true;
}

bool save_ply_normals(const std::string& path, const std::vector<unsigned int>& indices, const std::vector<float>& positions, const std::vector<float>& normals)
{
	std::ofstream mesh_stream(path, std::ios_base::out);
	if (!mesh_stream.good())
	{
		std::cerr << "Could not open output file: " << path << std::endl;
		return false;
	}

	mesh_stream << "ply" << std::endl;
	mesh_stream << "format ascii 1.0" << std::endl;
	mesh_stream << "element vertex " << positions.size() / 3 << std::endl;
	mesh_stream << "property float x" << std::endl;
	mesh_stream << "property float y" << std::endl;
	mesh_stream << "property float z" << std::endl;
	mesh_stream << "property float nx" << std::endl;
	mesh_stream << "property float ny" << std::endl;
	mesh_stream << "property float nz" << std::endl;
	mesh_stream << "element face " << indices.size() / 3 << std::endl;
	mesh_stream << "property list uchar int vertex_indices" << std::endl;
	mesh_stream << "end_header" << std::endl;

	for (int i = 0; i < static_cast<int>(positions.size()); i += 3)
		mesh_stream << positions[i + 0] << " " << -positions[i + 1] << " " << -positions[i + 2] << " "
		<< normals[i + 0] << " " << -normals[i + 1] << " " << -normals[i + 2] << std::endl;

	for (size_t i = 0; i < indices.size(); i += 3)
		mesh_stream << "3 " << indices[i + 0] << " " << indices[i + 1] << " " << indices[i + 2] << std::endl;

	mesh_stream.close();
	return true;
}
bool save_ply_uvs(const std::string &path, const std::vector<unsigned int> &indices, const std::vector<float> &positions, const std::vector<float> &uvs)
{
	std::ofstream mesh_stream(path, std::ios_base::out);
	if (!mesh_stream.good())
	{
		std::cerr << "Could not open output file: " << path << std::endl;
		return false;
	}

	mesh_stream << "ply" << std::endl;
	mesh_stream << "format ascii 1.0" << std::endl;
	mesh_stream << "element vertex " << positions.size() / 3 << std::endl;
	mesh_stream << "property float x" << std::endl;
	mesh_stream << "property float y" << std::endl;
	mesh_stream << "property float z" << std::endl;
	mesh_stream << "property float texture_u" << std::endl;
	mesh_stream << "property float texture_v" << std::endl;
	mesh_stream << "element face " << indices.size() / 3 << std::endl;
	mesh_stream << "property list uchar int vertex_indices" << std::endl;
	mesh_stream << "end_header" << std::endl;

	int uv_i = 0;
	for (int i = 0; i < static_cast<int>(positions.size()); i += 3) {
		mesh_stream << positions[i + 0] << " " << positions[i + 1] << " " << positions[i + 2] << " "
			<< uvs[uv_i + 0] << " " << uvs[uv_i + 1] << std::endl;
		uv_i += 2;
	}
		

	for (size_t i = 0; i < indices.size(); i += 3)
		mesh_stream << "3 " << indices[i + 0] << " " << indices[i + 1] << " " << indices[i + 2] << std::endl;

	mesh_stream.close();
	return true;
}


void printSolverStatus(Eigen::ComputationInfo infos) {
	std::string errorReason;
	switch (infos) {
	case Eigen::NumericalIssue:
		errorReason = "NumericalIssue";
		break;
	case Eigen::NoConvergence:
		errorReason = "NoConvergence";
		break;
	case Eigen::InvalidInput:
		errorReason = "InvalidInput";
		break;
	default:
		errorReason = "Unknown";
	}
	std::cout << "Solving FAILED: " << errorReason << std::endl;
}

cv::Mat1f solve_linear_system(const cv::Mat1f &data_image, const cv::Mat1f &weight_image,
	const std::function<float(int x, int y, int nx, int ny)> & pairwise_lambda)
{
	// Compute the sparse matrix using triplets.
	std::vector<Eigen::Triplet<double>> triplets;
	std::vector<double> bs;
	int current_row = 0;

	// Collect unary costs
	for (int y = 0; y < data_image.rows; ++y)
	for (int x = 0; x < data_image.cols; ++x)
	{
		const float w = weight_image(y, x);
		const float d = data_image(y, x);
		if (d <= 0.0f || w < 0.0)
			continue;

		const int pixel_id = y * data_image.cols + x;
		const int row_id = current_row++;

		triplets.emplace_back(row_id, pixel_id, w);
		bs.push_back(w * d);
	}

	//std::cout << "Collecting pairwise" << std::endl;
#if 1
	for (int y = 0; y < data_image.rows; ++y)
	for (int x = 0; x < data_image.cols; ++x)
	{
		const int this_id = y * data_image.cols + x;
		const int right_id = y * data_image.cols + x + 1;
		const int bottom_id = (y + 1) * data_image.cols + x;
		
		if (x < data_image.cols - 1)
		{
			int row_id = current_row++;
			float w = pairwise_lambda(x, y, x + 1, y);
			triplets.emplace_back(row_id, right_id, -w);
			triplets.emplace_back(row_id, this_id, w);
			bs.push_back(0.0);
		}
		if (y < data_image.rows - 1)
		{
			int row_id = current_row++;
			float w = pairwise_lambda(x, y, x, y + 1);
			triplets.emplace_back(row_id, bottom_id, -w);
			triplets.emplace_back(row_id, this_id, w);
			bs.push_back(0.0);
		}
	}
#endif

#if 1
	for (int y = 0; y < data_image.rows; ++y)
	for (int x = 0; x < data_image.cols; ++x)
	{
		const int this_id = y * data_image.cols + x;
		const int left_id = y * data_image.cols + x - 1;
		const int right_id = y * data_image.cols + x + 1;
		const int top_id = (y - 1) * data_image.cols + x;
		const int bottom_id = (y + 1) * data_image.cols + x;

		int row_id = current_row++;
		float total_w = 0.0f;
		if (x > 0)
		{
			float w = pairwise_lambda(x, y, x - 1, y);
			triplets.emplace_back(row_id, left_id, -w);
			total_w += w;
		}
		if (x < data_image.cols - 1)
		{
			float w = pairwise_lambda(x, y, x + 1, y);
			triplets.emplace_back(row_id, right_id, -w);
			total_w += w;
		}
		if (y > 0)
		{
			float w = pairwise_lambda(x, y, x, y - 1);
			triplets.emplace_back(row_id, top_id, -w);
			total_w += w;
		}
		if (y < data_image.rows - 1)
		{
			float w = pairwise_lambda(x, y, x, y + 1);
			triplets.emplace_back(row_id, bottom_id, -w);
			total_w += w;
		}

		triplets.emplace_back(row_id, this_id, total_w * 0.99825f);
	    // 0.99825f at 1024x1024 with sphere centered on car centroid
		// 0.99675f at 512x512 with sphere centered on car centroid
		// 0.9984375f at 512x512 with sphere centered on the ground
		// SHOULD BE 0.9999772221f at 512x512 (acos(total_angle / 512), where total_angle = 1.1 * PI)
		bs.push_back(0.0);
	}
#endif

	//std::cout << "Sparse matrix" << std::endl;
	Eigen::SparseMatrix<double > A(current_row, data_image.rows * data_image.cols);
	A.setFromTriplets(triplets.begin(), triplets.end());
	Eigen::MatrixXd b(current_row, 1);
	for (int i = 0; i < current_row; ++i) {
		b(i, 0) = bs[i];
	}

	//std::cout << "Building system." << std::endl;
	const Eigen::SparseMatrix<double> At = A.transpose();
	const Eigen::SparseMatrix<double> AtA = At * A;
	const Eigen::MatrixXd Atb = At * b;
	Eigen::MatrixXd solution(data_image.rows * data_image.cols, 1);

	// Start with a direct solver.
	//const Eigen::CholmodSupernodalLLT<Eigen::SparseMatrix<double>> solver(AtA);
	const Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver(AtA);
	solution = solver.solve(Atb);

	if (solver.info() != Eigen::Success) {
		printSolverStatus(solver.info());
		// Let's try an iterative solver instead.
		//Eigen::ConjugateGradient<Eigen::SparseMatrix<double>, Eigen::Lower | Eigen::Upper> solverer;
		Eigen::BiCGSTAB<Eigen::SparseMatrix<double>, Eigen::IncompleteLUT<double>> solverer;
		solverer.setMaxIterations(1500);
		solverer.setTolerance(1e-10);
		solverer.compute(AtA);
		solution = solverer.solve(Atb);
		std::cout << "#iterations: " << solverer.iterations() << ", ";
		std::cout << "estimated error: " << solverer.error() << "." << std::endl;
	}

	cv::Mat1f output(data_image.size());
	for (int y = 0; y < data_image.rows; ++y)
	for (int x = 0; x < data_image.cols; ++x)
		output(y, x) = solution(y * data_image.cols + x);

	return output;
}

cv::Mat1f solve_linear_system_irls(const cv::Mat1f &data_image, const cv::Mat1f &weight_image,
	const std::function<float(int x, int y, int nx, int ny)> & pairwise_lambda)
{
	cv::Mat1f solution = solve_linear_system(data_image, weight_image, pairwise_lambda);
	for (int i = 0; i < 10; ++i)
	{
		cv::Mat1f updated_weights = cv::Mat1f::ones(weight_image.size());
		for (int y = 0; y < data_image.rows; y++)
		for (int x = 0; x < data_image.cols; ++x)
		{
			float total_pairwise = 0.0f;
			static const int DELTA_X[] = { 0, 0, -1, 1 };
			static const int DELTA_Y[] = { -1, 1, 0, 0 };
			for (int j = 0; j < 4; ++j)
			{
				int nx = x + DELTA_X[j];
				int ny = y + DELTA_Y[j];
				if (nx < 0 || nx >= data_image.cols ||
					ny < 0 || ny >= data_image.cols)
					continue;

				total_pairwise += pairwise_lambda(x, y, nx, ny) * std::abs(solution(y, x) - solution(ny, nx));
			}

			updated_weights(y, x) = weight_image(y, x) / std::max(0.000001f, std::abs(data_image(y, x) - solution(y, x)) + total_pairwise);
		}

		solution = solve_linear_system(data_image, updated_weights, pairwise_lambda);
	}
	return solution;
}

} // anonymous namespace


int main(int argc, char* argv[])
{
	sibr::CommandLineArgs::parseMainArgs(argc, argv);
	sibr::BasicDatasetArgs args;

	const std::string folder_path = args.dataset_path;
	int num_extra = folder_path.length();
	std::string padding_string(num_extra, '=');
	std::cout << std::endl;
	std::cout << " ========================================" << padding_string << std::endl;
	std::cout << " ========================================" << padding_string << std::endl;
	std::cout << " ==== RUNNING SHRINKRWAP FOR SCENE: " << folder_path << " ====" << std::endl;
	std::cout << " ========================================" << padding_string << std::endl;
	std::cout << " ========================================" << padding_string << std::endl;
	std::cout << std::endl;

	
	const std::string images_path = folder_path + "/colmap/stereo/images";
	const std::string masks_path  = folder_path + "/semantic_reflections/masks";

	const float CARNESS_THRESHOLD = 0.4f; //< carness vote, robust.
	const int MIN_COMPONENT_SIZE = 1024; //< Vertex count to discard floaters, just used for speed up.
	const float COVERAGE_THRESHOLD = 33.0f; //< Camera % seeing a car for it to be considered as "major", robust.
	const Eigen::Vector2i SPHERE_RESOLUTION(128, 128);
	const float           MAX_SPHERE_ANGLE = (3.141592653f * 0.5f) * 1.5f;
	const Eigen::Vector2i DEPTH_MAP_RESOLUTION(1024, 1024);
	const float BBOX_TOLERANCE = 0.1f; //< bbox extension when generating depth map. 
	const int	SMOOTHING_ITERATIONS = 3; 
	const float DEPTH_VALIDITY_TH = 0.02f; //< Epsilon for depth in warped space.
	const float EPSILON = 0.01f; //< general epsilon.
	const float AVOID_GRAZING_ANGLE_THRESHOLD = 0.33f; //< Lower confidence in cameras seeing a surface at a grazing angle.
	const float WINDOW_THRESHOLD = 0.5f; //< Window probability th.
	const float MRF_WINDOW_COST = 0.52f; //< Acceptance threshold in the MRF for window regions.
	const float MRF_PHOTOCONS_TH = 0.2f; //< Photoconsistency threshold.
	const float MRF_WEIGHT_SCALE = 200.0f;  //< Weight, to relate to data term in 0,255
	const std::string baseName = "semantic_reflections/shrinkwrap";
	
	if (!directory_exists(folder_path))
	{
		std::cout << "Could not find input images at: " << folder_path << std::endl;
		return 0;
	}

	if (!directory_exists(images_path))
	{
		std::cout << "Could not find reference images at: " << images_path << std::endl;
		return 0;
	}

	if (!directory_exists(masks_path))
	{
		std::cout << "Could not find input masks at: " << masks_path << std::endl;
		return 0;
	}

	if (!directory_exists(folder_path + "/semantic_reflections"))
		make_directory(folder_path + "/semantic_reflections");


	// We need a GL context for CalibratedImage and PatchCloud
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		std::cerr << "Could not initialize glfw." << std::endl;
		return 1;
	}

	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	GLFWwindow* window = glfwCreateWindow(1280, 720, "IBRNext recon cmd", NULL, NULL);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);

	glewExperimental = GL_TRUE;
	GLenum glew_error = glewInit();
	if (GLEW_OK != glew_error)
	{
		std::cerr << "glew init failed: " << glewGetErrorString(glew_error) << std::endl;
		return 1;
	}

	std::cout << "Loading SfM scene..." << std::endl;
	CalibratedImage::Vector cameras;
	PointCloud::Ptr         point_cloud;
	PointCloudRenderer::Ptr point_renderer;
	Observations            observations;
	try
	{
		const std::string sfm_path = folder_path + "/colmap/database.db";
		load_calibrated_images(sfm_path, cameras, point_cloud, point_renderer, observations);
	}
	catch (const std::exception &e)
	{
		std::cerr << "Could not load SfM reconstruction " << e.what() << std::endl;
		return 1;
	}
	const std::string output_directory = folder_path + "/" + baseName;
	if (!directory_exists(output_directory))
		make_directory(output_directory);

	std::cout << "Loading images and masks..." << std::endl;
	std::vector<cv::Mat3b> masks(cameras.size());
	#ifdef NDEBUG
	#pragma omp parallel for 
	#endif
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		const int max_width = 1920;
		CalibratedImage &camera = cameras[i];
		const std::string mask_path = masks_path + "/" + camera.get_image_name() + ".png";
		camera.load_image(max_width, CalibratedImage::CorrectRadialDistortion, CalibratedImage::CPUOnly);
		masks[i] = cv::imread(mask_path, cv::IMREAD_UNCHANGED);
		cv::resize(masks[i], masks[i], camera.get_image().size(), 0.0, 0.0, cv::INTER_NEAREST);
	}

	#define EXPORT_MESH 0
	std::cout << "Loading mesh..." << std::endl;
	Scene::Ptr scene;
	try
	{
		std::string meshPath = folder_path + "/capreal/mesh.ply";
		if (!sibr::fileExists(meshPath)) {
			meshPath = folder_path + "/capreal/mesh.obj";
		}
		scene.reset(new Scene(meshPath, RH_Y_UP));
	}
	catch (const std::exception &e)
	{
		std::cout << e.what() << std::endl;
		std::cerr << "Could not find mesh /capreal/mesh.ply in the scene directory: " << folder_path << std::endl;
		return 1;
	}

	std::cout << "Initializing ray caster..." << std::endl;
	patcher::MeshRayTracer ray_tracer;
	ray_tracer.set_scene(scene);

	std::cout << "Building depth maps..." << std::endl;
	std::vector<std::vector<Eigen::Vector3f>> camera_points(cameras.size());
	std::vector<std::vector<Eigen::Vector3f>> camera_normals(cameras.size());

	#define HAS_SSD 1
	#if HAS_SSD	
	const std::string depth_map_path = folder_path + "/" + baseName + "/cam_depth";
	if (!load_depth_maps(depth_map_path, cameras, camera_points, camera_normals))
	{
		compute_depth_maps_from_mesh(cameras, ray_tracer, camera_points, camera_normals);
		save_depth_maps(depth_map_path, cameras, camera_points, camera_normals);
	}
	#else
	compute_depth_maps_from_mesh(cameras, ray_tracer, camera_points, camera_normals);
	#endif

	std::cout << "Adding a car-border to each window in the semantic masks..." << std::endl;
	#ifdef NDEBUG
	#pragma omp parallel for 
	#endif
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		cv::Mat1b channels[3];
		cv::split(masks[i], channels);

		cv::Mat1b fat_window, skinny_window;
		cv::dilate(channels[1], fat_window, cv::Mat(), cv::Point(-1, -1), 2);
		cv::erode(channels[1], skinny_window, cv::Mat(), cv::Point(-1, -1), 2);

		cv::Mat1b window_frame = fat_window;
		window_frame.setTo(0, skinny_window);

		channels[2].setTo(255, window_frame);
		cv::merge(channels, 3, masks[i]);
	}

	std::cout << "Computing car probabilities for each vertex..." << std::endl;
	if (scene->get_meshes().size() > 1)
		std::cout << "Warning: Found more than one mesh in the scene. Using only mesh 0..." << std::endl;

	std::vector<float>        positions = scene->get_meshes()[0]->get_vertices();
	std::vector<unsigned int> indices = scene->get_meshes()[0]->get_indices();
	std::vector<float>        colors(positions.size(), 1.0f);
	if (positions.empty() || indices.empty())
	{
		std::cout << "Unexpected mesh format. Expected to have positions and indices.";
		if (positions.empty())
			std::cout << " Did not find positions.";
		if (indices.empty())
			std::cout << " Did not find indices.";
		std::cout << std::endl;
		return 0;
	}

	std::vector<Eigen::Matrix4f> world_to_clip_list(cameras.size());
	for (int i = 0; i < static_cast<int>(cameras.size()); i++)
	{
		Eigen::Matrix4f cam_to_clip = cameras[i].get_camera_to_clip(0.01f, 2000.0f);
		Eigen::Matrix4f world_to_cam = cameras[i].get_world_to_camera().matrix();
		world_to_clip_list[i] = cam_to_clip * world_to_cam;
	}

	size_t probability_progress = 0;
	std::cout << "\rEstimating probabilities: 0%" << std::flush;
	#ifdef NDEBUG
	#pragma omp parallel for 
	#endif
	for (int i = 0; i < static_cast<int>(positions.size() / 3); i++)
	{
		int index = i * 3;
		Eigen::Vector3f position(positions[index + 0], positions[index + 1], positions[index + 2]);

		int num_observations = 0;
		int num_car_labels = 0;
		for (int j = 0; j < static_cast<int>(cameras.size()); ++j)
		{
			CalibratedImage& camera = cameras[j];

			Eigen::Vector2i image_position;
			if (!project_if_visible(position, world_to_clip_list[j], cameras[j], camera_points[j], 1.01f, image_position))
				continue;

			// Finally: reject if mask is window
			cv::Vec3b semantic_label = masks[j](masks[j].rows - 1 - image_position.y(), image_position.x());
			if (semantic_label[1] > 0 && semantic_label[2] == 0)
				continue;

			bool is_car = semantic_label[2] > 0;
			num_observations++;
			if (is_car)
				num_car_labels++;
		}

		colors[index + 0] = 0.0f;
		colors[index + 1] = 0.0f;
		colors[index + 2] = 0.0f;

		if (num_observations > 0)
		{
			colors[index + 0] = static_cast<float>(num_car_labels) / num_observations;
			colors[index + 1] = static_cast<float>(num_car_labels) / num_observations;
			colors[index + 2] = static_cast<float>(num_car_labels) / num_observations;
		}

		size_t prev_progress = InterlockedExchangeAdd(&probability_progress, 1);
		if (prev_progress % 1024 == 1)
		{
			#pragma omp critical
			{
				std::cout << "\rEstimating probabilities: " << (probability_progress + 1) * 3 * 100.0f / positions.size()
				          << "%                           " << std::flush;
			}
		}
	}
	std::cout << "\rEstimating probabilities: 100%                           " << std::endl;

	std::cout << "Stripping mesh..." << std::endl;
	
	std::vector<bool>         car_only_mask;
	std::vector<float>        car_only_positions;
	std::vector<unsigned int> car_only_indices;

	for (int i = 0; i < static_cast<int>(positions.size()); i += 3)
		car_only_mask.emplace_back(colors[i] > CARNESS_THRESHOLD);
	strip_mesh(car_only_mask, positions, indices, car_only_positions, car_only_indices);

	std::cout << "Saving intermediate meshes..." << std::endl;
	const std::string probability_mesh_path = output_directory + "/car_probability_mesh.ply";
	const std::string only_car_mesh_path = output_directory + "/only_car_mesh.ply";
	#if EXPORT_MESH
	save_ply_colors(probability_mesh_path, indices, positions, colors);
	save_ply(only_car_mesh_path, car_only_indices, car_only_positions);
	#endif

	std::cout << "Estimating scene up vector..." << std::endl;
	// \TODO make utility function  
	Eigen::Vector3f up(0.0f, 0.0f, 0.0f);
	for (CalibratedImage &camera : cameras)
		up += camera.get_orientation() * Eigen::Vector3f(0.0f, 1.0f, 0.0f);
	up.normalize();

	Eigen::Vector3f front;
	if (std::abs(up.x()) < std::min(std::abs(up.y()), std::abs(up.z())))
		front = Eigen::Vector3f(1.0f, 0.0f, 0.0f);
	else if (std::abs(up.y()) < std::min(std::abs(up.x()), std::abs(up.z())))
		front = Eigen::Vector3f(0.0f, 1.0f, 0.0f);
	else
		front = Eigen::Vector3f(0.0f, 0.0f, 1.0f);
	Eigen::Vector3f right = front.cross(up).normalized();
	front = up.cross(right).normalized();

	std::cout << "Finding connected components..." << std::endl;
	Eigen::MatrixXi indices_igl(car_only_indices.size() / 3, 3);
	for (int i = 0; i < static_cast<int>(car_only_indices.size()); i += 3)
	{
		indices_igl(i / 3, 0) = car_only_indices[i + 0];
		indices_igl(i / 3, 1) = car_only_indices[i + 1];
		indices_igl(i / 3, 2) = car_only_indices[i + 2];
	}
	Eigen::MatrixXi components;
	igl::components(indices_igl, components);

	std::cout << "Dumping connected components..." << std::endl;
	std::vector<float> car_only_colors(car_only_positions.size(), 0.0f);
	for (int i = 0; i < static_cast<int>(car_only_positions.size()); i += 3)
	{
		int component_index = components(i / 3, 0) % 8;
		if (component_index & 1)
			car_only_colors[i + 0] = 1.0f;
		if (component_index & 2)
			car_only_colors[i + 1] = 1.0f;
		if (component_index & 4)
			car_only_colors[i + 2] = 1.0f;
	}

	const std::string car_components_mesh_path = output_directory + "/car_components_mesh.ply";
	#if EXPORT_MESH
	save_ply_colors(car_components_mesh_path, car_only_indices, car_only_positions, car_only_colors);
	#endif
	int num_components = 0;
	for (int i = 0; i < static_cast<int>(car_only_positions.size() / 3); i++)
		num_components = std::max(components(i, 0) + 1, num_components);

	std::vector<int> component_sizes(num_components, 0);
	for (int i = 0; i < num_components; ++i)
	{
		int num_vertices = 0;
		for (int j = 0; j < static_cast<int>(car_only_positions.size() / 3); j++)
			if (components(j, 0) == i) num_vertices++;
		component_sizes[i] = num_vertices;
	}

	std::cout << "Finding oriented bounding boxes..." << std::endl;
	std::vector<Eigen::Matrix3f> bbox_matrices(num_components);
	std::vector<Eigen::Array3f>  bbox_minimums(num_components);
	std::vector<Eigen::Array3f>  bbox_maximums(num_components);

	
	std::vector<float> bboxes_mesh;
	for (int i = 0; i < num_components; ++i)
	{
		const int num_vertices = component_sizes[i];
		if (num_vertices < MIN_COMPONENT_SIZE)
			continue;

		int vertex_index = 0;
		Eigen::Matrix<float, 2, Dynamic> vertices(2, num_vertices);
		std::vector<float> up_coordinates(num_vertices);
		for (int j = 0; j < static_cast<int>(car_only_positions.size() / 3); j++)
		{
			if (components(j, 0) != i)
				continue;

			Eigen::Vector3f v(car_only_positions[j * 3 + 0],
				car_only_positions[j * 3 + 1],
				car_only_positions[j * 3 + 2]);

			vertices(0, vertex_index) = v.dot(front);
			vertices(1, vertex_index) = v.dot(right);
			up_coordinates[vertex_index] = v.dot(up);
			vertex_index++;
		}

		Eigen::Vector2f mean = vertices.rowwise().mean();
		Eigen::Matrix<float, 2, Dynamic> zero_mean_vertices = vertices;
		for (int j = 0; j < num_vertices; ++j)
			zero_mean_vertices.col(j) -= mean;

		Eigen::Matrix<float, 2, 2> scatter = zero_mean_vertices * zero_mean_vertices.transpose();
		Eigen::JacobiSVD<Eigen::Matrix<float, 2, 2> > svd(scatter, ComputeThinU | ComputeThinV);
		Eigen::Matrix<float, 2, 2> plane_to_eigen = svd.matrixV().transpose();
		if (plane_to_eigen.determinant() < 0)
			plane_to_eigen.row(0) *= -1;
		Eigen::Matrix<float, 2, 2> eigen_to_plane = plane_to_eigen.transpose();

		Eigen::Matrix<float, 2, Dynamic> eigen_vertices = plane_to_eigen * vertices;

		Eigen::Array3f bbox_min(1e21f, 1e21f, 1e21f);
		Eigen::Array3f bbox_max(-1e21f, -1e21f, -1e21f);
		for (int j = 0; j < num_vertices; ++j)
		{
			Eigen::Vector2f v2 = eigen_vertices.col(j);
			Eigen::Array3f v(v2.x(), up_coordinates[j], v2.y());
			bbox_min = bbox_min.min(v);
			bbox_max = bbox_max.max(v);
		}

		Eigen::Vector3f new_front = eigen_to_plane(0, 0) * right + eigen_to_plane(0, 1) * front;
		Eigen::Vector3f new_right = eigen_to_plane(1, 0) * right + eigen_to_plane(1, 1) * front;

		Eigen::Matrix3f bbox_matrix;
		bbox_matrix << new_right, up, new_front;

		bbox_matrices[i] = bbox_matrix;
		bbox_minimums[i] = bbox_min;
		bbox_maximums[i] = bbox_max;

		std::vector<float> bbox_positions = create_box(bbox_min, bbox_max, bbox_matrix, Eigen::Vector3f::Zero());
		bboxes_mesh.insert(bboxes_mesh.end(), bbox_positions.begin(), bbox_positions.end());
	}

	const std::string car_bboxes_mesh_path = output_directory + "/car_bboxes_mesh.ply";
	#if EXPORT_MESH
	save_ply(car_bboxes_mesh_path, bboxes_mesh);
	#endif
	std::cout << "Finding cameras that can see each car..." << std::endl;
	std::vector<std::vector<int>> camera_indices_per_component(num_components);
	std::vector<float> coverage(num_components, 0.0f);
	
	std::cout << "Assigning each camera to a car..." << std::endl;
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		float best_distance  = 1e21f;
		int   best_component = -1;
		for (int j = 0; j < num_components; ++j)
		{
			if (component_sizes[j] < MIN_COMPONENT_SIZE)
				continue;

			Eigen::Array3f  direction_bbox_max(-1e21f, -1e21f, -1e21f);
			Eigen::Array3f  direction_bbox_min(1e21f, 1e21f, 1e21f);
			Eigen::Vector3f bbox_center = bbox_matrices[j] * (bbox_maximums[j] + bbox_minimums[j]).matrix() / 2.0f;

			Eigen::Vector3f to_bbox_center = bbox_center - cameras[i].get_position();
			if (to_bbox_center.normalized().dot(cameras[i].get_forward()) < 0.5f)
				continue;

			float distance = to_bbox_center.norm();
			if (best_distance >= distance)
			{
				best_distance = distance;
				best_component = j;
			}
		}

		if (best_component < 0)
			continue;

		camera_indices_per_component[best_component].emplace_back(i);
	}

	for (int i = 0; i < num_components; ++i)
		coverage[i] = static_cast<float>(camera_indices_per_component[i].size());

	
	std::cout << "Isolating main cars..." << std::endl;
	std::vector<bool>         main_cars_mask;
	std::vector<float>        main_cars_positions;
	std::vector<unsigned int> main_cars_indices;

	for (int i = 0; i < static_cast<int>(car_only_positions.size() / 3); i++)
	{
		int component_index = components(i, 0);
		main_cars_mask.emplace_back(coverage[component_index] >= COVERAGE_THRESHOLD);
	}
	strip_mesh(main_cars_mask, car_only_positions, car_only_indices, main_cars_positions, main_cars_indices);

	std::cout << "Saving main cars mesh..." << std::endl;
	const std::string main_cars_mesh_path = output_directory + "/main_cars_mesh.ply";
	#if EXPORT_MESH
	save_ply(main_cars_mesh_path, main_cars_indices, main_cars_positions);
	#endif
	std::cout << "Visualizing bounding spheres for main cars..." << std::endl;
	
	std::vector<float>        all_bounding_sphere_positions;
	std::vector<unsigned int> all_bounding_sphere_indices;

	std::vector<Eigen::Vector3f> sphere_centers(num_components, Eigen::Vector3f::Zero());
	std::vector<float>           sphere_radii(num_components, 0.0f);

	int num_main_cars = 0;
	for (int i = 0; i < num_components; ++i)
	{
		if (coverage[i] < COVERAGE_THRESHOLD)
			continue;

		num_main_cars++;
		Eigen::Vector3f center = bbox_matrices[i] *
			Eigen::Vector3f((bbox_maximums[i].x() + bbox_minimums[i].x()) * 0.5f,
				            (bbox_maximums[i].y() + bbox_minimums[i].y()) * 0.5f,
				            (bbox_maximums[i].z() + bbox_minimums[i].z()) * 0.5f);
		sphere_centers[i] = center;

		float radius = 0.0f;
		for (int j = 0; j < 8; ++j)
		{
			Eigen::Vector3f corner = bbox_minimums[i].matrix();
			if (j & 1)
				corner.x() = bbox_maximums[i].x();
			if (j & 2)
				corner.y() = bbox_maximums[i].y();
			if (j & 4)
				corner.z() = bbox_maximums[i].z();
			corner = bbox_matrices[i] * corner;
			radius = std::max(radius, (corner - center).norm());
		}
		sphere_radii[i] = radius;

		const int index_offset = all_bounding_sphere_positions.size() / 3;
		for (int y = 0; y < SPHERE_RESOLUTION.y(); ++y)
		for (int x = 0; x < SPHERE_RESOLUTION.x(); ++x)
		{
			Eigen::Vector3f direction =
				pixel_to_direction(Eigen::Vector2f(x + 0.5f, y + 0.5f), SPHERE_RESOLUTION,
					bbox_matrices[i], MAX_SPHERE_ANGLE);

			Eigen::Vector3f vertex = center + direction * radius;
			all_bounding_sphere_positions.emplace_back(vertex.x());
			all_bounding_sphere_positions.emplace_back(vertex.y());
			all_bounding_sphere_positions.emplace_back(vertex.z());
		}

		for (int y = 1; y < SPHERE_RESOLUTION.y(); ++y)
		for (int x = 1; x < SPHERE_RESOLUTION.x(); ++x)
		{
			float xf = static_cast<float>(x) / SPHERE_RESOLUTION.x() - 0.5f;
			float yf = static_cast<float>(y) / SPHERE_RESOLUTION.y() - 0.5f;
			if (Eigen::Vector2f(xf, yf).norm() > 0.5f)
				continue;

			unsigned int i00 = (y - 1) * SPHERE_RESOLUTION.x() + (x - 1) + index_offset;
			unsigned int i01 = (y - 1) * SPHERE_RESOLUTION.x() + (x - 0) + index_offset;
			unsigned int i10 = (y - 0) * SPHERE_RESOLUTION.x() + (x - 1) + index_offset;
			unsigned int i11 = (y - 0) * SPHERE_RESOLUTION.x() + (x - 0) + index_offset;

			all_bounding_sphere_indices.emplace_back(i00); all_bounding_sphere_indices.emplace_back(i10); all_bounding_sphere_indices.emplace_back(i01);
			all_bounding_sphere_indices.emplace_back(i11); all_bounding_sphere_indices.emplace_back(i01); all_bounding_sphere_indices.emplace_back(i10);
		}
	}

	const std::string all_sphere_mesh_path = output_directory + "/all_bounding_sphere_meshes.ply";
	#if EXPORT_MESH
	save_ply(all_sphere_mesh_path, all_bounding_sphere_indices, all_bounding_sphere_positions);
	#endif
	std::cout << "Building normals for each vertex..." << std::endl;
	std::vector<float> normals(positions.size(), 0.0f);
	for (int i = 0; i < static_cast<int>(indices.size()); i += 3)
	{
		const int i0 = 3 * indices[i + 0];
		const int i1 = 3 * indices[i + 1];
		const int i2 = 3 * indices[i + 2];

		const Eigen::Vector3f p0(positions[i0 + 0], positions[i0 + 1], positions[i0 + 2]);
		const Eigen::Vector3f p1(positions[i1 + 0], positions[i1 + 1], positions[i1 + 2]);
		const Eigen::Vector3f p2(positions[i2 + 0], positions[i2 + 1], positions[i2 + 2]);

		const Eigen::Vector3f e01 = p1 - p0;
		const Eigen::Vector3f e02 = p2 - p0;
		
		const Eigen::Vector3f normal = e01.cross(e02);
		for (int j = 0; j < 3; ++j)
		{
			normals[i0 + j] += normal[j];
			normals[i1 + j] += normal[j];
			normals[i2 + j] += normal[j];
		}
	}

	for (int i = 0; i < static_cast<int>(normals.size()); i += 3)
	{
		Eigen::Vector3f normal(normals[i + 0], normals[i + 1], normals[i + 2]);
		normal.normalize();
		normals[i + 0] = normal.x();
		normals[i + 1] = normal.y();
		normals[i + 2] = normal.z();
	}

	const std::string normal_mesh_path = output_directory + "/car_mesh_normals.ply";
	#if EXPORT_MESH
	save_ply_normals(normal_mesh_path, indices, positions, normals);
	#endif
	
	for (int i = 0; i < num_components; ++i)
	{
		if (coverage[i] < COVERAGE_THRESHOLD)
			continue;

		std::cout << std::endl << "=========================== " << std::endl;
		std::cout << "==== PROCESSING CAR " << std::setw(2) << std::setfill('0') << i << " ==== " << std::endl;
		std::cout << "=========================== " << std::endl << std::endl;

		std::cout << "Building initial mesh for car: " << i << "..." << std::endl;
		Eigen::Matrix3f to_bb_space = bbox_matrices[i].transpose();
		Eigen::Vector3f bbox_center = bbox_matrices[i] * (bbox_maximums[i] + bbox_minimums[i]).matrix() * 0.5f;

		std::vector<bool>         current_car_mask;
		std::vector<float>        current_car_positions;
		std::vector<float>        current_car_normals;
		std::vector<unsigned int> current_car_indices;

		for (int j = 0; j < static_cast<int>(positions.size()); j += 3)
		{
			const Eigen::Vector3f position_world(positions[j + 0], positions[j + 1], positions[j + 2]);
			const Eigen::Vector3f normal_world(normals[j + 0], normals[j + 1], normals[j + 2]);
			const Eigen::Vector3f position_bbox = to_bb_space * position_world;
			const Eigen::Array3f  position_bbox_relative = (position_bbox.array() - bbox_minimums[i]) / (bbox_maximums[i] - bbox_minimums[i]);

			const bool inside = (position_bbox_relative.minCoeff() > BBOX_TOLERANCE && position_bbox_relative.maxCoeff() < 1.0f - BBOX_TOLERANCE);
			const bool near_edge = (position_bbox_relative.minCoeff() > -BBOX_TOLERANCE && position_bbox_relative.maxCoeff() < 1.0f + BBOX_TOLERANCE);
			const bool facing_inwards = (position_world - bbox_center).normalized().dot(normal_world) < 0.0f;

			bool is_in_other_bbox = false;
			for (int k = 0; k < num_components; ++k)
			{
				if (k == i || camera_indices_per_component[k].empty())
					continue;

				const Eigen::Vector3f position_other_bbox = bbox_matrices[k].transpose() * position_world;
				const Eigen::Vector3f position_other_bbox_relative = (position_other_bbox.array() - bbox_minimums[k]) / (bbox_maximums[k] - bbox_minimums[k]);
				if (position_other_bbox_relative.minCoeff() > 0.0f && position_other_bbox_relative.maxCoeff() < 1.0f)
				{
					is_in_other_bbox = true;
					break;
				}
			}

			current_car_mask.emplace_back(inside || (near_edge && !facing_inwards && !is_in_other_bbox));
		}
		strip_mesh(current_car_mask, positions, normals, indices, current_car_positions, current_car_normals, current_car_indices);

		std::stringstream ray_cast_mesh_stream;
		ray_cast_mesh_stream << output_directory << "/ray_cast_mesh_" << std::setw(3) << std::setfill('0') << i << ".ply";
		#if 1 || EXPORT_MESH
		save_ply(ray_cast_mesh_stream.str(), current_car_indices, current_car_positions);
		#endif
		// Build a new BVH for this mesh only
		std::cout << "Building ray caster BVH for car: " << i << "..." << std::endl;
		int num_triangles = static_cast<int>(current_car_indices.size()) / 3;

		std::vector<fribr::RTTriangle> triangles;
		fribr::RayTracer               ray_tracer;
		triangles.reserve(num_triangles);

		for (int j = 0; j < static_cast<int>(current_car_indices.size()); j += 3)
		{
			fribr::RTTriangle t;
			t.m_user_pointer = 0;
			t.m_vertices[0] = &current_car_positions[current_car_indices[j + 0] * 3];
			t.m_vertices[1] = &current_car_positions[current_car_indices[j + 1] * 3];
			t.m_vertices[2] = &current_car_positions[current_car_indices[j + 2] * 3];
			t.m_original_index = static_cast<uint32_t>(triangles.size());
			triangles.emplace_back(t);
		}
		ray_tracer.construct_hierarchy(triangles, fribr::BVHHeuristic_SAH);

		std::cout << "Ray casting spherical depth map for car: " << i << "..." << std::endl;
		cv::Mat1f spherical_depth(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		cv::Mat3f spherical_positions(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		cv::Mat3f spherical_normals(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());

		#ifdef NDEBUG
		#pragma omp parallel for 
		#endif
		for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
		for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
		{
			spherical_depth(y, x) = 0.0f;
			spherical_positions(y, x) = cv::Vec3f(0.0f, 0.0f, 0.0f);
			fribr::RTResult result;
			result.tri = 0;

			// Our ray-triangle test is not watertight. Use these offets to maximize our chances
			// of always hitting a triangle, even if the ray is very close to a triangle edge.
			float offsets_x[] = { 0.0f, 0.1f, -0.1f,  0.0f, -0.1f };
			float offsets_y[] = { 0.0f, 0.1f,  0.1f, -0.1f,  0.1f };
			for (size_t j = 0; j < 5; ++j)
			{
				Eigen::Vector3f direction =
					pixel_to_direction(Eigen::Vector2f(x + 0.5f + offsets_x[j], y + 0.5f + offsets_y[j]), DEPTH_MAP_RESOLUTION,
						bbox_matrices[i], MAX_SPHERE_ANGLE);

				Eigen::Vector3f vertex = sphere_centers[i] + direction * sphere_radii[i] * (1.0f + BBOX_TOLERANCE);

				result = ray_tracer.ray_cast(vertex, -direction * sphere_radii[i] * (1.0f + BBOX_TOLERANCE));
				if (result.tri)
					break;
			}

			if (!result.tri || result.t > 1.0f)
				continue;

			spherical_depth(y, x) = result.t;

			const int initial_tid = 3 * result.tri->m_original_index;
				
			for (int j = 0; j < 3; ++j) {
				spherical_positions(y, x)[j] = result.p[j];
				// Barycentric normal inteprolation.
				const float nw = current_car_normals[current_car_indices[initial_tid + 0] * 3 + j];
				const float nu = current_car_normals[current_car_indices[initial_tid + 1] * 3 + j];
				const float nv = current_car_normals[current_car_indices[initial_tid + 2] * 3 + j];
				spherical_normals(y, x)[j] = (1 - result.u - result.v) * nw + result.u * nu + result.v * nv;
			}
		}

		spherical_depth *= 255.0f;
		cv::Mat1b output_depth = spherical_depth;
		std::stringstream ray_cast_depth_map_stream;
		ray_cast_depth_map_stream << output_directory << "/ray_cast_depth_map_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(ray_cast_depth_map_stream.str(), spherical_depth);
		spherical_depth /= 255.0f;

		cv::Mat1f iterated_depth = spherical_depth.clone();
		cv::Mat1f solution       = iterated_depth.clone();

		std::vector<float>        smooth_positions;
		std::vector<float>        smooth_normals;
		std::vector<unsigned int> smooth_indices;

		std::vector<fribr::RTTriangle> smooth_triangles;
		fribr::RayTracer               smooth_ray_tracer;

		const int max_smoothing_iterations = SMOOTHING_ITERATIONS;
		
		for (int smoothing_iteration = 0; smoothing_iteration < max_smoothing_iterations; ++smoothing_iteration)
		{
			std::cout << std::endl << "==== SMOOTHING ITERATION " << smoothing_iteration << " ==== " << std::endl;

			// Estimating reliability for each pixel
			std::cout << "Estimating depth reliability for car: " << i << "..." << std::endl;
			cv::Mat1f spherical_carness(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
			cv::Mat1f spherical_windowness(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());

			#ifdef NDEBUG
			#pragma omp parallel for 
			#endif
			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				spherical_carness(y, x)    = 0.0f;
				spherical_windowness(y, x) = 0.0f;

				if (iterated_depth(y, x) <= 0.0f)
					continue;

				Eigen::Vector3f position;
				for (int j = 0; j < 3; ++j)
					position[j] = spherical_positions(y, x)[j];

				Eigen::Vector3f normal;
				for (int j = 0; j < 3; ++j)
					normal[j] = spherical_normals(y, x)[j];
				normal.normalize();

				float total_weight  = 0.0f;
				float car_weight    = 0.0f;
				float window_weight = 0.0f;

				Eigen::Array3f dir_bbox_max(-1e21f, -1e21f, -1e21f);
				Eigen::Array3f dir_bbox_min( 1e21f,  1e21f,  1e21f);

				for (int camera_index : camera_indices_per_component[i])
				{
					CalibratedImage& camera = cameras[camera_index];

					Eigen::Vector3f image_position_f;
					if (!project_if_visible(position, world_to_clip_list[camera_index], cameras[camera_index], camera_points[camera_index], 1.01f, image_position_f))
						continue;
					Eigen::Vector2i image_position = image_position_f.head<2>().cast<int>();

					cv::Vec3b semantic_label = masks[camera_index](masks[camera_index].rows - 1 - image_position.y(), image_position.x());
					bool      is_car         = semantic_label[2] > 0;
					bool      is_window      = semantic_label[1] > 0 && semantic_label[2] == 0;

					const Eigen::Vector3f view_ray = (camera.get_position() - position).normalized();
					const float cosine_weight = std::max(normal.dot(view_ray), 0.0f);

					total_weight += cosine_weight;
					if (is_car)
					{
						car_weight += cosine_weight;
						dir_bbox_max = dir_bbox_max.max(view_ray.array());
						dir_bbox_min = dir_bbox_min.min(view_ray.array());
					}

					if (is_window)
						window_weight += cosine_weight;
				}

				if (total_weight > 0.0f) {
					spherical_carness(y, x)    = car_weight / total_weight;
					spherical_windowness(y, x) = window_weight / total_weight;
				}
			}

			spherical_carness    *= 255.0f;
			spherical_windowness *= 255.0f;

			cv::Mat1b output_carness    = spherical_carness;
			cv::Mat1b output_windowness = spherical_windowness;

			std::stringstream ray_cast_carness_stream;
			ray_cast_carness_stream << output_directory << "/carness_" << std::setw(3) << std::setfill('0') << i << "_iteration_" << smoothing_iteration << ".png";
			cv::imwrite(ray_cast_carness_stream.str(), output_carness);

			std::stringstream ray_cast_windowness_stream;
			ray_cast_windowness_stream << output_directory << "/windowness_" << std::setw(3) << std::setfill('0') << i << "_iteration_" << smoothing_iteration << ".png";
			cv::imwrite(ray_cast_windowness_stream.str(), output_windowness);

			spherical_carness    /= 255.0f;
			spherical_windowness /= 255.0f;

			std::cout << "Estimating smooth depth for car: " << i << "..." << std::endl;
			cv::Mat1f spherical_reliability(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				float weight = std::abs(spherical_depth(y, x) - iterated_depth(y, x)) <= DEPTH_VALIDITY_TH ? 1.0f : 0.0f;
				spherical_reliability(y, x) = std::min(weight * std::max(spherical_carness(y, x) - spherical_windowness(y, x), 0.0f), 1.0f);
			}

			spherical_reliability *= 255.0f;
			cv::Mat1b output_reliability = spherical_reliability;
			std::stringstream ray_cast_reliability_stream;
			ray_cast_reliability_stream << output_directory << "/reliability_" << std::setw(3) << std::setfill('0') << i << "_iteration_" << smoothing_iteration << ".png";
			cv::imwrite(ray_cast_reliability_stream.str(), output_reliability);
			spherical_reliability /= 255.0f;

			// Define pairwise functor
			const auto evaluate_pairwise = [](int x, int y, int nx, int ny)
			{
				return 0.33f;
			};
		
			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				iterated_depth(y, x)  = std::max(0.0f, std::min(1.0f, 1.0f - iterated_depth(y, x)));
				spherical_depth(y, x) = std::max(0.0f, std::min(1.0f, 1.0f - spherical_depth(y, x)));
			}

			solution = solve_linear_system(spherical_depth, spherical_reliability, evaluate_pairwise);

			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				iterated_depth(y, x)  = std::max(0.0f, std::min(1.0f, 1.0f - iterated_depth(y, x)));
				spherical_depth(y, x) = std::max(0.0f, std::min(1.0f, 1.0f - spherical_depth(y, x)));
				solution(y, x)        = std::max(0.0f, std::min(1.0f, 1.0f - solution(y, x)));
			}

			solution *= 255.0f;
			cv::Mat1b output_solution = solution;

			std::cout << "Visualizing smooth car: " << i << "..." << std::endl;
			std::stringstream ray_cast_solution_stream;
			ray_cast_solution_stream << output_directory << "/solution_" << std::setw(3) << std::setfill('0') << i << "_iteration_" << smoothing_iteration << ".png";
			cv::imwrite(ray_cast_solution_stream.str(), output_solution);
			solution /= 255.0f;

			smooth_positions.clear();
			smooth_indices.clear();
			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				float depth = std::max(0.001f, solution(y, x));

				Eigen::Vector3f direction =
					pixel_to_direction(Eigen::Vector2f(x + 0.5f, y + 0.5f), DEPTH_MAP_RESOLUTION,
						bbox_matrices[i], MAX_SPHERE_ANGLE);

				Eigen::Vector3f vertex = sphere_centers[i] + direction * sphere_radii[i] * (1.0f - depth) * (1.0f + BBOX_TOLERANCE);
				smooth_positions.emplace_back(vertex.x());
				smooth_positions.emplace_back(vertex.y());
				smooth_positions.emplace_back(vertex.z());
			}

			for (int y = 1; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 1; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				float xf = static_cast<float>(x) / DEPTH_MAP_RESOLUTION.x() - 0.5f;
				float yf = static_cast<float>(y) / DEPTH_MAP_RESOLUTION.y() - 0.5f;
				if (Eigen::Vector2f(xf, yf).norm() > 0.5f)
					continue;

				unsigned int i00 = (y - 1) * DEPTH_MAP_RESOLUTION.x() + (x - 1);
				unsigned int i01 = (y - 1) * DEPTH_MAP_RESOLUTION.x() + (x - 0);
				unsigned int i10 = (y - 0) * DEPTH_MAP_RESOLUTION.x() + (x - 1);
				unsigned int i11 = (y - 0) * DEPTH_MAP_RESOLUTION.x() + (x - 0);

				smooth_indices.emplace_back(i00); smooth_indices.emplace_back(i10); smooth_indices.emplace_back(i01);
				smooth_indices.emplace_back(i11); smooth_indices.emplace_back(i01); smooth_indices.emplace_back(i10);
			}

			std::stringstream ray_cast_smooth_stream;
			ray_cast_smooth_stream << output_directory << "/smooth_mesh_" << std::setw(3) << std::setfill('0') << i << "_iteration_" << smoothing_iteration << ".ply";
			//save_ply(ray_cast_smooth_stream.str(), smooth_indices, smooth_positions);

			std::cout << "Building normals for smoothed mesh: " << i << "..." << std::endl;
			smooth_normals.swap(std::vector<float>(smooth_positions.size(), 0.0f));
			for (int j = 0; j < static_cast<int>(smooth_indices.size()); j += 3)
			{
				const int i0 = 3 * smooth_indices[j + 0];
				const int i1 = 3 * smooth_indices[j + 1];
				const int i2 = 3 * smooth_indices[j + 2];

				const Eigen::Vector3f p0(smooth_positions[i0 + 0], smooth_positions[i0 + 1], smooth_positions[i0 + 2]);
				const Eigen::Vector3f p1(smooth_positions[i1 + 0], smooth_positions[i1 + 1], smooth_positions[i1 + 2]);
				const Eigen::Vector3f p2(smooth_positions[i2 + 0], smooth_positions[i2 + 1], smooth_positions[i2 + 2]);

				const Eigen::Vector3f e01 = p1 - p0;
				const Eigen::Vector3f e02 = p2 - p0;

				const Eigen::Vector3f normal = e01.cross(e02);
				for (int k = 0; k < 3; ++k)
				{
					smooth_normals[i0 + k] += normal[k];
					smooth_normals[i1 + k] += normal[k];
					smooth_normals[i2 + k] += normal[k];
				}
			}

			for (int j = 0; j < static_cast<int>(smooth_normals.size()); j += 3)
			{
				Eigen::Vector3f normal(smooth_normals[j + 0], smooth_normals[j + 1], smooth_normals[j + 2]);
				normal.normalize();
				smooth_normals[j + 0] = normal.x();
				smooth_normals[j + 1] = normal.y();
				smooth_normals[j + 2] = normal.z();
			}

			smooth_ray_tracer.destroy_hierarchy();
			smooth_triangles.clear();
			smooth_triangles.reserve(smooth_indices.size() / 3);
			for (int j = 0; j < static_cast<int>(smooth_indices.size()); j += 3)
			{
				fribr::RTTriangle t;
				t.m_user_pointer = 0;
				t.m_vertices[0] = &smooth_positions[smooth_indices[j + 0] * 3];
				t.m_vertices[1] = &smooth_positions[smooth_indices[j + 1] * 3];
				t.m_vertices[2] = &smooth_positions[smooth_indices[j + 2] * 3];
				t.m_original_index = static_cast<uint32_t>(smooth_triangles.size());
				smooth_triangles.emplace_back(t);
			}
			smooth_ray_tracer.construct_hierarchy(smooth_triangles, fribr::BVHHeuristic_SAH);

			std::cout << "Updating depth maps with smooth car mesh for car: " << i << "..." << std::endl;
			update_depth_maps_from_mesh(cameras, camera_indices_per_component[i], smooth_ray_tracer, smooth_positions, camera_points, camera_normals);

			#ifdef NDEBUG
			#pragma omp parallel for 
			#endif
			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				iterated_depth(y, x)      = 0.0f;
				spherical_positions(y, x) = cv::Vec3f(0.0f, 0.0f, 0.0f);

				fribr::RTResult result;
				result.tri = 0;

				// Our ray-triangle test is not watertight. Use these offets to maximize our chances
				// of always hitting a triangle, even if the ray is very close to a triangle edge.
				float offsets_x[] = { 0.0f, 0.1f, -0.1f,  0.0f, -0.1f };
				float offsets_y[] = { 0.0f, 0.1f,  0.1f, -0.1f,  0.1f };
				for (size_t j = 0; j < 5; ++j)
				{
					Eigen::Vector3f direction =
						pixel_to_direction(Eigen::Vector2f(x + 0.5f + offsets_x[j],  y + 0.5f + offsets_y[j]), DEPTH_MAP_RESOLUTION,
							bbox_matrices[i], MAX_SPHERE_ANGLE);

					Eigen::Vector3f vertex = sphere_centers[i] + direction * sphere_radii[i] * (1.0f + BBOX_TOLERANCE);

					result = smooth_ray_tracer.ray_cast(vertex, -direction * sphere_radii[i] * (1.0f + BBOX_TOLERANCE));
					if (result.tri)
						break;
				}

				if (!result.tri || result.t > 1.0f)
					continue;

				iterated_depth(y, x) = result.t;
				const int initial_tid = 3 * result.tri->m_original_index;
				for (int j = 0; j < 3; ++j)
				{
					spherical_positions(y, x)[j] = result.p[j];
					// Barycentric normal inteprolation.
					const float nw = smooth_normals[smooth_indices[initial_tid + 0] * 3 + j];
					const float nu = smooth_normals[smooth_indices[initial_tid + 1] * 3 + j];
					const float nv = smooth_normals[smooth_indices[initial_tid + 2] * 3 + j];
					spherical_normals(y, x)[j] = (1 - result.u - result.v) * nw + result.u * nu + result.v * nv;
				}
			}
		}

		// Export the final mesh, and its flipped version, with UVs.
		{
			std::cout << "Exporting final meshes for: " << i << "..." << std::endl;

			// Final mesh has already been computed.
			
			

			std::vector<float> smooth_positions_final;
			std::vector<float> smooth_positions_final_flip;
			std::vector<unsigned int> smooth_indices_final;
			std::vector<unsigned int> smooth_indices_final_flip;
			std::vector<float> smooth_texcoords_final;

			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				float depth = std::max(0.001f, solution(y, x));

				// We flip along the horizontal axis, y becomes height-y;
				Eigen::Vector3f direction =
					pixel_to_direction(Eigen::Vector2f(x + 0.5f, y + 0.5f), DEPTH_MAP_RESOLUTION,
						bbox_matrices[i], MAX_SPHERE_ANGLE);
				Eigen::Vector3f direction_flip =
					pixel_to_direction(Eigen::Vector2f(x + 0.5f, (DEPTH_MAP_RESOLUTION.y() - y - 1) + 0.5f), DEPTH_MAP_RESOLUTION,
						bbox_matrices[i], MAX_SPHERE_ANGLE);

				Eigen::Vector3f vertex = sphere_centers[i] + direction * sphere_radii[i] * (1.0f - depth) * (1.0f + BBOX_TOLERANCE);
				Eigen::Vector3f vertex_flip = sphere_centers[i] + direction_flip * sphere_radii[i] * (1.0f - depth) * (1.0f + BBOX_TOLERANCE);
				smooth_positions_final.emplace_back(vertex.x());
				smooth_positions_final.emplace_back(vertex.y());
				smooth_positions_final.emplace_back(vertex.z());

				smooth_positions_final_flip.emplace_back(vertex_flip.x());
				smooth_positions_final_flip.emplace_back(vertex_flip.y());
				smooth_positions_final_flip.emplace_back(vertex_flip.z());

				smooth_texcoords_final.emplace_back(float(x) / DEPTH_MAP_RESOLUTION.x());
				smooth_texcoords_final.emplace_back(float(y) / DEPTH_MAP_RESOLUTION.y());
			}

			for (int y = DEPTH_MAP_RESOLUTION.y()-1; y >= 1; --y)
			for (int x = 1; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				float xf = static_cast<float>(x) / DEPTH_MAP_RESOLUTION.x() - 0.5f;
				float yf = static_cast<float>(y) / DEPTH_MAP_RESOLUTION.y() - 0.5f;
				if (Eigen::Vector2f(xf, yf).norm() > 0.5f)
					continue;

				unsigned int i00 = (y - 1) * DEPTH_MAP_RESOLUTION.x() + (x - 1);
				unsigned int i01 = (y - 1) * DEPTH_MAP_RESOLUTION.x() + (x - 0);
				unsigned int i10 = (y - 0) * DEPTH_MAP_RESOLUTION.x() + (x - 1);
				unsigned int i11 = (y - 0) * DEPTH_MAP_RESOLUTION.x() + (x - 0);

				smooth_indices_final.emplace_back(i00); smooth_indices_final.emplace_back(i10); smooth_indices_final.emplace_back(i01);
				smooth_indices_final.emplace_back(i11); smooth_indices_final.emplace_back(i01); smooth_indices_final.emplace_back(i10);
				smooth_indices_final_flip.emplace_back(i00); smooth_indices_final_flip.emplace_back(i01); smooth_indices_final_flip.emplace_back(i10);
				smooth_indices_final_flip.emplace_back(i11); smooth_indices_final_flip.emplace_back(i10); smooth_indices_final_flip.emplace_back(i01);
			}

			// Save the meshes.
			std::stringstream final_mesh_export_stream;
			final_mesh_export_stream << output_directory << "/smooth_mesh_" << std::setw(3) << std::setfill('0') << i << "_final.ply";
			save_ply_uvs(final_mesh_export_stream.str(), smooth_indices_final, smooth_positions_final, smooth_texcoords_final);

			std::stringstream final_mesh_flip_export_stream;
			final_mesh_flip_export_stream << output_directory << "/smooth_mesh_" << std::setw(3) << std::setfill('0') << i << "_final_flip.ply";
			save_ply_uvs(final_mesh_flip_export_stream.str(), smooth_indices_final_flip, smooth_positions_final_flip, smooth_texcoords_final);
		}

		std::cout << "Computing colors and window probabilities for: " << i << "..." << std::endl;
		std::vector<float> smooth_colors(smooth_positions.size(), 0.0f);
		cv::Mat3f          spherical_texture(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		cv::Mat1f          variance_texture(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		cv::Mat1f          window_probabilities(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		#ifdef NDEBUG
		#pragma omp parallel for 
		#endif
		for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
		for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
		{
			

			spherical_texture(y, x)    = cv::Vec3f(0.0f, 0.0f, 0.0f);
			variance_texture(y, x)     = 0.0f;
			window_probabilities(y, x) = 0.0f;

			Eigen::Vector3f position;
			for (int j = 0; j < 3; ++j)
				position[j] = smooth_positions[3 * (y * DEPTH_MAP_RESOLUTION.x() + x) + j];
			Eigen::Vector3f normal;
			for (int j = 0; j < 3; ++j)
				normal[j] = smooth_normals[3 * (y * DEPTH_MAP_RESOLUTION.x() + x) + j];

			float total_weight        = 0.0f;
			float total_window_weight = 0.0f;
			for (int camera_index : camera_indices_per_component[i])
			{
				CalibratedImage& camera = cameras[camera_index];

				Eigen::Vector2i image_position;
				if (!project_if_visible(position, world_to_clip_list[camera_index], cameras[camera_index], camera_points[camera_index], 1.05f, image_position))
				{
					Eigen::Vector3f shifted_position = position + EPSILON * normal;
					if (!project_if_visible(shifted_position, world_to_clip_list[camera_index], cameras[camera_index], camera_points[camera_index], 1.05f, image_position))
						continue;
				}

				cv::Vec3b color          = camera.get_image().at<cv::Vec3b>(camera.get_scaled_resolution().y() - 1 - image_position.y(), image_position.x());
				cv::Vec3b semantic_label = masks[camera_index](masks[camera_index].rows - 1 - image_position.y(), image_position.x());
				bool      is_car         = semantic_label[2] > 0 || semantic_label[1] > 0;

				float weight = is_car ? 1.0f : EPSILON;
				const Eigen::Vector3f viewRay = (camera.get_position() - position).normalized();
				const float cosine_weight = std::max(normal.dot(viewRay), EPSILON);
				weight *= cosine_weight;

				spherical_texture(y, x) += weight * color;
			
				bool is_window = cosine_weight > AVOID_GRAZING_ANGLE_THRESHOLD && semantic_label[1] > 0 && semantic_label[2] == 0;
				if (is_window)
					window_probabilities(y, x) += weight;
				if (cosine_weight > AVOID_GRAZING_ANGLE_THRESHOLD)
					total_window_weight += weight;

				total_weight += weight;
			}

			if (total_weight > 0.0f)
			{
				spherical_texture(y, x) /= total_weight;

				if (total_window_weight > 0.0f)
					window_probabilities(y, x) /= total_window_weight;

				for (int j = 0; j < 3; ++j)
					smooth_colors[3 * (y * DEPTH_MAP_RESOLUTION.x() + x) + j] = spherical_texture(y, x)[2 - j] / 255.0f;
			}

			for (int camera_index : camera_indices_per_component[i])
			{
				CalibratedImage& camera = cameras[camera_index];

				Eigen::Vector2i image_position;
				if (!project_if_visible(position, world_to_clip_list[camera_index], cameras[camera_index], camera_points[camera_index], 1.05f, image_position))
				{
					Eigen::Vector3f shifted_position = position + EPSILON * normal;
					if (!project_if_visible(shifted_position, world_to_clip_list[camera_index], cameras[camera_index], camera_points[camera_index], 1.05f, image_position))
						continue;
				}

				cv::Vec3b color = camera.get_image().at<cv::Vec3b>(camera.get_scaled_resolution().y() - 1 - image_position.y(), image_position.x());
				cv::Vec3b semantic_label = masks[camera_index](masks[camera_index].rows - 1 - image_position.y(), image_position.x());
				bool      is_car = semantic_label[2] > 0 || semantic_label[1] > 0;

				float weight = is_car ? 1.0f : EPSILON;
				const Eigen::Vector3f viewRay = (camera.get_position() - position).normalized();
				const float cosine_weight = std::max(normal.dot(viewRay), EPSILON);
				weight *= cosine_weight;

				cv::Vec3f colorf = color;
				variance_texture(y, x) += weight * (colorf - spherical_texture(y, x)).dot(colorf - spherical_texture(y, x)) / total_weight;
			}

			float luminance = 0.0f;
			for (int j = 0; j < 3; ++j)
				luminance += spherical_texture(y, x)[j] / 3.0f;

			variance_texture(y, x) = std::sqrt(variance_texture(y, x)) / (0.00001f + luminance);
		}
		
		std::cout << "Exporting mesh with colors for: " << i << "..." << std::endl;
		cv::Mat3b output_texture = spherical_texture;
		std::stringstream ray_cast_texture_stream;
		ray_cast_texture_stream << output_directory << "/texture_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(ray_cast_texture_stream.str(), output_texture);

		// DESIGNED TO GET RID OF THE WINDSHIELDS EROSION -- might create overspill on some side windows.
		// Overall, variance_texture is what saves our bacon for perfect window edges. Disable, and trust a guided filter soft mask instead?
		cv::ximgproc::guidedFilter(spherical_texture, variance_texture, variance_texture, 4, 5);
		variance_texture *= 255.0f;
		cv::Mat1b output_variance = variance_texture;
		std::stringstream ray_cast_variance_stream;
		ray_cast_variance_stream << output_directory << "/mrf_photoconsistency_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(ray_cast_variance_stream.str(), output_variance);
		variance_texture /= 255.0f;

		window_probabilities *= 255.0f;
		cv::Mat1b output_probabilities = window_probabilities;
		std::stringstream ray_cast_probability_stream;
		ray_cast_probability_stream << output_directory << "/mrf_window_probability_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(ray_cast_probability_stream.str(), output_probabilities);
		window_probabilities /= 255.0f;

		std::stringstream ray_cast_texture_mesh_stream;
		ray_cast_texture_mesh_stream << output_directory << "/texture_mesh_" << std::setw(3) << std::setfill('0') << i << ".ply";
		#if EXPORT_MESH
		save_ply_colors(ray_cast_texture_mesh_stream.str(), smooth_indices, smooth_positions, smooth_colors);
		#endif
		std::cout << "Computing refined window masks for : " << i << "..." << std::endl;
		std::unique_ptr<GCoptimizationGridGraph> gc =
			std::make_unique<GCoptimizationGridGraph>(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x(), 2);
		
		// Equal to 0...
		const int   UNCERTAINTY_RADIUS = static_cast<int>(0.008f) * DEPTH_MAP_RESOLUTION.norm();

		cv::Mat1b initial_mask = window_probabilities > 0.5f;
		// Maybe these can be replaced with a better threshold for window probabilities.
		// Big problem: PREVIOUSLY (before the depth map update fix, and per-camera car assignment), some
		// rear windows had a window probability value of ~0.51 or something. So threshold + erosion creates
		// the same uncertainty region around each window, instead of just having a very low probability for back windows.
		// Maybe this is useless now with all the fixes?
		// It is disabled as the UNCERTAINTY radius is now 0.
		cv::Mat1b eroded_mask, dilated_mask;
		cv::erode(initial_mask, eroded_mask, cv::Mat(), cv::Point(-1, -1), UNCERTAINTY_RADIUS);
		cv::dilate(initial_mask, dilated_mask, cv::Mat(), cv::Point(-1, -1), UNCERTAINTY_RADIUS);


		// One of our data terms discourages window-labels for regions in the colmap mesh.
		// However, some windows are filled completely. So detect this per-connected component
		// and disable this cost selectively.
		cv::Mat1b filled_mask = cv::Mat1b::zeros(eroded_mask.size());
		for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
		for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
		{
				bool is_filled = spherical_depth(y, x) <= 0.0f;
				if (!is_filled)
					is_filled = (solution(y, x) / spherical_depth(y, x)) > 1.05f ||
					            (spherical_depth(y, x) / solution(y, x)) > 1.05f;
				filled_mask(y, x) = is_filled ? 255 : 0;
		}

		cv::Mat labelsMap;
		const int newRegions = cv::connectedComponents(eroded_mask, labelsMap);

		for (int i = 1; i < newRegions; ++i) {
			cv::Mat1b componentMask = (labelsMap == i);

			int num_filled = 0;
			int num_total  = 0;
			for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
			for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
			{
				if (componentMask(y, x) == 0)
					continue;

				if (filled_mask(y, x) != 0)
					num_filled++;
				num_total++;
			}

			if (static_cast<float>(num_filled) / num_total < 0.5f)
				filled_mask.setTo(unsigned char(255), componentMask);
		}

		
		cv::Mat1b data_cost(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
		for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
		{
			float window_probability = window_probabilities(y, x);
			bool  is_photoconsistent = variance_texture(y, x) < MRF_PHOTOCONS_TH;
			bool  is_filled_in       = filled_mask(y, x) != 0;
			
			// This is the main tuning parameter. Setting this to 0.5 will massively increase the window region size
			// increasing it will limit the refined mask to just the "definitely window" region. Last I checked,
			// 0.55 was the upper limit where we didn't extend the masks at all. 
			// Keep in mind, that this is directly compared with WINDOW_THRESHOLD == 0.5 (hence the value range)/
			float window_cost = MRF_WINDOW_COST; // We don't know initially, but err on the side of "Not window".

			// This is where we create holes in the windshield. We need to find a way to either have no "photoconsistent" pixels. 
			// Currently, I'm using the guided filter hack above (untuned, and probably a bad idea).
			if (dilated_mask(y, x) == 0 || is_photoconsistent) window_cost = 1.0f; // Definitely not window. 
			
			// OR, we can correct for it here (notice there is no "else if", so we overwrite). Make sure that eroded mask always covers photoconsistent pixels.
			if (eroded_mask(y, x)  != 0 && is_filled_in)       window_cost = 0.0f; // Definitely window.

			// Finally: All of these if/else combinations of costs can very likely be phrased as sums, multiplications, minimums, and maximums.
			// This will make it into a "real" cost, and likely simplify.

			gc->setDataCost(y * DEPTH_MAP_RESOLUTION.x() + x, 0, MRF_WEIGHT_SCALE * WINDOW_THRESHOLD);
			gc->setDataCost(y * DEPTH_MAP_RESOLUTION.x() + x, 1, MRF_WEIGHT_SCALE * window_cost);
			data_cost(y, x) = 255.0f * window_cost;
		}

		struct SeamHidingSmoothFunctor : public GCoptimization::SmoothCostFunctor {
			GCoptimization::EnergyTermType compute(GCoptimization::SiteID s1, GCoptimization::SiteID s2,
				GCoptimization::LabelID l1, GCoptimization::LabelID l2)
			{
				if (l1 == l2)
					return 0;

				int x1 = s1 % ref->cols;
				int y1 = s1 / ref->cols;
				int x2 = s2 % ref->cols;
				int y2 = s2 / ref->cols;

				
				cv::Vec3f s1rgb = (*ref)(y1, x1);
				cv::Vec3f s2rgb = (*ref)(y2, x2);
				cv::Vec3f delta = s1rgb - s2rgb;
				float distance = delta.dot(delta);

				return 500.0f * std::exp(-distance / (3.0f * 3.0f * 2.0f)) + 5;
			}
			cv::Mat3f *ref;
		};
		
		
		SeamHidingSmoothFunctor functor;
		functor.ref = &spherical_texture;

		cv::Mat1b pairwise_x(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x() - 1);
		cv::Mat1b pairwise_y(DEPTH_MAP_RESOLUTION.y() - 1, DEPTH_MAP_RESOLUTION.x());
		for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
		for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
		{
			if (y > 0)
				pairwise_y(y - 1, x - 0) = std::min(255.0f, 0.5f * functor.compute((y - 1) * DEPTH_MAP_RESOLUTION.x() + (x - 0), (y - 0) * DEPTH_MAP_RESOLUTION.x() + (x - 0), 0, 1));
			if (x > 0)
				pairwise_x(y - 0, x - 1) = std::min(255.0f, 0.5f * functor.compute((y - 0) * DEPTH_MAP_RESOLUTION.x() + (x - 1), (y - 0) * DEPTH_MAP_RESOLUTION.x() + (x - 0), 0, 1));
		}

		std::stringstream data_cost_stream;
		data_cost_stream << output_directory << "/mrf_data_cost_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(data_cost_stream.str(), data_cost);	

		std::stringstream pairwise_x_stream;
		pairwise_x_stream << output_directory << "/mrf_pairwise_x_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(pairwise_x_stream.str(), pairwise_x);

		std::stringstream pairwise_y_stream;
		pairwise_y_stream << output_directory << "/mrf_pairwise_y_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(pairwise_y_stream.str(), pairwise_y);

		gc->setSmoothCostFunctor(&functor);
		gc->setVerbosity(2);
		gc->expansion(8);

		cv::Mat1b output_mask(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		cv::Mat3b output_vis(DEPTH_MAP_RESOLUTION.y(), DEPTH_MAP_RESOLUTION.x());
		for (int y = 0; y < DEPTH_MAP_RESOLUTION.y(); ++y)
		for (int x = 0; x < DEPTH_MAP_RESOLUTION.x(); ++x)
		{
			int l = gc->whatLabel(y * spherical_texture.cols + x);
			output_mask(y, x) = l ? 255 : 0;

			int grey = std::min(255.0f, 2 * (spherical_texture(y, x)[0] + spherical_texture(y, x)[1] + spherical_texture(y, x)[2]) / 3);
			output_vis(y, x) = l ? cv::Vec3b(0, 0, grey) : spherical_texture(y, x);

		}

		// Do a flood fill to get rid of small missing regions.
		cv::floodFill(output_mask, cv::Point(0, 0), 128);
		// Everything that has not been filled is considered as windows.
		output_mask = (output_mask != 128);
		
		// Apply a guided filter.
		cv::Mat1b filtered_output_mask;
		cv::ximgproc::guidedFilter(spherical_texture, output_mask, filtered_output_mask, 9, 8.0f);

		std::cout << "Exporting mesh with masks for: " << i << "..." << std::endl;

		std::stringstream ray_cast_mask_stream;
		ray_cast_mask_stream << output_directory << "/mrf_window_mask_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(ray_cast_mask_stream.str(), output_mask);

		std::stringstream ray_cast_mask_filtered_stream;
		ray_cast_mask_filtered_stream << output_directory << "/mrf_window_mask_filtered_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(ray_cast_mask_filtered_stream.str(), filtered_output_mask);

		std::stringstream ray_cast_mask_vis_stream;
		ray_cast_mask_vis_stream << output_directory << "/mrf_window_mask_vis_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(ray_cast_mask_vis_stream.str(), output_vis);
		/*
		std::stringstream ray_cast_mask_mesh_stream;
		ray_cast_mask_mesh_stream << output_directory << "/mrf_window_mask_mesh_" << std::setw(3) << std::setfill('0') << i << ".ply";
		save_ply_colors(ray_cast_mask_mesh_stream.str(), smooth_indices, smooth_positions, smooth_colors);*/
		
	}

	glfwTerminate();
    return 0;
}
